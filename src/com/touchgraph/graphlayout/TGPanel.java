/*
 * TouchGraph LLC. Apache-Style Software License
 *
 *
 * Copyright (c) 2002 Alexander Shapiro. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by
 *        TouchGraph LLC (http://www.touchgraph.com/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "TouchGraph" or "TouchGraph LLC" must not be used to endorse
 *    or promote products derived from this software without prior written
 *    permission.  For written permission, please contact
 *    alex@touchgraph.com
 *
 * 5. Products derived from this software may not be called "TouchGraph",
 *    nor may "TouchGraph" appear in their name, without prior written
 *    permission of alex@touchgraph.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL TOUCHGRAPH OR ITS CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 */

package com.touchgraph.graphlayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JPanel;

import com.touchgraph.graphlayout.graphelements.GraphEltSet;
import com.touchgraph.graphlayout.graphelements.ImmutableGraphEltSet;
import com.touchgraph.graphlayout.graphelements.TGForEachEdge;
import com.touchgraph.graphlayout.graphelements.TGForEachNode;
import com.touchgraph.graphlayout.graphelements.VisibleLocality;
import com.touchgraph.graphlayout.interaction.TGAbstractClickUI;

/** TGPanel contains code for drawing the graph, and storing which nodes
 * are selected, and which ones the mouse is over.
 *
 * It houses methods to activate TGLayout, which performs dynamic layout.
 * Whenever the graph is moved, or repainted, TGPanel fires listner
 * methods on associated objects.
 *
 * <p><b>
 * Parts of this code build upon Sun's Graph Layout example.
 * http://java.sun.com/applets/jdk/1.1/demo/GraphLayout/Graph.java
 * </b></p>
 *
 * @author   Alexander Shapiro
 * @author   Murray Altheim  (2001-11-06; cleanup)
 * @version  1.20
 */
public class TGPanel
    extends JPanel {

  // static variables for use within the package

  public static Color BACK_COLOR = Color.white;

  // ....
  protected Hashtable completeEltSetHash = null;
  protected Hashtable visibleLocalityHash = null;
  protected Hashtable localityUtilsHash = null;
  private GraphEltSet completeEltSet;
  //private GraphEltSet completeEltSet2;
  protected VisibleLocality visibleLocality;
  private LocalityUtils localityUtils;
  public TGLayout tgLayout;
  protected BasicMouseMotionListener basicMML;

  protected Edge mouseOverE; //mouseOverE is the edge the mouse is over
  protected Node mouseOverN; //mouseOverN is the node the mouse is over
  protected boolean maintainMouseOver = false;
  //If true, then don't change mouseOverN or mouseOverE

  protected Node select;

  Node dragNode; //Node currently being dragged

  protected Point mousePos;
  //Mouse location, updated in the mouseMotionListener
  Image offscreen;
  Dimension offscreensize;
  Graphics offgraphics;

  private Vector graphListeners;
  private Vector paintListeners;
  TGLensSet tgLensSet;
  // Converts between a nodes visual position (drawx, drawy),
  // and its absolute position (x,y).
  AdjustOriginLens adjustOriginLens;
  SwitchSelectUI switchSelectUI;

  //cluster support
  protected Vector openClusters;

  // ............

  /** Default constructor.
   */
  public TGPanel() {
    setLayout(null);
    completeEltSetHash = new Hashtable();
    visibleLocalityHash = new Hashtable();
    localityUtilsHash = new Hashtable();

    setNewGraphEltSet(0);
    addMouseListener(new BasicMouseListener());
    basicMML = new BasicMouseMotionListener();
    addMouseMotionListener(basicMML);

    graphListeners = new Vector();
    paintListeners = new Vector();

    adjustOriginLens = new AdjustOriginLens();
    switchSelectUI = new SwitchSelectUI();

    TGLayout tgLayout = new TGLayout(this);
    setTGLayout(tgLayout);
    tgLayout.start();
    //setGraphEltSet(new GraphEltSet());
    openClusters = new Vector();
  }

  //public boolean doLayout = true;
  public void setLayoutMode(boolean doLt) {
    //doLayout = doLt;
    if (doLt) {
      tgLayout.start();
    }
    else {
      tgLayout.stop();
    }
    //tgLayout.setLayoutMode( doLt);
  }

  public void layoutOnce() {
    tgLayout.runOnce();
  }

  public GraphEltSet setNewGraphEltSet(int ID) {
    completeEltSet = new GraphEltSet();
    visibleLocality = new VisibleLocality(completeEltSet);
    localityUtils = new LocalityUtils(visibleLocality, this);

    completeEltSetHash.put(String.valueOf(ID), completeEltSet);
    visibleLocalityHash.put(String.valueOf(ID), visibleLocality);
    localityUtilsHash.put(String.valueOf(ID), localityUtils);
    return completeEltSet;

  }

  public void setGraphEltSet(int ID) {
    completeEltSet =
        (GraphEltSet) completeEltSetHash.get(String.valueOf(ID));
    visibleLocality =
        (VisibleLocality) visibleLocalityHash.get(String.valueOf(ID));
    localityUtils =
        (LocalityUtils) localityUtilsHash.get(String.valueOf(ID));

  }

  public GraphEltSet getCompleteEltSet() {
    return completeEltSet;
  }

  public static boolean properContains(
      Rectangle2D.Double fr1,
      Rectangle2D.Double fr2) {
    return fr1.contains(fr2) && !fr2.contains(fr1);
  }

  //returns innermost open cluster that contains point x, y
  public ClusterNode getOpenCluster(double x, double y) {
    ClusterNode cn = null;
    for (int i = 0; i < openClusters.size(); i++) {
      ClusterNode tempCN = (ClusterNode) openClusters.elementAt(i);
      if ( (tempCN.getFrame()).contains(x, y)) {
        if (cn == null) {
          cn = tempCN;
        }
        else if ( (cn.getFrame()).contains(tempCN.getFrame())) {
          cn = tempCN;
        }
      }
    }
    return cn;
  }

  public ClusterNode getOuterCluster(Rectangle2D.Double frame) {
    ClusterNode cn = null;
    for (int i = 0; i < openClusters.size(); i++) {
      ClusterNode tempCN = (ClusterNode) openClusters.elementAt(i);
      if (properContains(tempCN.getFrame(), frame)) {
        if (cn == null) {
          cn = tempCN;
        }
        else if (properContains(cn.getFrame(), tempCN.getFrame())) {
          cn = tempCN;
        }
      }
    }
    return cn;

  }

  public ClusterNode getInnermostCluster(
      double x,
      double y,
      double x1,
      double y1) {
    ClusterNode cn, cn1;
    cn = getOpenCluster(x, y);
    cn1 = getOpenCluster(x1, y1);
    if (cn == null) {
      return cn1;
    }
    if (cn1 == null) {
      return cn;
    }
    if (cn.getFrame().contains(cn1.getFrame())) {
      return cn1;
    }
    if (cn1.getFrame().contains(cn.getFrame())) {
      return cn;
    }
    return null;
  }

  public boolean badCluster(Rectangle2D.Double fr) {
    for (int i = 0; i < openClusters.size(); i++) {
      Rectangle2D.Double test =
          (Rectangle2D.Double) ( (ClusterNode) openClusters.elementAt(i))
          .getFrame();
      if (fr.intersects(test)
          && ! (fr.contains(test) || test.contains(fr))) {
        return true;
      }
    }
    return false;
  }

  //cluster related code, to have clusters working positions should be set befor, so use alwase
  //addNode(node, x, y) while working with clusters
  public void addNode(final Node node, double xc, double yc) throws TGException {
    node.drawx = xc;
    node.drawy = yc;
    updatePosFromDraw(node);
    ClusterNode cnode = getOpenCluster(xc, yc);
    if (cnode != null) {
      node.parentCluster = cnode;
      cnode.addNodeToCluster(node);
    }

    synchronized (localityUtils) {
      visibleLocality.addNode(node);
    }
  }

  public void addClusterElements(GraphEltSet ges) {
    visibleLocality.addCluster(ges);
  }

  public void withdrewClusterElements(GraphEltSet ges) {
    visibleLocality.withdrewCluster(ges);
  }

  public void addOpenCluster(ClusterNode cn) {
    openClusters.addElement(cn);
  }

  public void removeOpenCluster(ClusterNode cn) {
    openClusters.removeElement(cn);
  }

  public void setLensSet(TGLensSet lensSet) {
    tgLensSet = lensSet;
  }

  protected void setTGLayout(TGLayout tgl) {
    tgLayout = tgl;
  }

  public void setGraphEltSet(GraphEltSet ges) {
    completeEltSet = ges;
    visibleLocality = new VisibleLocality(completeEltSet);
    localityUtils = new LocalityUtils(visibleLocality, this);
  }

  public AdjustOriginLens getAdjustOriginLens() {
    return adjustOriginLens;
  }

  public SwitchSelectUI getSwitchSelectUI() {
    return switchSelectUI;
  }

  // color and font setters ......................

  public void setBackColor(Color color) {
    BACK_COLOR = color;
  }

  // Node manipulation ...........................

  /** Returns the current node count. */
  public int getNodeCount() {
    return completeEltSet.nodeCount;
  }

  /** Returns the current node count within the VisibleLocality. */
  public int nodeNum() {
    return visibleLocality.nodeNum();
  }

  /** Return the Node whose ID matches the String <tt>id</tt>, null if no match is found.
   *
   * @param id The ID identifier used as a query.
   * @return The Node whose ID matches the provided 'id', null if no match is found.
   */
  public Node findNode(String id) {
    if (id == null) {
      return null; // ignore
    }
    return completeEltSet.findNode(id);
  }

  public Collection findNodesByLabel(String label) {
    if (label == null) {
      return null; // ignore
    }
    return completeEltSet.findNodesByLabel(label);
  }

  /** Adds a Node, with its ID and label being the current node count plus 1.
   * @see com.touchgraph.graphlayout.Node
   */
  public Node addNode() throws TGException {
    String id = String.valueOf(getNodeCount() + 1);
    return addNode(id, null);
  }

  /** Adds a Node, provided its label.  The node is assigned a unique ID,
       * by appending an underscore followed by an integer until a unique ID is found.
   * @see com.touchgraph.graphlayout.Node
   */
  public Node addNode(String label) throws TGException {
    String id;
    if (findNode(label) != null) {
      id = label;
    }
    else {
      int i;
      for (i = 1; findNode(label + "-" + i) != null; i++) {
        ;
      }
      id = label + "-" + i;
      // if label is a valid XML Name, then so is ID.
    }
    return addNode(id, label);
  }

  /** Adds a Node, provided its ID and label.
   * @see com.touchgraph.graphlayout.Node
   */
  public Node addNode(String id, String label) throws TGException {
    Node node;
    if (label == null) {
      node = new Node(id);
    }
    else {
      node = new Node(id, label);

    }
    updateDrawPos(node);
    // The addNode() call should probably take a position, this just sets it at 0,0
    addNode(node);
    return node;
  }

  /** Add the Node <tt>node</tt> to the visibleLocality, checking for ID uniqueness. */
  public void addNode(final Node node) throws TGException {
    synchronized (localityUtils) {
      visibleLocality.addNode(node);
      resetDamper();
    }
  }

  /** Remove the Node object matching the ID <code>id</code>, returning true if the
   * deletion occurred, false if a Node matching the ID does not exist (or if the ID
   * value was null).
   *
   * @param id The ID identifier used as a query.
   * @return true if the deletion occurred.
   */
  public boolean deleteNodeById(String id) {
    if (id == null) {
      return false; // ignore
    }
    Node node = findNode(id);
    if (node == null) {
      return false;
    }
    else {
      return deleteNode(node);
    }
  }

  public boolean deleteNode(Node node) {
    // ClusterNode cn=node.getParentCluster();
    //  if(cn!=null) cn.removeNodeFromCluster(node);

    synchronized (localityUtils) {
      if (visibleLocality.deleteNode(node)) {
        // delete from visibleLocality, *AND completeEltSet
        if (node == select) {
          clearSelect();
        }
        resetDamper();

        return true;
      }

      return false;
    }
  }

  public void clearAll() {
    synchronized (localityUtils) {
      visibleLocality.clearAll();
    }
  }

  public Node getSelect() {
    return select;
  }

  public Node getMouseOverN() {
    return mouseOverN;
  }

  public synchronized void setMouseOverN(Node node) {
    if (dragNode != null || maintainMouseOver) {
      return; // So you don't accidentally switch nodes while dragging
    }
    if (mouseOverN != node) {
      Node oldMouseOverN = mouseOverN;
      mouseOverN = node;
    }
  }

  // Edge manipulation ...........................

  public void deleteEdge(Edge e) {
    // ClusterNode cn=getInnermostCluster(e.to.drawx, e.to.drawy, e.from.drawx, e.from.drawy);
    // if(cn!=null) cn.removeEdgeFromCluster(e);
    synchronized (localityUtils) {
      visibleLocality.deleteEdge(e);
      resetDamper();
    }
  }

  public void deleteEdge(Node from, Node to) {
    synchronized (localityUtils) {
      visibleLocality.deleteEdge(from, to);
    }
  }

  public int edgeNum() {
    return visibleLocality.edgeNum();
  }

  public Edge findEdge(Node f, Node t) {
    return visibleLocality.findEdge(f, t);
  }

  //only this will work with clusters
  public void addEdge(Edge e) {
    //ClusterNode cn=getInnermostCluster(e.to.drawx, e.to.drawy, e.from.drawx, e.from.drawy);
    if (e.to.parentCluster != null) {
      e.to.parentCluster.addEdgeToCluster(e);
    }
    if (e.from.parentCluster != null) {
      e.from.parentCluster.addEdgeToCluster(e);
    }
    synchronized (localityUtils) {
      visibleLocality.addEdge(e);
      resetDamper();
    }
  }

  public void addEdgeOnly(Edge e) {
    // ClusterNode cn=getInnermostCluster(e.to.drawx, e.to.drawy, e.from.drawx, e.from.drawy);
    // if(cn!=null) cn.addEdgeToCluster(e);
    synchronized (localityUtils) {
      visibleLocality.addEdgeOnly(e);
      resetDamper();
    }
  }

  public Edge addEdge(Node f, Node t, int tens) {
    synchronized (localityUtils) {
      return visibleLocality.addEdge(f, t, tens);
    }
  }

  public Edge getMouseOverE() {
    return mouseOverE;
  }

  public synchronized void setMouseOverE(Edge edge) {
    if (dragNode != null || maintainMouseOver) {
      return; // No funny business while dragging
    }
    if (mouseOverE != edge) {
      Edge oldMouseOverE = mouseOverE;
      mouseOverE = edge;
    }
  }

  // miscellany ..................................

  protected class AdjustOriginLens
      extends TGAbstractLens {
    protected void applyLens(TGPoint2D p) {
      p.x = p.x + TGPanel.this.getSize().width / 2;
      p.y = p.y + TGPanel.this.getSize().height / 2;
    }

    protected void undoLens(TGPoint2D p) {
      p.x = p.x - TGPanel.this.getSize().width / 2;
      p.y = p.y - TGPanel.this.getSize().height / 2;
    }
  }

  public class SwitchSelectUI extends TGAbstractClickUI {
    public void mouseClicked(MouseEvent e) {
      if (mouseOverN != null) {
        if (mouseOverN != select) {
          setSelect(mouseOverN);
        }
        else {
          clearSelect();
        }
      }
    }
  }

  public void fireMovedEvent() {
    Vector listeners;

    synchronized (this) {
      listeners = (Vector) graphListeners.clone();
    }

    for (int i = 0; i < listeners.size(); i++) {
      GraphListener gl = (GraphListener) listeners.elementAt(i);
      gl.graphMoved();
    }
  }

  public void fireResetEvent() {
    Vector listeners;

    synchronized (this) {
      listeners = (Vector) graphListeners.clone();
    }

    for (int i = 0; i < listeners.size(); i++) {
      GraphListener gl = (GraphListener) listeners.elementAt(i);
      gl.graphReset();
    }
  }

  public synchronized void addGraphListener(GraphListener gl) {
    graphListeners.addElement(gl);
  }

  public synchronized void removeGraphListener(GraphListener gl) {
    graphListeners.removeElement(gl);
  }

  public synchronized void addPaintListener(TGPaintListener pl) {
    paintListeners.addElement(pl);
  }

  public synchronized void removePaintListener(TGPaintListener pl) {
    paintListeners.removeElement(pl);
  }

  private void redraw() {
    resetDamper();
  }

  public ImmutableGraphEltSet getGES() {
    return visibleLocality;
  }

  public void setMaintainMouseOver(boolean maintain) {
    maintainMouseOver = maintain;
  }

  public void clearSelect() {
    if (select != null) {
      select = null;
      repaint();
    }
  }

  /** A convenience method that selects the first node of a graph, so that hiding works.
   */
  public void selectFirstNode() {
    setSelect(getGES().getFirstNode());
  }

  public void setSelect(Node node) {
    if (node != null) {
      select = node;
      repaint();
      //System.out.println("Get node when click: " +  node.toString() );
    }
    else if (node == null) {
      clearSelect();
    }
  }

  public void multiSelect(TGPoint2D from, TGPoint2D to) {
    final double minX, minY, maxX, maxY;

    if (from.x > to.x) {
      maxX = from.x;
      minX = to.x;
    }
    else {
      minX = from.x;
      maxX = to.x;
    }
    if (from.y > to.y) {
      maxY = from.y;
      minY = to.y;
    }
    else {
      minY = from.y;
      maxY = to.y;
    }

    final Vector selectedNodes = new Vector();

    TGForEachNode fen = new TGForEachNode() {
      public void forEachNode(Node node) {
        double x = node.drawx;
        double y = node.drawy;
        if (x > minX && x < maxX && y > minY && y < maxY) {
          selectedNodes.addElement(node);
        }
      }
    };

    visibleLocality.forAllNodes(fen);

    if (selectedNodes.size() > 0) {
      int r = (int) (Math.random() * selectedNodes.size());
      setSelect( (Node) selectedNodes.elementAt(r));
    }
    else {
      clearSelect();
    }
  }

  public void updateLocalityFromVisibility() throws TGException {
    visibleLocality.updateLocalityFromVisibility();
  }

  public void setLocale(Node node, int radius) {
    if (radius > 45) {
      restoreAll();
    }
    else {
      localityUtils.setLocale(node, radius);
    }
  }

  public void restoreAll() {
    //localityUtils.restoreAll();
    Vector nodes = completeEltSet.getNodes();
    synchronized (visibleLocality) {
      for (int i = 0; i < nodes.size(); i++) {
        Node n = (Node) nodes.elementAt(i);
        if (!visibleLocality.contains(n)) {
          try {
            n.markedForRemoval = false;
            n.justMadeLocal = false;
            n.isHiden = false;
            visibleLocality.addNodeWithEdges(n);
          }
          catch (TGException tge) {
            System.err.println("TGException: " + tge.getMessage());
          }
        }
      }
    }
  }

  public void expandNode(Node node, NodeFilter filter) {
    localityUtils.expandNode(node, filter);
  }

  public void hideNode(Node hideNode, Node focusNode) {
    localityUtils.hideNode(hideNode, focusNode);
  }

  public void hideEdge(Edge hideEdge) {
    visibleLocality.removeEdge(hideEdge);
    if (mouseOverE == hideEdge) {
      setMouseOverE(null);
    }
    resetDamper();
  }

  public void setDragNode(Node node) {
    dragNode = node;
    tgLayout.setDragNode(node);
  }

  public Node getDragNode() {
    return dragNode;
  }

  void setMousePos(Point p) {
    mousePos = p;
  }

  public Point getMousePos() {
    return mousePos;
  }

  /** Start and stop the damper.  Should be placed in the TGPanel too. */
  public void startDamper() {
    tgLayout.startDamper();
  }

  public void stopDamper() {
    tgLayout.stopDamper();
  }

  /** Makes the graph mobile, and slowly slows it down. */
  public void resetDamper() {
    tgLayout.resetDamper();
  }

  class BasicMouseListener
      extends MouseAdapter {

    public void mouseEntered(MouseEvent e) {
      addMouseMotionListener(basicMML);
    }

    public void mouseExited(MouseEvent e) {
      removeMouseMotionListener(basicMML);
      mousePos = null;
      setMouseOverN(null);
      setMouseOverE(null);
      repaint();
    }
  }

  class BasicMouseMotionListener
      implements MouseMotionListener {
    public void mouseDragged(MouseEvent e) {
      mousePos = e.getPoint();
      findMouseOver();
      try {
        Thread.sleep(6);
        //An attempt to make the cursor flicker less
      }
      catch (InterruptedException ex) {
        //break;
      }
    }

    public void mouseMoved(MouseEvent e) {
      mousePos = e.getPoint();
      synchronized (this) {
        Edge oldMouseOverE = mouseOverE;
        Node oldMouseOverN = mouseOverN;
        findMouseOver();

        if (oldMouseOverE != mouseOverE
            || oldMouseOverN != mouseOverN) {
          repaint();
        }
        // Replace the above lines with the commented portion below to prevent whole graph
        // from being repainted simply to highlight a node On mouseOver.
        // This causes some annoying flickering though.
        /*
             if(oldMouseOverE!=mouseOverE) {
            if (oldMouseOverE!=null) {
                synchronized(oldMouseOverE) {
             oldMouseOverE.paint(TGPanel.this.getGraphics(),TGPanel.this);
             oldMouseOverE.from.paint(TGPanel.this.getGraphics(),TGPanel.this);
             oldMouseOverE.to.paint(TGPanel.this.getGraphics(),TGPanel.this);
                }
            }
            if (mouseOverE!=null) {
                synchronized(mouseOverE) {
                    mouseOverE.paint(TGPanel.this.getGraphics(),TGPanel.this);
             mouseOverE.from.paint(TGPanel.this.getGraphics(),TGPanel.this);
             mouseOverE.to.paint(TGPanel.this.getGraphics(),TGPanel.this);
                }
            }
             }
             if(oldMouseOverN!=mouseOverN) {
            if (oldMouseOverN!=null) oldMouseOverN.paint(TGPanel.this.getGraphics(),TGPanel.this);
             if (mouseOverN!=null) mouseOverN.paint(TGPanel.this.getGraphics(),TGPanel.this);
             }
         */
      }
    }
  }

  protected synchronized void findMouseOver() {

    if (mousePos == null) {
      setMouseOverN(null);
      setMouseOverE(null);
      return;
    }

    final int mpx = mousePos.x;
    final int mpy = mousePos.y;

    final Node[] monA = new Node[1];
    final Edge[] moeA = new Edge[1];

    TGForEachNode fen = new TGForEachNode() {

      double minoverdist = 100;
      //Kind of a hack (see second if statement)
      //Nodes can be as wide as 200 (=2*100)
      public void forEachNode(Node node) {
        if (node.stable) {
          return; //cluster code
        }
        double x = node.drawx;
        double y = node.drawy;

        double dist =
            Math.sqrt( (mpx - x) * (mpx - x) + (mpy - y) * (mpy - y));

        if ( (dist < minoverdist) && node.containsPoint(mpx, mpy)) {
          minoverdist = dist;
          monA[0] = node;
        }
      }
    };
    visibleLocality.forAllNodes(fen);

    TGForEachEdge fee = new TGForEachEdge() {

      double minDist = 8; // Tangential distance to the edge
      double minFromDist = 1000; // Distance to the edge's "from" node

      public void forEachEdge(Edge edge) {
        double x = edge.from.drawx;
        double y = edge.from.drawy;
        double dist = edge.distFromPoint(mpx, mpy);
        if (dist < minDist) {
          // Set the over edge to the edge with the minimun tangential distance
          minDist = dist;
          minFromDist =
              Math.sqrt(
              (mpx - x) * (mpx - x) + (mpy - y) * (mpy - y));
          moeA[0] = edge;
        }
        else if (dist == minDist) {
          // If tangential distances are identical, chose
          // the edge whose "from" node is closest.
          double fromDist =
              Math.sqrt(
              (mpx - x) * (mpx - x) + (mpy - y) * (mpy - y));
          if (fromDist < minFromDist) {
            minFromDist = fromDist;
            moeA[0] = edge;
          }
        }
      }
    };
    visibleLocality.forAllEdges(fee);

    setMouseOverN(monA[0]);
    if (monA[0] == null) {
      setMouseOverE(moeA[0]);
    }
    else {
      setMouseOverE(null);
    }
  }

  TGPoint2D topLeftDraw = null;
  TGPoint2D bottomRightDraw = null;

  public TGPoint2D getTopLeftDraw() {
    return new TGPoint2D(topLeftDraw);
  }

  public TGPoint2D getBottomRightDraw() {
    return new TGPoint2D(bottomRightDraw);
  }

  public TGPoint2D getCenter() {
    return tgLensSet.convDrawToReal(
        getSize().width / 2,
        getSize().height / 2);
  }

  public TGPoint2D getDrawCenter() {
    return new TGPoint2D(getSize().width / 2, getSize().height / 2);
  }

  public void updateGraphSize() {
    if (topLeftDraw == null) {
      topLeftDraw = new TGPoint2D(0, 0);
    }
    if (bottomRightDraw == null) {
      bottomRightDraw = new TGPoint2D(0, 0);

    }
    TGForEachNode fen = new TGForEachNode() {
      boolean firstNode = true;
      public void forEachNode(Node node) {
        if (firstNode) { //initialize topRight + bottomLeft
          topLeftDraw.setLocation(node.drawx, node.drawy);
          bottomRightDraw.setLocation(node.drawx, node.drawy);
          firstNode = false;
        }
        else { //Standard max and min finding
          topLeftDraw.setLocation(
              Math.min(node.drawx, topLeftDraw.x),
              Math.min(node.drawy, topLeftDraw.y));
          bottomRightDraw.setLocation(
              Math.max(node.drawx, bottomRightDraw.x),
              Math.max(node.drawy, bottomRightDraw.y));
        }
      }
    };

    visibleLocality.forAllNodes(fen);
  }

  public synchronized void processGraphMove() {
    updateDrawPositions();
    updateGraphSize();
  }

  public synchronized void repaintAfterMove() { // Called by TGLayout + others to indicate that graph has moved
    processGraphMove();
    findMouseOver();
    fireMovedEvent();
    repaint();
  }

  public void updateDrawPos(Node node) { //sets the visual position from the real position
    TGPoint2D p = tgLensSet.convRealToDraw(node.x, node.y);
    node.drawx = p.x;
    node.drawy = p.y;

    if (node instanceof ClusterNode) {
      Rectangle2D.Double realFrame = ( (ClusterNode) node).getRealFrame();
      TGPoint2D drawTopRight =
          tgLensSet.convRealToDraw(
          node.x + realFrame.width / 2,
          node.y + realFrame.height / 2);
      ( (ClusterNode) node).setDrawTopRight(
          drawTopRight.x,
          drawTopRight.y);
    }
  }

  public void updatePosFromDraw(Node node) { //sets the real position from the visual position
    TGPoint2D p = tgLensSet.convDrawToReal(node.drawx, node.drawy);
    node.x = p.x;
    node.y = p.y;

    if (node instanceof ClusterNode) {
      Rectangle2D.Double drawFrame = ( (ClusterNode) node).getFrame();
      TGPoint2D topright =
          tgLensSet.convDrawToReal(
          node.drawx + drawFrame.width / 2,
          node.drawy + drawFrame.height / 2);
      ( (ClusterNode) node).setTopRight(topright.x, topright.y);
    }

  }

  public void updateDrawPositions() {
    TGForEachNode fen = new TGForEachNode() {
      public void forEachNode(Node node) {
        updateDrawPos(node);
      }
    };
    visibleLocality.forAllNodes(fen);
  }

  Color myBrighter(Color c) {
    int r = c.getRed();
    int g = c.getGreen();
    int b = c.getBlue();

    r = Math.min(r + 96, 255);
    g = Math.min(g + 96, 255);
    b = Math.min(b + 96, 255);

    return new Color(r, g, b);
  }

  public synchronized void paint(Graphics g) {
    update(g);
  }

  public synchronized void update(Graphics g) {
    Dimension d = getSize();
    if ( (offscreen == null)
        || (d.width != offscreensize.width)
        || (d.height != offscreensize.height)) {
      offscreen = createImage(d.width, d.height);
      offscreensize = d;
      offgraphics = offscreen.getGraphics();

      processGraphMove();
      findMouseOver();
      fireMovedEvent();
    }

    offgraphics.setColor(BACK_COLOR);
    offgraphics.fillRect(0, 0, d.width, d.height);

    synchronized (this) {
      paintListeners = (Vector) paintListeners.clone();
    }

    for (int i = 0; i < paintListeners.size(); i++) {
      TGPaintListener pl = (TGPaintListener) paintListeners.elementAt(i);
      pl.paintFirst(offgraphics);
    }

    TGForEachEdge fee = new TGForEachEdge() {
      public void forEachEdge(Edge edge) {
        edge.paint(offgraphics, TGPanel.this);
      }
    };

    visibleLocality.forAllEdges(fee);

    for (int i = 0; i < paintListeners.size(); i++) {
      TGPaintListener pl = (TGPaintListener) paintListeners.elementAt(i);
      pl.paintAfterEdges(offgraphics);
    }

    TGForEachNode fen = new TGForEachNode() {
      public void forEachNode(Node node) {
        node.paint(offgraphics, TGPanel.this);
      }
    };

    visibleLocality.forAllNodes(fen);

    if (mouseOverE != null) {
      //Make the edge the mouse is over appear on top.
      mouseOverE.paint(offgraphics, this);
      mouseOverE.from.paint(offgraphics, this);
      mouseOverE.to.paint(offgraphics, this);
    }

    if (select != null) { //Make the selected node appear on top.
      select.paint(offgraphics, this);
    }

    if (mouseOverN != null) {
      //Make the node the mouse is over appear on top.
      mouseOverN.paint(offgraphics, this);
    }

    for (int i = 0; i < paintListeners.size(); i++) {
      TGPaintListener pl = (TGPaintListener) paintListeners.elementAt(i);
      pl.paintLast(offgraphics);
    }

    paintComponents(offgraphics);
    //Paint any components that have been added to this panel
    g.drawImage(offscreen, 0, 0, null);

  }

  /**
   * @return
   */
  public LocalityUtils getLocalityUtils() {
    return localityUtils;
  }

  /**
   * @return
   */
  public VisibleLocality getVisibleLocality() {
    return visibleLocality;
  }

} //  @jve:visual-info  decl-index=0 visual-constraint="10,10" end com.touchgraph.graphlayout.TGPanel
