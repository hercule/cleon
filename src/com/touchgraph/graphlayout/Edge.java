/*
 * TouchGraph LLC. Apache-Style Software License
 *
 *
 * Copyright (c) 2002 Alexander Shapiro. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by
 *        TouchGraph LLC (http://www.touchgraph.com/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "TouchGraph" or "TouchGraph LLC" must not be used to endorse
 *    or promote products derived from this software without prior written
 *    permission.  For written permission, please contact
 *    alex@touchgraph.com
 *
 * 5. Products derived from this software may not be called "TouchGraph",
 *    nor may "TouchGraph" appear in their name, without prior written
 *    permission of alex@touchgraph.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL TOUCHGRAPH OR ITS CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 */

package com.touchgraph.graphlayout;

import java.awt.*;
import java.awt.geom.*;

/**  Edge.
  *
  * @author   Alexander Shapiro
 * @author   Serguei Krivov - support for , arrows , styles and tiles
  * @version  1.20
  */
public class Edge {

	public static Color DEFAULT_COLOR = Color.decode("#0000B0");
	public static Color MOUSE_OVER_COLOR = Color.pink;
	public static int DEFAULT_LENGTH = 40;

	public final static int BIDIRECTIONAL_EDGE = 0;
	public final static int HIERARCHICAL_EDGE = 1;
	public final static int ARROW_EDGE = 2;
	public final static int DOUBLEARROW_EDGE = 3;
	public final static int CIRCLE_EDGE = 4;

	public final static int STYLE_NORMAL = 0;
	public final static int STYLE_THICK = 1;
	public final static int STYLE_DASHED = 2;

	public final static int TILE_NONE = 0;
	public final static int TILE_DOT = 1;
	public final static int TILE_ARROW = 2;
	public final static int TILE_DOUBLEARROW = 3;

	public static int DEFAULT_TYPE = 0;

	public final static BasicStroke normalStroke = new BasicStroke(1.0f);
	public final static BasicStroke wideStroke =
		new BasicStroke(2.0f);//BasicStroke(3.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);

	public final static float dash1[] = { 10.0f, 4.0f };
	public final static BasicStroke dashed =
		new BasicStroke(
			2.0f,
			BasicStroke.CAP_BUTT,
			BasicStroke.JOIN_MITER,
			10.0f,
			dash1,
			0.0f);
	public final static double CIRCLE_RADIUS = 4.0;
	public final static double ARROW_SIZE = 8.0;

	public Node from; //Should be private, changing from effects "from" Node
	public Node to; //Should be private, changing from effects "to" Node
	public Color col;
	protected int length;
	protected boolean visible;
  public int distance = 1; // distance between connected nodes when computing graph structure

	public int type = 1;
	public int edgeStyle = 0;
	public int edgeTile = 0;

	protected BasicStroke stroke;

	//geometrical constants for drawing arrows
	public static double r = CIRCLE_RADIUS;
	//length of arrow
	public static double arr = 10;
	//width of arrow
	public static int arw = 3;
	//width of hierarchical arrow
	public static double hw = 3.2;
	// ............

	/** Constructor with two Nodes and a length.
	  */
	public Edge(Node f, Node t, int len) {
		from = f;
		to = t;
		length = len;
		col = DEFAULT_COLOR;
		stroke = normalStroke;
	}

        public Edge(Node f, Node t, int len, Color c) {
                from = f;
                to = t;
                length = len;
                this.setEdgeDefaultColor(c);
                col = DEFAULT_COLOR;
                stroke = normalStroke;
        }


	/** Constructor with two Nodes, which uses a default length.
	  */
	public Edge(Node f, Node t) {
		this(f, t, DEFAULT_LENGTH, null);
	}

	// setters and getters ...............

	public static void setEdgeDefaultColor(Color color) {
		DEFAULT_COLOR = color;
               // DEFAULT_COLOR = Color.RED;
	}
	public static void setEdgeMouseOverColor(Color color) {
		MOUSE_OVER_COLOR = color;
	}
	public static void setEdgeDefaultLength(int length) {
		DEFAULT_LENGTH = length;
	}

	public void setType(int t) {
		type = t;
	}

	public static void setEdgeDafaultType(int type) {
		DEFAULT_TYPE = type;
	}

	public int getType() {
		return type;
	}

	public BasicStroke getStroke() {
		return stroke;
	}

	public void setStroke(BasicStroke s) {
		stroke = s;
	}

	public void setStyle(int edStyle) {
		if (edStyle == Edge.STYLE_THICK)
			stroke = wideStroke;
		else if (edStyle == Edge.STYLE_DASHED)
			stroke = wideStroke;
		else
			stroke = normalStroke;
	}

	/** Returns the starting "from" node of this edge as Node. */
	public Node getFrom() {
		return from;
	}

	/** Returns the terminating "to" node of this edge as Node. */
	public Node getTo() {
		return to;
	}

	/** Returns the color of this edge as Color. */
	public Color getColor() {
		return col;
	}

	/** Set the color of this Edge to the Color <tt>color</tt>. */
	public void setColor(Color color) {
		col = color;
	}

	/** Returns the length of this Edge as a double. */
	public int getLength() {
		return length;
	}

	public double getDrawLength() {
		return Math.sqrt(
			(to.drawx - from.drawx) * (to.drawx - from.drawx)
				+ (to.drawy - from.drawy) * (to.drawy - from.drawy));
	}
	/** Set the length of this Edge to the int <tt>len</tt>. */
	public void setLength(int len) {
		length = len;
	}

	/** Set the visibility of this Edge to the boolean <tt>v</tt>. */
	public void setVisible(boolean v) {
		visible = v;
	}

	/** Return the visibility of this Edge as a boolean. */
	public boolean isVisible() {
		return visible;
	}

	public Node getOtherEndpt(Node n) { //yields false results if Node n is not an endpoint
		if (to != n)
			return to;
		else
			return from;
	}

	/** Switches the endpoints of the edge */
	public void reverse() {
		Node temp = to;
		to = from;
		from = temp;
	}

	public boolean intersects(Dimension d) {
		int x1 = (int) from.drawx;
		int y1 = (int) from.drawy;
		int x2 = (int) to.drawx;
		int y2 = (int) to.drawy;

		return (
			((x1 > 0 || x2 > 0) && (x1 < d.width || x2 < d.width))
				&& ((y1 > 0 || y2 > 0) && (y1 < d.height || y2 < d.height)));

	}

	public double distFromPoint(double px, double py) {
		double x1 = from.drawx;
		double y1 = from.drawy;
		double x2 = to.drawx;
		double y2 = to.drawy;

		if (px < Math.min(x1, x2) - 8
			|| px > Math.max(x1, x2) + 8
			|| py < Math.min(y1, y2) - 8
			|| py > Math.max(y1, y2) + 8)
			return 1000;

		double dist = 1000;
		if (x1 - x2 != 0)
			dist = Math.abs((y2 - y1) / (x2 - x1) * (px - x1) + (y1 - py));
		if (y1 - y2 != 0)
			dist =
				Math.min(
					dist,
					Math.abs((x2 - x1) / (y2 - y1) * (py - y1) + (x1 - px)));

		return dist;
	}

	public boolean containsPoint(double px, double py) {
		return distFromPoint(px, py) < 10;
	}

	//depricated function from old release
	public static void paintArrow(
		Graphics g,
		int x1,
		int y1,
		int x2,
		int y2,
		Color c) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setPaint(c);

		double dely = y2 - y1;
		if (Math.abs(dely) > 1) {
			double k = - (x2 - x1) / dely;
			double dx = hw / Math.sqrt(1 + k * k);
			double dy = k * dx;
			int idy = (int) dy;
			int idx = (int) dx;
			int[] x3Points = { x1 + idx, x1 - idx, x2 };
			int[] y3Points = { y1 + idy, y1 - idy, y2 };
			g2.fill(new Polygon(x3Points, y3Points, 3));
		} else {
			int[] x3Points = { x1, x1, x2 };
			int[] y3Points = { y1 + (int) hw, y1 - (int) hw, y2 };
			g2.fill(new Polygon(x3Points, y3Points, 3));
		}
	}

	public void paintBidirectionalEdge(Graphics2D g2) {
		g2.draw(new Line2D.Double(from.drawx, from.drawy, to.drawx, to.drawy));
	}

	public void paintHierarchicalEdge(Graphics2D g2) {

		int x1 = (int) from.drawx;
		int y1 = (int) from.drawy;
		TGPoint2D p = to.getIntersection(from.drawx, from.drawy, 0);
		int x2 = (int) p.x;
		int y2 = (int) p.y;
		double dely = p.y - from.drawy;
		if (Math.abs(dely) > 0.01) {
			double k = - (p.x - from.drawx) / dely;
			double dx = hw / Math.sqrt(1 + k * k);
			double dy = k * dx;
			int idy = (int) dy;
			int idx = (int) dx;
			int[] x3Points = { x1 + idx, x1 - idx, x2 };
			int[] y3Points = { y1 + idy, y1 - idy, y2 };
			g2.fill(new Polygon(x3Points, y3Points, 3));
		} else {
			int[] x3Points = { x1, x1, x2 };
			int[] y3Points = { y1 + (int) hw, y1 - (int) hw, y2 };
			g2.fill(new Polygon(x3Points, y3Points, 3));
		}

	}

	public void paintArrowEdge(Graphics2D g2) {
		g2.draw(new Line2D.Double(from.drawx, from.drawy, to.drawx, to.drawy));
		paintArrow(g2, from, to, arr);
	}

	public void paintDoubleArrowEdge(Graphics2D g2) {
		g2.draw(new Line2D.Double(from.drawx, from.drawy, to.drawx, to.drawy));
		paintDoubleArrow(g2, from, to, arr * 0.8);
	}

	public void paintCircleEdge(Graphics2D g2) {
		g2.draw(new Line2D.Double(from.drawx, from.drawy, to.drawx, to.drawy));
		paintCircle(g2, from, to);
	}

	public void paintArrow(
		Graphics2D g2,
		Node fromnode,
		Node tonode,
		double rs) {
		double x1 = fromnode.drawx;
		double y1 = fromnode.drawy;
		TGPoint2D p = tonode.getIntersection(x1, y1, 0);
		double x2 = p.x;
		double y2 = p.y;
		double l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));

		double xa = x2 - rs * (x2 - x1) / l;
		double ya = y2 - rs * (y2 - y1) / l;

		if (Math.abs(y2 - y1) > 1) {
			double k = (x2 - x1) / (y2 - y1);
			double dx = arw / Math.sqrt(1 + k * k);
			double dy = k * dx;

			/*
			 * for alternative painting you may decomment these lines and comment out lines below
			g2.draw(new Line2D.Double(xa+dx ,ya-dy, x2, y2));
			g2.draw(new Line2D.Double(xa-dx ,ya+dy,    x2, y2));
			 */
			double ix2 = xa + dx;
			double iy2 = ya - dy;
			double ix3 = xa - dx;
			double iy3 = ya + dy;
			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) ix2, (int) iy2);
			arrow.addPoint((int) ix3, (int) iy3);
			g2.fill(arrow);

		} else {
			/*
			 ** for alternative painting you may decomment these lines and comment out lines below
			g2.draw(new Line2D.Double(xa, ya+arw, x2, y2));
			g2.draw(new Line2D.Double(xa, ya-arw,   x2, y2));
			 */

			double iy2 = ya + arw;
			double iy3 = ya - arw;
			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) xa, (int) iy2);
			arrow.addPoint((int) xa, (int) iy3);
			g2.fill(arrow);
		}

	}

	public void paintDoubleArrow(
		Graphics2D g2,
		Node fromnode,
		Node tonode,
		double rs) {
		double x1 = fromnode.drawx;
		double y1 = fromnode.drawy;
		TGPoint2D p = tonode.getIntersection(x1, y1, 0);
		double x2 = p.x;
		double y2 = p.y;

		double l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));

		double shiftx = rs * (x2 - x1) / l;
		double shifty = rs * (y2 - y1) / l;
		double xa = x2 - shiftx;
		double ya = y2 - shifty;
		double xb = xa - shiftx * 0.8;
		double yb = ya - shifty * 0.8;

		if (Math.abs(y2 - y1) > 1) {
			double k = (x2 - x1) / (y2 - y1);
			double dx = 4 / Math.sqrt(1 + k * k);
			double dy = k * dx;

			/*
			 *for alternative painting you may decomment these lines and comment out lines below
			g2.draw(new Line2D.Double(xa+dx ,ya-dy, x2, y2));
			g2.draw(new Line2D.Double(xa-dx ,ya+dy,    x2, y2));
			g2.draw(new Line2D.Double(xb+dx ,yb-dy, x2-shiftx , y2-shifty ));
			g2.draw(new Line2D.Double(xb-dx ,yb+dy,    x2-shiftx , y2-shifty ));
			 */
			double ix2 = xa + dx;
			double iy2 = ya - dy;

			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) ix2, (int) iy2);
			ix2 = xa - dx;
			iy2 = ya + dy;
			arrow.addPoint((int) ix2, (int) iy2);
			g2.fill(arrow);
			//second arrow
			arrow = new Polygon();
			ix2 = x2 - shiftx * 0.8;
			iy2 = y2 - shifty * 0.8;
			arrow.addPoint((int) ix2, (int) iy2);
			ix2 = xb + dx;
			iy2 = yb - dy;
			arrow.addPoint((int) ix2, (int) iy2);
			ix2 = xb - dx;
			iy2 = yb + dy;
			arrow.addPoint((int) ix2, (int) iy2);
			g2.fill(arrow);

		} else {
			/*
			 *for alternative painting you may decomment these lines and comment out lines below
			g2.draw(new Line2D.Double(xa, ya+1.5, x2, y2));
			g2.draw(new Line2D.Double(xa, ya-1.5,   x2, y2));

			g2.draw(new Line2D.Double(xb, yb+1.5, x2-shiftx , y2-shifty ));
			g2.draw(new Line2D.Double(xb, yb-1.5,  x2-shiftx , y2-shifty ));
			 */
			double iy2 = ya + 2;
			double iy3 = ya - 2;
			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) xa, (int) iy2);
			arrow.addPoint((int) xa, (int) iy3);
			g2.fill(arrow);

			//second arrow
			arrow.reset();
			double ix2 = x2 - shiftx * 0.8;
			iy2 = y2 - shifty * 0.8;
			arrow.addPoint((int) ix2, (int) iy2);
			iy2 = yb + 4;
			arrow.addPoint((int) xb, (int) iy2);
			iy2 = yb - 4;
			arrow.addPoint((int) xb, (int) iy2);
			g2.fill(arrow);

		}

	}

	public void paintCircle(Graphics2D g2, Node fromnode, Node tonode) {
		TGPoint2D p = tonode.getIntersection(fromnode.drawx, fromnode.drawy, 3);
		g2.fill(new Ellipse2D.Double(p.x - r, p.y - r, 2 * r, 2 * r));
	}

	public void paint(Graphics g, TGPanel tgPanel) {
		if (!intersects(tgPanel.getSize()))
			return;
		Graphics2D g2 = (Graphics2D) g;
		Color c = (tgPanel.getMouseOverE() == this) ? MOUSE_OVER_COLOR : col;
		g2.setPaint(c);
		g2.setStroke(stroke);

		switch (type) {
			case BIDIRECTIONAL_EDGE :
				paintBidirectionalEdge(g2);
				break;
			case HIERARCHICAL_EDGE :
				paintHierarchicalEdge(g2);
				break;
			case ARROW_EDGE :
				paintArrowEdge(g2);
				break;
			case DOUBLEARROW_EDGE :
				paintDoubleArrowEdge(g2);
				break;
			case CIRCLE_EDGE :
				paintCircleEdge(g2);
				break;
		}

		switch (edgeTile) {
			case TILE_NONE :
				break;
			case TILE_DOT :
				paintCircle(g2, to, from);
				break;
			case TILE_ARROW :
				paintArrow(g2, to, from, arr);
				break;
			case TILE_DOUBLEARROW :
				paintDoubleArrow(g2, to, from, arr * 0.5);
				break;

		}
	}

	public static void paintGhost(
		Graphics g,
		double x1,
		double y1,
		double x2,
		double y2,
		Color c) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setPaint(c);
		g2.setStroke(dashed);
		g2.draw(new Line2D.Double(x1, y1, x2, y2));

		double l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		double xa = x2 - arr * (x2 - x1) / l;
		double ya = y2 - arr * (y2 - y1) / l;

		if (Math.abs(y2 - y1) > 1) {
			double k = (x2 - x1) / (y2 - y1);
			double dx = arw / Math.sqrt(1 + k * k);
			double dy = k * dx;

			/*
			g2.draw(new Line2D.Double(xa+dx ,ya-dy, x2, y2));
			g2.draw(new Line2D.Double(xa-dx ,ya+dy,    x2, y2));
			 */
			double ix2 = xa + dx;
			double iy2 = ya - dy;
			double ix3 = xa - dx;
			double iy3 = ya + dy;
			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) ix2, (int) iy2);
			arrow.addPoint((int) ix3, (int) iy3);
			g2.fill(arrow);

		} else {
			/*
			g2.draw(new Line2D.Double(xa, ya+arw, x2, y2));
			g2.draw(new Line2D.Double(xa, ya-arw,   x2, y2));
			 */

			double iy2 = ya + arw;
			double iy3 = ya - arw;
			Polygon arrow = new Polygon();
			arrow.addPoint((int) x2, (int) y2);
			arrow.addPoint((int) xa, (int) iy2);
			arrow.addPoint((int) xa, (int) iy3);
			g2.fill(arrow);
		}
	}

} // end com.touchgraph.graphlayout.Edge
