/*
 * ClusterNode.java
 *
 * Created on June 9, 2003, 2:39 PM
 */

package com.touchgraph.graphlayout;

//import  com.touchgraph.graphlayout.interaction.*;
import  com.touchgraph.graphlayout.graphelements.*;

import java.awt.*;
import java.awt.geom.*;

import  java.util.Vector;
 

/**
 *This class handles clusters. It stores cluster structure, adds it to tgPanel when cluster opens, it withdrews it
 * from tgPanel when cluster clouses. When cluster is closed it behaves as ordinary node.
 * @author  serguei krivov
 */
public class ClusterNode extends Node {
    public static int DEFW= 300;
    public static int DEFH= 200;
    
    public static int FROMTHIS=0;
    public static int TOTHIS=1;
    public static int INTERNAL=2;
    public static int FOREIGN=3;
    
    public static Color FRAMECOLOR = Color.black; 
    //triggers the fold / unfold state 
    public boolean open;
    //frame or window parameters
    //protected Rectangle2D.Double frame;
    
    public double CH=DEFH;
    public double CW=DEFW;
    
    public double memx, memy;     //???????
    
    protected Point2D.Double drawTopRight;
    public  Point2D.Double topRight;
     protected Point2D.Double memTopRight;
    
    //graph elements that are part of the cluster
    protected GraphEltSet gEltSet;
    public GraphEltSet getGraphEltSet() {
        return gEltSet;
    }
    //this is needed to handle transcluster edges, those that should remain visible in some way ,when cluster is collapsed
    static Vector edgeMap=new Vector();
    
    /** Creates a new instance of ClusterNode  */
    public ClusterNode( String id, boolean Opn) {
        super(id);
        open=Opn;
        
        initialize();
         
    }
    
    
    
    public ClusterNode( String id, String label,  boolean Opn  )
    {
         super(id, label);
         open=Opn;
         initialize( );
          
    }
    
    protected void initialize( ) {
        stable=open;
       gEltSet= new GraphEltSet();
     //   frame = new Rectangle2D.Double(); 
       
       // setDefaultFrame(0, 0);
        typ = TYPE_ROUNDRECT;
        font=TEXT_FONT;
         
    }
    
    /*
    public void setDefaultFrame(int x, int y ){
        frame.setRect(  x-DEFW,   y-DEFH,  DEFW,   DEFH) ;
        drawx= x;
        drawy= y;
    }*/
    
    public void setFrame(Rectangle2D.Double rect){
          
         drawx=  rect.getCenterX();
         drawy=   rect.getCenterY();
         CW=rect.width ;
         CH=rect.height ;
         drawTopRight= new Point2D.Double(drawx+rect.width/2, drawy+rect.height/2);
         
         
       //  x=  rect.getCenterX();
        // y=  rect.getCenterY();
    }
    
    public Rectangle2D.Double getFrame(){
       Rectangle2D.Double frame =new Rectangle2D.Double();
       frame.setFrameFromCenter(new Point2D.Double(drawx, drawy), drawTopRight); 

//setRect(drawx-CW/2, drawy-CH/2, CW, CH);
       return frame; 
    }
    
    public void setTopRight(double x, double y) {
        topRight= new Point2D.Double(x, y);
    }
    
    public void setDrawTopRight(double x, double y) {
        drawTopRight= new Point2D.Double(x, y);
    }
    
    public Rectangle2D.Double getRealFrame(){
       Rectangle2D.Double frame =new Rectangle2D.Double();
       frame.setFrameFromCenter(new Point2D.Double( x,  y), topRight); 
     //  frame.setRect( x-CW/2,  y-CH/2, CW, CH);
       return frame; 
    }
    /*
     public Rectangle2D.Double getFrame2(){
       Rectangle2D.Double frame =new Rectangle2D.Double();
       frame.setRect(drawx-CW/2, drawy-CW/2, CW, CH);
       return frame; 
    }*/
    public boolean frameContainsPoint(int x, int y) {
        return getFrame().contains(x,y);
    }
   /* //checks if the edge connects this cluster with a node outside of this cluster 
    public boolean isExternal(Edge e) {
        boolean toIsHere=e.to.parentCluster==this;  
        boolean fromIsHere= e.from.parentCluster==this;// 
        
        boolean toIsExternal= !e.to.hasSuperCluster(this);
        boolean fromIsExternal= !e.from.hasSuperCluster(this);
        
        return ((toIsHere && fromIsExternal) || (fromIsHere && toIsExternal));             //just to show it clearly
    } 
    
    
    public boolean isExternal(Edge e) {
         boolean toIsHere=e.to.parentCluster==this;  
        boolean fromIsHere= e.from.parentCluster==this;
        return ((toIsHere && !fromIsHere) || (fromIsHere && !toIsHere)); 
    }*/
    
    
     public boolean isTranscluster(Edge e) {
          
         return e.to.parentCluster!=e.from.parentCluster;
    }
      
      
   //stores   node inside the cluster,   to keep tack of them
    public void addNodeToCluster(Node n){
      if(!gEltSet.contains(n)) {
        try {
            n.parentCluster=this;
            gEltSet.addNode(n);
         
        }
        catch ( TGException tge ) {
                System.err.println(tge.getMessage());
                tge.printStackTrace(System.err);
        }
      }
    }
    
    
    //stores   edge inside the cluster or connected to cluster
    //  asumed that cluster is open
     public void addEdgeToCluster(Edge e){
        if(!open || gEltSet.contains(e)) return; 
        if( findByOriginal(  e)!=null) {System.err.println( "Found by original ");  return;}
         if( findByCurrent(  e)!=null)  {System.err.println( "Found by current ");  return;}
       
        if(isTranscluster(e) ) { 
                    
             e.setLength((int)(CW +CH)/2);
            EdgeMap1 ep = new EdgeMap1( e);
            edgeMap.addElement(ep);
            
            
           // System.err.println( "Added Edge  from ");
           // System.err.println(  e.from.getLabel());
           // System.err.println( " to ");
           // System.err.println(  e.to.getLabel());
        } 
        else
         gEltSet.addEdgeOnly(e);
    }
     
        //removes node inside the cluster
    public boolean removeNodeFromCluster(Node node){
      node.parentCluster=null;  
        for ( int i = 0 ; i < node.edgeNum(); i++ ) {
            Edge e = node.edgeAt(i);
            removeEdgeFromCluster(  e); 
            
      } 
      return gEltSet.deleteNode(node); 
      
    }
    
    
     
    //removes edge inside the cluster
     public boolean removeEdgeFromCluster(Edge e) {
      if(isTranscluster(e)) {
         
       return  edgeMap.removeElement(findByOriginal(e))||edgeMap.removeElement(findByCurrent(e));//????
            
      } else
      return gEltSet.deleteEdgeOnly(e);
    }
    
    
    
    public void unfoldCluster(final TGPanel tgPanel) {
        open=true;
        stable=true;
         tgPanel.clearSelect(); //It does not work without this and I do not know why
         x =memx;  
         y=memy; 
         topRight=memTopRight;
         tgPanel.updateDrawPos(this);
           /*
         ClusterNode parent=getParentCluster();
        if(parent!=null) {
           x =memx+parent.x;  
           y=memy+parent.y;  
        }
        else
        {
           x =memx;  
           y=memy;  
        }
        topRight.x+=x; topRight.y+=y;
        tgPanel.updateDrawPos(this);
         
         TGForEachNode fen = new TGForEachNode() {
            public void forEachNode( Node node ) {
                 node.x+=x;
                 node.y+=y;
                 tgPanel.updateDrawPos(node);
            }
        };

        gEltSet.forAllNodes(fen);
         
       */
         
        tgPanel.addClusterElements(gEltSet);
         
        //map external edges, just reconnect them to this object 
         tgPanel.addOpenCluster(this);
          
         //System.err.println("unfolding....map size");
         // System.err.println(  String.valueOf( edgeMap.size()));
          
         for(int i=0; i<edgeMap.size(); i++) {
            EdgeMap1 em1= (EdgeMap1) edgeMap.elementAt(i);
            int relation=relationToThis(em1);
            if(relation==FROMTHIS || relation ==TOTHIS){
               tgPanel.deleteEdge( em1.currentEdge  );
               tgPanel.addEdge(getEdgeUnfold(em1,relation)); 
            }
            else if(relation==INTERNAL)
            {
                if((em1.currentEdge.to.parentCluster==this) && (em1.currentEdge.from.parentCluster==this))
                    tgPanel.addEdge(em1.currentEdge);  
            }
         }
                 
    }
         
    public void  collapsCluster(final TGPanel tgPanel) {
       tgPanel.clearSelect();
      if(open==false) return;
      
        Vector clOpenClusters= (Vector) tgPanel.openClusters.clone();
        
        for(int i=0;i< clOpenClusters.size(); i++) {
          ClusterNode tcn= (ClusterNode)clOpenClusters.elementAt(i);
          if(tcn.parentCluster==this)
              tcn.collapsCluster(tgPanel);
          
      } 
         tgPanel.updatePosFromDraw(this);
         memx=x;  
          memy=y;
          memTopRight=topRight;
        /*
         topRight.x-=x; topRight.y-=y;
        ClusterNode parent=getParentCluster();
        if(parent!=null) {
          memx=x-parent.x;  
          memy=y-parent.y;  
        }
        else
        {
          memx=x;  
          memy=y; 
        }
         **/
            
       open=false; 
       stable=false;
       /*
        TGForEachNode fen = new TGForEachNode() {
            public void forEachNode( Node node ) {
                 node.x-=x;
                 node.y-=y;
                 tgPanel.updateDrawPos(node);
            }
        };

        gEltSet.forAllNodes(fen); */
         
         
          // System.err.println("folding....map size");
          //System.err.println(  String.valueOf( edgeMap.size()));
       tgPanel.removeOpenCluster(this);
       //map external edges
        for(int i=0; i<edgeMap.size(); i++) {
            EdgeMap1 em1= (EdgeMap1) edgeMap.elementAt(i);
            int relation=relationToThis(em1);
            if(relation==FROMTHIS || relation ==TOTHIS){
               tgPanel.deleteEdge( em1.currentEdge  );
               tgPanel.addEdge(getEdgeCollaps(em1,relation)); 
            }
            else if(relation==INTERNAL)
            {
                if((em1.currentEdge.to.parentCluster==this) && (em1.currentEdge.from.parentCluster==this))
                        tgPanel.deleteEdge(em1.currentEdge);  
            } 
        }
        tgPanel.withdrewClusterElements(gEltSet);
         
        
    }
    
    /** Paints the Node. */
    public void paint( Graphics g, TGPanel tgPanel ) {
       if(!open){
             super.paintNodeBody(g, tgPanel);
            
       }
        else
        {
           Graphics2D g2 = (Graphics2D) g;
           g2.setPaint(FRAMECOLOR);
           g2.setStroke(new BasicStroke(1.0f));
           Rectangle2D.Double frm= getFrame();
           g2.draw(frm);
          
           g2.setFont(font);
           fontMetrics = g2.getFontMetrics(); 
           
           g2.setPaint( Color.white);
           
           int fh=fontMetrics.getHeight();
           g2.fill(new Rectangle2D.Double( frm.x +5,  frm.y-2* fh/3, fontMetrics.stringWidth(lbl), fh));
               
	   Color textCol = getPaintTextColor(tgPanel);
	   g2.setColor(textCol);
           g2.drawString(lbl, (int)frm.x +5 , (int)frm.y+ fh/3 );
        }
             
    }
    
    protected class EdgeMap1 {
       
        
       
       public  Node from;
       public  Node to;
       public  Edge currentEdge;
         
       public int openType;
        public int collapsedType=Edge.BIDIRECTIONAL_EDGE;
        
       int length;
       /*public int thisClosedType=Edge.BIDIRECTIONAL_EDGE;
       public int thatClosedType=Edge.BIDIRECTIONAL_EDGE;
       public int bothClosedType=Edge.BIDIRECTIONAL_EDGE;
        **/
       public String edgeClass; //to enable Mapping of edges according to rules. not used in default implementation 
       
       public EdgeMap1(  Edge e) {
           from=e.from;
           to=e.to;
           currentEdge=e;
           openType=e.type;
           length=e.length;
       }
    }
   
    //search edgeMap
    public EdgeMap1 findByOriginal(Edge e) {
        for(int i=0; i<edgeMap.size(); i++){
           EdgeMap1 ep= (EdgeMap1) edgeMap.elementAt(i); 
           if(ep.from==e.from && ep.to==e.to && ep.openType==e.type) return ep;                  
        }
        return null;
    }
    
    public EdgeMap1 findByCurrent(Edge e){
        for(int i=0; i<edgeMap.size(); i++){
           EdgeMap1 ep= (EdgeMap1) edgeMap.elementAt(i); 
           if(ep.currentEdge==e) return ep;                 
        }
        return null;
    }
    
    public int relationToThis(EdgeMap1 em1) {
     boolean fromThis= em1.currentEdge.from.hasSuperCluster(this) ||(em1.currentEdge.from==this) ;
     boolean toThis= em1.currentEdge.to.hasSuperCluster(this)||(em1.currentEdge.to==this);  
     if(fromThis&&toThis)
         return INTERNAL;
     else if(fromThis)
         return FROMTHIS ;
     else if(toThis)
         return TOTHIS ;
     else
         return FOREIGN;
    }
    
   //two good functions to overwrite
    
    public Edge getEdgeCollaps(EdgeMap1 em1,int relation) {
       Edge nedge=em1.currentEdge;
       Node temp;
       if(relation==FROMTHIS){
          nedge=new Edge(this, em1.currentEdge.to);    
                 
       }
       else if(relation==TOTHIS){
          nedge=new Edge(  em1.currentEdge.from, this);   
           
       }
       nedge.type=em1.collapsedType;
       nedge.setLength( em1.length);
       em1.currentEdge=nedge; 
       return nedge;
    }
    
     public Edge getEdgeUnfold(EdgeMap1 em1,int relation) {
       Edge nedge=em1.currentEdge;
       Node temp=null;
       if(relation==FROMTHIS){
          Node n= getSupremum(em1.from, this);
          if(n==null) System.err.println("Error in Unfold");
          nedge=new Edge(n, em1.currentEdge.to);    
          temp= em1.currentEdge.to;
           
       }
       else if(relation==TOTHIS){
          Node n= getSupremum(em1.to,   this);
          if(n==null) System.err.println("Error in Unfold");
          nedge=new Edge(  em1.currentEdge.from, n);   
          temp= em1.currentEdge.from; 
       }
       
       
       if(temp instanceof ClusterNode)
          nedge.type=em1.collapsedType;
       else
          nedge.type=em1.openType; 
       nedge.setLength( em1.length);
       em1.currentEdge=nedge; 
       return nedge;
        
    }
    
    public Node getSupremum(Node start, ClusterNode parent) {
        if(parent==null) return null;
        Node n=start;
        if(n.parentCluster==null)
            return null;
        else if(n.parentCluster==parent)
            return n;
        else
            return getSupremum(n.parentCluster, parent);
    }
     
     
     public void clusterSelectedArea(final Rectangle2D.Double selectFrame,final TGPanel tgPanel )
    {
          
       final Vector selectedNodes = new Vector();
       setFrame(selectFrame); 
       tgPanel.updatePosFromDraw(this);
       tgPanel.addOpenCluster(this);//it is essential to addOpenCluster(this) here befor the next loop
                                    //otherwise getOuterCluster() will not work correctly
       for(int i=0;i< tgPanel.openClusters.size(); i++) {
          ClusterNode tcn= (ClusterNode)tgPanel.openClusters.elementAt(i);
          if(this==tgPanel.getOuterCluster( tcn.getFrame())){
              if(tcn.parentCluster!=null) tcn.parentCluster.removeNodeFromCluster(tcn);
              addNodeToCluster(tcn);
              selectedNodes.addElement(tcn);
              
          } 
          else if(tcn==tgPanel.getOuterCluster(selectFrame))
                tcn.addNodeToCluster(this);
          
      } 
       
      
       
         
        TGForEachNode fen = new TGForEachNode() {
            public void forEachNode( Node node ) {
                double x = node.drawx;
                double y = node.drawy;
                if( selectFrame.contains(x,y)&& !(node instanceof ClusterNode && ((ClusterNode)node).open )  ) {
                     
                    if(node.parentCluster==null) {
                        addNodeToCluster(node);
                         selectedNodes.addElement(node);
                    } 
                     else if(TGPanel.properContains(node.parentCluster.getFrame(),selectFrame))
                    {
                         node.parentCluster.removeNodeFromCluster(node);
                         addNodeToCluster(node);
                          selectedNodes.addElement(node);
                    } 
                       
                }
            }
        };

        tgPanel.visibleLocality.forAllNodes(fen);
         
         
                 
         for(int i=0; i<selectedNodes.size(); i++) { 
            Node n= (Node) selectedNodes.elementAt(i);
            for(int j=0; j<n.edges.size(); j++) {
               addEdgeToCluster((Edge) n.edges.elementAt(j));  
            }
             
         }   
        
        
        
         try {
                 tgPanel.addNode(this );
                      
         }
         catch ( TGException tge ) {
                System.err.println(tge.getMessage());
                tge.printStackTrace(System.err);
         }
         
        
         //tgPanel.resetDamper();
          
    }
  
     public Vector  getOutEdges(Node n) {
         Vector outedges= new Vector();  
         Vector alledges=gEltSet.getEdges();  
         for(int i=0; i< alledges.size(); i++){
             Edge e= (Edge)alledges.elementAt(i);
             if(e.from==n)
               outedges.addElement(e);  
         }
         
         for(int i=0; i< edgeMap.size(); i++){
             EdgeMap1 em= (EdgeMap1)edgeMap.elementAt(i);
             if(em.from==n)
               outedges.addElement(new Edge(em.from, em.to));  
         }
         return outedges;
     }
     
     public Vector  getInEdges(Node n) {
         Vector inedges= new Vector();  
         Vector alledges=gEltSet.getEdges();  
         for(int i=0; i< alledges.size(); i++){
             Edge e= (Edge)alledges.elementAt(i);
             if(e.to==n)
               inedges.addElement(e);  
         }
         
         for(int i=0; i< edgeMap.size(); i++){
             EdgeMap1 em= (EdgeMap1)edgeMap.elementAt(i);
             if(em.to==n)
               inedges.addElement(new Edge(em.from, em.to));  
         }
         return inedges;
         
     }
     
     
    
     
      
    
}
 