/*
 * NodeFilter.java 
 * Created on Sep 4, 2004
 *
 */
package com.touchgraph.graphlayout;

/**
 * NodeFilter.java
 * @author Rich
 *
 */
public interface NodeFilter
{
  boolean match(Node node);
}
