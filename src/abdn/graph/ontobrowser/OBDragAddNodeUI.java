package abdn.graph.ontobrowser;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

import com.touchgraph.graphlayout.ClusterNode;
import com.touchgraph.graphlayout.Node;
import com.touchgraph.graphlayout.TGPaintListener;
import com.touchgraph.graphlayout.interaction.TGAbstractDragUI;

/**
 * LBDragAddNodeUI contains code for adding OBNodes + OBEdges by dragging.
 *
 * @author Alexander Shapiro ,Serguei Krivov
 * @version 1.20
 */

public class OBDragAddNodeUI extends TGAbstractDragUI implements
		TGPaintListener {

	Point mousePos = null;

	Node dragAddNode = null;

	ESDOntoBrowser esdOntoBrowser;

	// ............

	public OBDragAddNodeUI(ESDOntoBrowser ob) {
		super(ob.getTGPanel());
		esdOntoBrowser = ob;
	}

	public void preActivate() {
		mousePos = null;
		tgPanel.addPaintListener(this);
	}

	public void preDeactivate() {
		tgPanel.removePaintListener(this);
	};

	public void mousePressed(MouseEvent e) {
		dragAddNode = tgPanel.getMouseOverN();
	}

	public void mouseReleased(MouseEvent e) {

		if ((dragAddNode != null) && (dragAddNode instanceof ClusterNode))
			return;
		mousePos = e.getPoint();
		Node mouseOverN = tgPanel.getMouseOverN();
		if ((mouseOverN != null) && (mouseOverN instanceof ClusterNode))
			return;
		if (dragAddNode != mouseOverN) {
			if (esdOntoBrowser.guiPolicy instanceof GUIPolicyEditor)
				((GUIPolicyEditor) esdOntoBrowser.guiPolicy).addLink(
						(OBNode) dragAddNode, e.getX(), e.getY());
		}
		//if(esdOntoBrowser.ntPolicy!=null)
		//  esdOntoBrowser.ntPolicy.addLink( dragAddNode.getID(),
		// mouseOverN.getID());

		/*
		 * if (mouseOverN != null && dragAddNode != null && mouseOverN !=
		 * dragAddNode) { Edge ed = tgPanel.findEdge(dragAddNode, mouseOverN);
		 *
		 * if ((dragAddNode instanceof OBObjectNode) && (mouseOverN instanceof
		 * OBObjectNode)) {
		 *
		 * esdOntoBrowser.guiPolicy.createNewRelation( (OBNode) dragAddNode,
		 * (OBNode) mouseOverN); } else if ( ed == null && (dragAddNode
		 * instanceof OBRelationNode) && (mouseOverN instanceof OBObjectNode)) {
		 * esdOntoBrowser.guiPolicy.addLink( (OBRelationNode) dragAddNode,
		 * (OBNode) mouseOverN); } else if ( ed == null && (dragAddNode
		 * instanceof OBObjectNode) && (mouseOverN instanceof OBRelationNode)) {
		 * esdOntoBrowser.guiPolicy.addLink( (OBRelationNode) mouseOverN,
		 * (OBNode) dragAddNode); } // else tgPanel.deleteEdge(ed); } else if
		 * (mouseOverN == null && dragAddNode != null) {
		 *
		 * esdOntoBrowser.guiPolicy.createNewRelation( (OBNode) dragAddNode,
		 * null); /* OBObjectNode n = esdOntoBrowser.createNewObject(mousePos.x,
		 * mousePos.y); if (esdOntoBrowser.ntPolicy != null)
		 * esdOntoBrowser.ntPolicy.createNewObject(n.getID()); if (dragAddNode
		 * instanceof OBObjectNode) {
		 *
		 * //addRelationWithObject OBRelationNode attrnode =
		 * esdOntoBrowser.createNewRelation( (OBNode) dragAddNode, (OBNode) n);
		 * if (esdOntoBrowser.ntPolicy != null)
		 * esdOntoBrowser.ntPolicy.createNewRelation( attrnode.getID(),
		 * dragAddNode.getID(), n.getID()); } else if (dragAddNode instanceof
		 * OBRelationNode) { esdOntoBrowser.addObjectToRange( (OBRelationNode)
		 * dragAddNode, (OBNode) n); if (esdOntoBrowser.ntPolicy != null)
		 * esdOntoBrowser.ntPolicy.addObjectToRange( dragAddNode.getID(),
		 * n.getID()); } tgPanel.setSelect(n);
		 * (esdOntoBrowser.guiPolicy).setObjectNode((OBObjectNode) n); }
		 */

		if (mouseWasDragged) { //Don't reset the damper on a mouseClicked
			tgPanel.resetDamper();
			tgPanel.startDamper();
		}

		dragAddNode = null;
	}

	public void mouseDragged(MouseEvent e) {
		mousePos = e.getPoint();
		tgPanel.repaint();
	}

	public void paintFirst(Graphics g) {
	};

	public void paintLast(Graphics g) {
	};

	public void paintAfterEdges(Graphics g) {

		if (mousePos == null)
			return;
		if (dragAddNode instanceof ClusterNode)
			return;

		Node mouseOverN = tgPanel.getMouseOverN();

		if (mouseOverN == null) {
			g.setColor(Node.BACK_DEFAULT_COLOR);
			g.drawRect(mousePos.x - 7, mousePos.y - 7, 14, 14);
		}

		Color c;
		if (mouseOverN == dragAddNode)
			c = Color.lightGray;
		else
			c = OBEdge.DEFAULT_COLOR;

		OBEdge.paintGhost(g, dragAddNode.drawx, dragAddNode.drawy, mousePos.x,
				mousePos.y, c);
	}

} // end com.touchgraph.graphlayout.interaction.LBDragAddNodeUI
