/*
 * Created on Oct 14, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package abdn.graph.ontobrowser;

/**
 * @author skrivov
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface GUIPolicyEditor {
  //this will be responsible for creating new relations and argument links from different operators
  public void addLink(OBNode fromObj, int x, int y);

  // public void addLink(OBNode fromObj, OBNode toObj, int x, int y);

  //this function will be normaly responsible for creating new nodes
  public void processLBClick(int x, int y);

  // notify gui of changes to nodes
  //public void addNotification(OBNode node);

  //public void changeNameNotification(OBNode node, String old);

  //public void changeLabelNotification(OBNode node, String newLabel);

  //public void deleteNotification(OBNode node);

  // notify gui of changes to links
  public void addEdgeNotification(OBEdge e);

  public void deleteEdgeNotification(OBEdge e);

  //public OBRelationNode createNewRelation(OBNode fromObj, OBNode toObj);
  //public void addObjectToRange(OBRelationNode rel, OBNode obj);
  //public void addObjectToDomain(OBRelationNode rel, OBNode obj);
  //public OBObjectNode createNewObject(float x, float y) throws TGException;
  //public OBRelationNode createNewRelation(float x, float y)
  //throws TGException;

}
