/*
 * OBDragAddClusterUI.java
 *
 * Created on June 23, 2003, 2:18 PM
 */

package abdn.graph.ontobrowser;
import  com.touchgraph.graphlayout.interaction.*;

import java.awt.geom.*;

import  java.awt.event.*;

/**
 *
 * @author  serguei
 */
public class OBDragAddClusterUI extends DragMultiselectUI {
     ESDOntoBrowser esdOntoBrowser;


    /** Creates a new instance of OBDragAddClusterUI */
    public OBDragAddClusterUI( ESDOntoBrowser ob) {
        super(ob.getTGPanel());
        esdOntoBrowser=ob;

    }

     public void mouseReleased(MouseEvent e)
    {
      Rectangle2D.Double selectRectangle =new Rectangle2D.Double();
      selectRectangle.setFrameFromDiagonal(startPos, mousePos);
      if(!tgPanel.badCluster(selectRectangle))
      {
          OBClusterNode cn= new OBClusterNode(null,null, true);
          cn.clusterSelectedArea(selectRectangle, tgPanel );
      }

      esdOntoBrowser.tgUIManager.activate("Edit");
    }

}
