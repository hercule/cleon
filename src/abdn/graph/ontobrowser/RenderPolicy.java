/*
 * ConfigPolicy.java
 *
 * Created on December 8, 2003, 11:03 AM
 */

package abdn.graph.ontobrowser;

/**
 *
 * @author  skrivov
 */
public interface RenderPolicy {
    public void resetRelationCategory(OBRelationNode rn);
    public void resetObjectColor(OBObjectNode on);
    public void resetEdgeCategory(OBEdge ed,int relationCategory, boolean inheritable, boolean multy, boolean isvariable  );
    public void resetEdge(OBEdge ed);
    public void initRelation(OBRelationNode rn);
    public void initObject(OBObjectNode on);
    public void initObject(OBObjectNode on, int type);
    public void initRelation(OBRelationNode rn, int category);
}
