/*
 * NotificationPolicy.java
 *
 * Created on December 4, 2003, 10:39 AM
 */

package abdn.graph.ontobrowser;

/**
 *
 * @author  skrivov
 */
public interface NotificationPolicy {
  public void resetOntology(boolean empty);
	public void setOBPointer(ESDOntoBrowser ob);

	//Main Notification API

	public void creatNewObject(String objID, int objCategory);
	public void addLink(String fromObjID, String toObjID);
	public void  createNewRelation(String relID,  int RelationType);
	public void  createNewRelation(String relID,  int RelationType, String fromObjID ,String toObjID);
	// public void addObjectToRange( String relID, String toObjID);
	// public void addObjectToDomain( String relID, String fromObjID ) ;
	public void deleteObjectFromRange(String relID, String toObjID);
	public void deleteObjectFromDomain(String relID, String fromObjID);

	public void deleteRelation(String relID);
	public void deleteObject(String objID);

	//the following three are depricated and will be removed after introduction of new gui
//	public void createNewObject(String objID);
//	public void createNewRelation(String relID);
	/*public void createNewRelation(
		String relID,
		String fromObjID,
		String toObjID);*/

	//Object Notification API, may be called after respective functions of OBObjectNode
	public void setObjectName(String objID, String Name);
	//may be called after Node::setLabel(String) is called
	public void setObjectDescription(String objID, String desc);
	//may be called after Node::setHint(String) is called
	public void setObjectAsVariable(String objID, boolean flag);
	public void setObjectAsClass(String objID, boolean flag);
	public void setClassMinCardinality(String objID, int minCard);
	public void setClassMaxCardinality(String objID, int maxCard);

	//Relation Notification API may be called after respective functions of OBRelationNode
	public void setRelationType(String relID, int type);
	//see below for the list of relation types
	public void setRelationName(String relID, String Name);
	//may be called after Node::setLabel(String) is called on OBRelationNode
	public void setRelationDescription(String relID, String desc);
	//may be called after Node::setHint(String) is called
	public void setRelationAsVariable(String relID, boolean flag);
	public void setRelationMinCardinality(String relID, int minCard);
	public void setRelationMaxCardinality(String relID, int maxCard);

	/*Heer is declaration of relation types from OBRelationNode - this will be not that important in context ow growl.
	public final static int IS_SUBCLASS =1;
	public final static int IS_INSTANCE =2;
	public final static int IS_SIGNATURE =3;
	public final static int IS_ATTRIBUTE =4;

	//CONSTRAINT OPERATORS
	// public final static int IS_CONSTRAINT_ATTRIBUTE =4;
	// public final static int IS_CONSTRAINT_OPERATOR =5;

	public final static int IS_INTERSECTION =5;
	public final static int IS_UNION =6;
	public final static int IS_DISJOINT =7;
	public final static int IS_DISJOINT_UNION =8;
	public final static int IS_DISJOINT_WITH =9;  //THIS ONE IS NOT AN OPERATOR
	public final static int IS_SAME_AS =10;
	public final static int IS_ONE_OF =11;
	 *
	 *
	 */

}
