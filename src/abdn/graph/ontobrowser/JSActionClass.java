/*
 * JSActionClass.java
 *
 * Created on September 24, 2003, 1:58 PM
 */

package abdn.graph.ontobrowser;

/**
 *
 * @author  skrivov
 */
import  java.util.Vector;
import javax.swing.*;

public class JSActionClass {
    public String name;
    Vector actions;

    /** Creates a new instance of JSActionClass */
    public JSActionClass(String _name ) {
        name =_name;
        actions= new Vector();
    }

     public void  addAction(JSAction a) {
        actions.addElement(a);
    }

    public JMenu createMenuComponent(OBNode node){
        JMenu menu= new JMenu(JSAction.processMacros(name, node));
        JMenuItem mitem;
        for(int i=0; i<actions.size(); i++){
            JSAction act= (JSAction) actions.elementAt(i);
            mitem=act.createMenuItem( node);
            menu.add(mitem);
        }
        return menu;
    }

}
