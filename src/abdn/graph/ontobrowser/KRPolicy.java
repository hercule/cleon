/*
 * KRPolicy.java
 *
 * Created on December 4, 2003, 10:38 AM
 */

package abdn.graph.ontobrowser;

/**
 *
 * @author  skrivov
 */
import java.io.*;
import java.util.*;
import java.net.*;
import abdn.Cleon.util.EntityVector;
import abdn.Cleon.util.EntityRelationVector;
//import abdn.taxon.data.PropertyVector;*/

public interface KRPolicy {

  public void setOBPointer(ESDOntoBrowser ob);
  //some of these read methods can be redundant and if so they will go, some methods will take URI as parameter instead of URL
  public void read(String fileName) throws Exception ;
  //public void read(URL xmlURL) throws Exception ;
  public void read(String fileName, Thread afterReading) throws Exception;
  public void read(EntityVector e, EntityRelationVector er1, EntityRelationVector er2, boolean o, Thread afterReading) throws Exception ;
  public void read(URL url, Thread afterReading) throws Exception;
  public void read(URI uri, Thread afterReading) throws Exception;
  // public void read(String fileName, InputStream in, Thread afterReading) throws Exception;

}