/*
 * JSAction.java
 *
 * Created on September 24, 2003, 11:49 AM
 */

package abdn.graph.ontobrowser;
import  java.util.Vector;
import javax.swing.*;
/**
 *
 * @author  skrivov
 */
public class JSActionGroup {

    public String ID;
    Vector actClasses;
    /** Creates a new instance of JSActionGroup */
    public JSActionGroup(String _ID) {
        ID=_ID;
        actClasses=new Vector();
    }

    public void  addActionClass(JSActionClass ac) {
        actClasses.addElement(ac);
    }

    //the main operation- create a vector of menu items
    public Vector getMenuComponents (OBNode node) {

       Vector mcomps= new Vector();
       for(int i=0; i<actClasses.size(); i++){
          JSActionClass acl= (JSActionClass)actClasses.elementAt(i);
          JMenu  mitem=acl.createMenuComponent(node);
          mcomps.addElement(mitem);
       }
       return mcomps ;
    }

}


