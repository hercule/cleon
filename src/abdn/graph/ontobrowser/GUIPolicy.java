/*
 * Created on Apr 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package abdn.graph.ontobrowser;

import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import com.touchgraph.graphlayout.Node;
import com.touchgraph.graphlayout.TGPanel;

/**
 * @author skrivov
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface GUIPolicy {

  //initialization of gui
  public void initialize();

  public void setOBPointer(ESDOntoBrowser ob);

  public void showOBPopup(MouseEvent e);

  public TGPanel getTgPanel();

 // public void setRelationNode(OBRelationNode rn);

  public void setObjectNode(OBObjectNode rn);

  public void setEditing(boolean editing);

  public void setControlVisibility(boolean vis);

  public void restoreAll();

  //interactions

  public void setPopupNode(Node n);

  public JPopupMenu getNodePopup(OBNode popupNode);

  //public JPopupMenu setUpEdgePopup();
  public JPopupMenu getBackPopup();

  public void showErrorMessage(String msg);
}
