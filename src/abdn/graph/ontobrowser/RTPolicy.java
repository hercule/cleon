/*
 * RuntimePolicy.java
 * Created on December 4, 2003, 10:38 AM
 */

package abdn.graph.ontobrowser;

/**
 * @author  skrivov
 */
public interface RTPolicy {
  public void initialize();
}
