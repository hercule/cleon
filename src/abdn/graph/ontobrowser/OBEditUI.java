/*
 "This product includes software developed by
 TouchGraph LLC (http://www.touchgraph.com/)."
 */

package abdn.graph.ontobrowser;

import com.touchgraph.graphlayout.interaction.*;
import com.touchgraph.graphlayout.*;

import javax.swing.*;

import java.awt.event.*;

/**
 * OBEditUI: User Interface for editing the graph.
 *
 * Author: Alexander Shapiro , Serguei Krivov
 *
 */

public class OBEditUI
    extends TGUserInterface {
  ESDOntoBrowser esdOntoBrowser;

  TGPanel tgPanel;

  OBDragAddNodeUI dragAddNodeUI;

  DragNodeUI dragNodeUI;

  TGAbstractClickUI switchSelectUI;

  TGAbstractDragUI hvDragUI;

  Node popupNode = null;

  //Edge popupEdge = null;
  //JPopupMenu nodePopup;
  JPopupMenu clNodePopup;

  //JPopupMenu edgePopup;
  //JPopupMenu backPopup;
  JPopupMenu backClusterPopup;

  int mousex, mousey;

  ClusterNode clNode;

  LBEditMouseListener ml;

  LBEditMouseMotionListener mml;

  public OBEditUI(ESDOntoBrowser esdob) {
    esdOntoBrowser = esdob;
    tgPanel = esdOntoBrowser.getTGPanel();

    ml = new LBEditMouseListener();
    mml = new LBEditMouseMotionListener();
    dragAddNodeUI = new OBDragAddNodeUI(esdOntoBrowser);
    dragNodeUI = new DragNodeUI(tgPanel);
    switchSelectUI = tgPanel.getSwitchSelectUI();
    hvDragUI = esdOntoBrowser.hvScroll.getHVDragUI();
  }

  public void activate() {
    tgPanel.addMouseListener(ml);
    tgPanel.addMouseMotionListener(mml);
  }

  public void deactivate() {
    tgPanel.removeMouseListener(ml);
    tgPanel.removeMouseMotionListener(mml);
  }

  class LBEditMouseListener
      extends MouseAdapter {
    public void mousePressed(MouseEvent e) {
      if (ESDOntoBrowser.deleteOnLClick) {
        return;
      }
      Node mouseOverN = tgPanel.getMouseOverN();
      Node select = tgPanel.getSelect();
      mousex = e.getX();
      mousey = e.getY();
      clNode = tgPanel.getOpenCluster(mousex, mousey);

      if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
        if (mouseOverN != null) {
          if (mouseOverN != select) {
            dragNodeUI.activate(e);
          }
          else {
            dragAddNodeUI.activate(e);
          }
        }
        else {
          hvDragUI.activate(e);

        }
      }
    }

    public void mouseClicked(MouseEvent e) {
      Node mouseOverN = tgPanel.getMouseOverN();
      Edge mouseOverE = tgPanel.getMouseOverE();
      Node select = tgPanel.getSelect();
      if ( (e.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) {
        if (mouseOverN != null) {
          if (ESDOntoBrowser.deleteOnLClick) {
            if (esdOntoBrowser.guiPolicy instanceof GUIPolicyEditor) {
              esdOntoBrowser.deleteNode( (OBNode) mouseOverN, true);
              ( (GUIPolicyEditor) esdOntoBrowser.guiPolicy)
                  .processLBClick(e.getX(), e.getY());
              tgPanel.repaint();

            }
            return;
          }
          else if (mouseOverN != select) {
            esdOntoBrowser.setSelect( (OBNode) mouseOverN);

          }
          else if (mouseOverN instanceof OBNode) {
            //esdOntoBrowser.processNodeUrl((OBNode)mouseOverN);
          }
        }
        else if (mouseOverE != null) {
          if (ESDOntoBrowser.deleteOnLClick) {
            if (esdOntoBrowser.guiPolicy instanceof GUIPolicyEditor) {
              esdOntoBrowser.deleteEdge( (OBEdge) mouseOverE);
              ( (GUIPolicyEditor) esdOntoBrowser.guiPolicy)
                  .processLBClick(e.getX(), e.getY());
            }
            return;
          }
        }
        else {

          //hvDragUI.activate(e);
          if (esdOntoBrowser.guiPolicy instanceof GUIPolicyEditor) {
            ( (GUIPolicyEditor) esdOntoBrowser.guiPolicy)
                .processLBClick(e.getX(), e.getY());
          }
        }
      }
      /*
       * if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
       * switchSelectUI.activate(e); /* Node mouseOverN =
        * tgPanel.getMouseOverN(); Node select = tgPanel.getSelect();
        *
        * if (mouseOverN != select) esdOntoBrowser.setSelect((OBNode)
        * mouseOverN); //else //tgPanel.clearSelect(); }
        */

    }

    public void mouseReleased(MouseEvent e) {
      //  if (e.isPopupTrigger()) {
      if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
        popupNode = tgPanel.getMouseOverN();
        esdOntoBrowser.guiPolicy.setPopupNode(popupNode);
        //popupEdge = tgPanel.getMouseOverE();
        if (popupNode != null) {
          tgPanel.setMaintainMouseOver(true);
          tgPanel.removeMouseListener(ml);
          tgPanel.removeMouseMotionListener(mml);
          //So that dismissing popup does not add node
          if (popupNode instanceof ClusterNode) {
            clNodePopup.show(e.getComponent(), e.getX(), e.getY());
          }
          else {
            esdOntoBrowser.guiPolicy.getNodePopup(
                (OBNode) popupNode).show(e.getComponent(),
                                         e.getX(), e.getY());
            /*
             * } else if (popupEdge != null) {
             * tgPanel.setMaintainMouseOver(true);
             * tgPanel.removeMouseListener(ml); //So that dismissing
             * popup does not add node edgePopup.show(e.getComponent(),
             * e.getX(), e.getY());
             */
          }
        }
        else {

          if (clNode == null) {
            esdOntoBrowser.guiPolicy.getBackPopup().show(
                e.getComponent(), mousex, mousey);
          }
          else {
            backClusterPopup.show(e.getComponent(), mousex, mousey);
          }
        }

      }
    }
  }

  class LBEditMouseMotionListener
      extends MouseMotionAdapter {
    public void mouseMoved(MouseEvent e) {
      tgPanel.startDamper();
    }
  }

}
/*
 * private void setUpNodePopup() { nodePopup = new JPopupMenu(); clNodePopup =
 * new JPopupMenu(); JMenuItem menuItem; JMenu navigateMenu = new
 * JMenu("Navigate");
 *
     * menuItem = new JMenuItem("Unfold Cluster"); ActionListener unfoldAction = new
 * ActionListener() { public void actionPerformed(ActionEvent e) { if (popupNode !=
 * null) { ((ClusterNode) popupNode).unfoldCluster(tgPanel); tgPanel.repaint();
 * tgPanel.resetDamper(); } } };
 *
 * menuItem.addActionListener(unfoldAction); clNodePopup.add(menuItem);
 *
 * menuItem = new JMenuItem("Edit Node"); ActionListener editNodeAction = new
 * ActionListener() { public void actionPerformed(ActionEvent e) { if (popupNode !=
     * null) { tgPanel.setSelect(popupNode); if (popupNode instanceof OBObjectNode) {
 * (esdOntoBrowser.guiPolicy).setObjectNode( (OBObjectNode) popupNode); } else
 * if (popupNode instanceof OBRelationNode) {
 * (esdOntoBrowser.guiPolicy).setRelationNode( (OBRelationNode) popupNode); } } } };
 *
 * menuItem.addActionListener(editNodeAction); nodePopup.add(menuItem);
 *
 * menuItem = new JMenuItem("Expand Node"); ActionListener expandAction = new
 * ActionListener() { public void actionPerformed(ActionEvent e) { if (popupNode !=
 * null) { tgPanel.expandNode(popupNode); } } };
 *
 * menuItem.addActionListener(expandAction); navigateMenu.add(menuItem);
 *
 * menuItem = new JMenuItem("Hide Node"); ActionListener hideAction = new
     * ActionListener() { public void actionPerformed(ActionEvent e) { Node select =
 * tgPanel.getSelect(); if (popupNode != null) { tgPanel.hideNode(popupNode,
 * select); } } };
 *
 * menuItem.addActionListener(hideAction); navigateMenu.add(menuItem);
 *
 * nodePopup.add(navigateMenu); clNodePopup.add(navigateMenu);
 *
 * menuItem = new JMenuItem("Delete Node"); ActionListener deleteNodeAction =
 * new ActionListener() { public void actionPerformed(ActionEvent e) { if
 * (popupNode != null) {
 *
 * if (!(popupNode instanceof OBObjectNode)) { esdOntoBrowser.deleteRelation(
 * (OBRelationNode) popupNode); if (esdOntoBrowser.ntPolicy != null)
 * esdOntoBrowser.ntPolicy.deleteRelation( popupNode.getID()); } else {
 * esdOntoBrowser.deleteObject((OBObjectNode) popupNode); if
 * (esdOntoBrowser.ntPolicy != null) esdOntoBrowser.ntPolicy.deleteObject(
 * popupNode.getID()); } } } };
 *
 * menuItem.addActionListener(deleteNodeAction); nodePopup.add(menuItem);
 *
 * nodePopup.addPopupMenuListener(new PopupMenuListener() { public void
 * popupMenuCanceled(PopupMenuEvent e) { } public void
 * popupMenuWillBecomeInvisible(PopupMenuEvent e) {
 * tgPanel.setMaintainMouseOver(false); tgPanel.setMouseOverN(null);
 * tgPanel.repaint(); tgPanel.addMouseListener(ml); } public void
 * popupMenuWillBecomeVisible(PopupMenuEvent e) { } });
 *
 * clNodePopup.addPopupMenuListener(new PopupMenuListener() { public void
 * popupMenuCanceled(PopupMenuEvent e) { } public void
 * popupMenuWillBecomeInvisible(PopupMenuEvent e) {
 * tgPanel.setMaintainMouseOver(false); tgPanel.setMouseOverN(null);
 * tgPanel.repaint(); tgPanel.addMouseListener(ml); } public void
 * popupMenuWillBecomeVisible(PopupMenuEvent e) { } }); }
 *
 * private void setUpEdgePopup() { edgePopup = new JPopupMenu(); JMenuItem
 * menuItem;
 *
 * menuItem = new JMenuItem("Edit Edge"); ActionListener editEdgeAction = new
 * ActionListener() { public void actionPerformed(ActionEvent e) { if (popupEdge !=
 * null) { //dlg esdOntoBrowser.lbEdgeDialog.setOBEdge(popupEdge); //dlg
 * esdOntoBrowser.lbEdgeDialog.showDialog(); } } };
 * menuItem.addActionListener(editEdgeAction); edgePopup.add(menuItem);
 *
 * menuItem = new JMenuItem("Reverse Edge"); ActionListener reverseEdgeAction =
 * new ActionListener() { public void actionPerformed(ActionEvent e) { if
 * (popupEdge != null) { popupEdge.reverse(); } } };
 * menuItem.addActionListener(reverseEdgeAction); edgePopup.add(menuItem);
 *
 * menuItem = new JMenuItem("Delete Edge"); ActionListener deleteEdgeAction =
 * new ActionListener() { public void actionPerformed(ActionEvent e) { if
 * (popupEdge != null) { tgPanel.deleteEdge(popupEdge); } } };
 * menuItem.addActionListener(deleteEdgeAction); edgePopup.add(menuItem);
 *
 * edgePopup.addPopupMenuListener(new PopupMenuListener() { public void
 * popupMenuCanceled(PopupMenuEvent e) { } public void
 * popupMenuWillBecomeInvisible(PopupMenuEvent e) {
 * tgPanel.setMaintainMouseOver(false); tgPanel.setMouseOverE(null);
 * tgPanel.repaint(); tgPanel.addMouseListener(ml); } public void
 * popupMenuWillBecomeVisible(PopupMenuEvent e) { } }); }
 *
 */