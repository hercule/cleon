/*
 "This product includes software developed by
      TouchGraph LLC (http://www.touchgraph.com/)."
 */

package abdn.graph.ontobrowser;

import java.util.Enumeration;
import java.util.Vector;

import com.touchgraph.graphlayout.ClusterNode;
import com.touchgraph.graphlayout.Edge;
import com.touchgraph.graphlayout.TGPanel;

/**  OBNode:  A LinkBrowser Node.  Extends node by adding data storage for URL's, hints.
 *  Some minor modifications have been made to the paint methods, allowing node color
 *  to show through on selection, and adding horizontal lines for XML nodes.
 *
 *  @author   Alexander Shapiro  , Serguei Krivov
 */

public class OBObjectNode extends OBNode {

  public static int countobj = 0;
  public int objectCategory = 0;

  public boolean isClass = true;

  public OBObjectNode() {
    this(null, "", "");

  }

  public OBObjectNode(String l) {
    this(null, l, "");
  }

  public OBObjectNode(String id, String l, String u) {
    super(id, l, u);

    /*
       borderColor=OBConfig.OBJECT_BORDER_COLOR;
       backColor=OBConfig.OBJECT_BACK_COLOR;
       textColor=OBConfig.OBJECT_TEXT_COLOR;
       typ=OBConfig.OBJECT_SHAPE;
       style=OBConfig.OBJECT_STYLE;
     */

    countobj++;
    if (id == null) {
      String ID;
      ID = "Object" + String.valueOf(countobj);

      this.setID(ID);
      if (l == "") {
        this.setLabel(ID);
      }
    }

    url = u;
    hint = "";
    hintFont = HINT_FONT;
  }

  public int edgeType(Edge edge) {

    if (isEdgeFromThis(edge) && edge.to instanceof OBRelationNode) {
      return ( (OBRelationNode) edge.to).relationCategory;
    }
    else if (edge.from instanceof OBRelationNode) {
      return - ( (OBRelationNode) edge.from).relationCategory;
    }

    return -1000;
  }

  /*
    public void resetColor(){
     if(isvariable)
      {
         borderColor=OBConfig.VARIABLE_OBJ_BORDER_COLOR;
         backColor=OBConfig.VARIABLE_OBJ_BACK_COLOR;
         textColor=OBConfig.VARIABLE_OBJ_TEXT_COLOR;
         typ=OBConfig.OBJECT_SHAPE;
      }
      else
      {
          borderColor=OBConfig.OBJECT_BORDER_COLOR;
          backColor=OBConfig.OBJECT_BACK_COLOR;
          textColor=OBConfig.OBJECT_TEXT_COLOR;
      }
    } */

  public Vector findInstances() {
    Vector instances = new Vector();
    for (int i = 0; i < edges.size(); i++) {
      OBEdge edge = (OBEdge) edges.elementAt(i);
      if (isEdgeToThis(edge)
          && (edge.from instanceof OBRelationNode)
          && ( ( (OBRelationNode) edge.from).relationCategory
              == OBRelationNode.IS_INSTANCE)) {
        OBNode n =
            (OBNode) ( ( (OBRelationNode) edge.from).getInputs())
            .firstElement();
        if (n instanceof OBObjectNode) {
          instances.addElement(n);
        }
      }

    }
    return instances;
  }

  public Vector findTypes() {
    Vector types = new Vector();
    for (int i = 0; i < edges.size(); i++) {
      OBEdge edge = (OBEdge) edges.elementAt(i);
      if (isEdgeFromThis(edge)
          && (edge.to instanceof OBRelationNode)
          && ( ( (OBRelationNode) edge.to).relationCategory
              == OBRelationNode.IS_INSTANCE)) {
        OBNode n =
            (OBNode) ( ( (OBRelationNode) edge.to).getOutputs())
            .firstElement();
        if (n instanceof OBObjectNode) {
          types.addElement(n);
        }
      }

    }
    return types;
  }

}
