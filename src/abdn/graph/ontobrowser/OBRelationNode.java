/*
 "This product includes software developed by
      TouchGraph LLC (http://www.touchgraph.com/)."
 */

package abdn.graph.ontobrowser;

import java.util.Vector;

/**  OBNode:  A LinkBrowser Node.  Extends node by adding data storage for URL's, hints.
 *  Some minor modifications have been made to the paint methods, allowing node color
 *  to show through on selection, and adding horizontal lines for XML nodes.
 *
 *  @author   Alexander Shapiro  , Serguei Krivov
 */

public class OBRelationNode
    extends OBNode {

  public static int countrelns = 0;
  public boolean isPureRelation = false;

  public OBRelationNode() {
    this(null, "", "");

  }

  public OBRelationNode(String l) {
    this(null, l, "");
  }

  public OBRelationNode(String id, String l, String u) {
    super(id, l, u);
    // category=IS_RELATION;
    /*
             borderColor=OBConfig.ATTRIB_BORDER_COLOR;
             backColor=OBConfig.ATTRIB_BACK_COLOR;
             textColor=OBConfig.ATTRIB_TEXT_COLOR;
             typ=OBConfig.ATTRIB_SHAPE;
             style=OBConfig.ATTRIB_STYLE;
             relationCategory=IS_ATTRIBUTE;*/
    //localityIndex=0;
    String ID;

    countrelns++;
    if (id == null) {
      ID = "Relation" + String.valueOf(countrelns);
      this.setID(ID);
      if (l == "") {
        this.setLabel(ID);
      }
    }

    url = u;
    hint = "";
    hintFont = HINT_FONT;
  }

  //The following are category manipulation variables used mainly with relation nodes, not with object nodes
  //exclusively for use with OBRelationDialog

  public int relationCategory = 0;
  public int relationSubCategory = 0;
  // protected int  CategoryCounter;
  public boolean inheritable = false;
  public boolean multy = false;

  public final static int IS_SUBCLASS = 1;
  public final static int IS_INSTANCE = 2;
  public final static int IS_SIGNATURE = 3;
  public final static int IS_ATTRIBUTE = 4;

  public final static int IS_CLASS_OPERATOR = 15;
  public final static int IS_OBJECT_OPERATOR = 16;

  //CONSTRAINT OPERATORS
  // public final static int IS_CONSTRAINT_ATTRIBUTE =4;
  // public final static int IS_CONSTRAINT_OPERATOR =5;

  //the next  constants will go extinct
  public final static int IS_INTERSECTION = 5;
  public final static int IS_UNION = 6;
  public final static int ARE_DISJOINT = 7;
  public final static int IS_DISJOINT_UNION = 8;
  public final static int IS_DISJOINT_WITH = 9; //THIS ONE IS NOT AN OPERATOR
  public final static int IS_SAME_AS = 10;
  public final static int IS_ONE_OF = 11;
  public final static int ARE_EQUIVALENT = 12;
  public final static int ARE_DIFFERENT = 13;
  public final static int ARE_SAME = 14;

  // public boolean isOperator=false;
  // public boolean isConstraint=false;

  public boolean isTransitive = false;
  public boolean isFunctional = false;
  public boolean isOneToOne = false;
  public boolean isSimmetric = false;
  public boolean isInverseFunctional = false;

  /*
        public static int getRelationType(String relType){
    int cat;
    if(relType.equals("Property Value"))
          cat=OBRelationNode.IS_ATTRIBUTE;
     else if(relType.startsWith("Prop"))
         cat=OBRelationNode.IS_SIGNATURE;
     else if(relType.startsWith("Inst"))
         cat=OBRelationNode.IS_INSTANCE;
     else if(relType.startsWith("Sub"))
         cat=OBRelationNode.IS_SUBCLASS;
     else if(relType.startsWith("Inters"))
         cat=OBRelationNode.IS_INTERSECTION;
     else if(relType.startsWith("Union"))
         cat=OBRelationNode.IS_UNION;
     else if(relType.startsWith("Disjoint Union"))
         cat=OBRelationNode.IS_DISJOINT_UNION;
     else if(relType.startsWith("Disjoint With"))
         cat=OBRelationNode.IS_DISJOINT_WITH;
     else if(relType.startsWith("Disjoint"))
         cat=OBRelationNode.ARE_DISJOINT;
     else if(relType.startsWith("Same As"))
         cat=OBRelationNode.IS_SAME_AS;
     else if(relType.startsWith("One"))
         cat=OBRelationNode.IS_ONE_OF;
     else cat=0;
    return cat;
        }*/

  /*
   public void assertAttribute(){
     relationCategory=IS_ATTRIBUTE;
   //  CategoryCounter=3;
   }
   public void assertSignature(){
     relationCategory= IS_SIGNATURE;
    // CategoryCounter= 11;
   }
   public void assertInstanceRel(){
    relationCategory= IS_INSTANCE;
      //  CategoryCounter=2;
   // inheritable=false;
     multy=false;
     isvariable=false;
     this.setLabel(OBConfig.INSTANCE_LABEL);
   }
   public void assertSubclassRel(){
      relationCategory= IS_SUBCLASS;
    //  CategoryCounter=4;
   //   inheritable=false;
      multy=false;
       isvariable=false;
        this.setLabel(OBConfig.SUBCLASS_LABEL);
   }
   */
  public void setInheritable(boolean inh) {
    inheritable = inh;
  }

  public void setMulty(boolean mult) {
    multy = mult;
  }

  public boolean isInheritable() {
    return inheritable;
  }

  public boolean isMulty() {
    return multy;
  }

  // find out if relation is instance , subclas, attribute or signature
  public int getRelationCategory() {
    return relationCategory;
  }

  public void setRelationSubCategory(int c) {
    relationSubCategory = c;
  }

  //assign the true category calculated
  public void resetCategory(RenderPolicy renPolicy) {
    //if object then reset color

    renPolicy.resetRelationCategory(this);
    resetRelatedEdges(renPolicy);
  }

  public void resetShape(RenderPolicy renPolicy) {
    //this become renPolicy.resetRelationCategory(this);
    /*
             if(relationCategory== IS_SUBCLASS)
             {
        this.setLabel(OBConfig.SUBCLASS_LABEL);
        borderColor=OBConfig.ISA_SUBCLASS_BORDER_COLOR;
        backColor=OBConfig.ISA_SUBCLASS_BACK_COLOR;
        textColor=OBConfig.ISA_SUBCLASS_TEXT_COLOR;
        typ=OBConfig.ISA_SUBCLASS_SHAPE;
        return;
             }
             if(relationCategory== IS_INSTANCE)
             {
        this.setLabel(OBConfig.INSTANCE_LABEL);
        borderColor=OBConfig.ISA_INSTANCE_BORDER_COLOR;
        backColor=OBConfig.ISA_INSTANCE_BACK_COLOR;
        textColor=OBConfig.ISA_INSTANCE_TEXT_COLOR;
        typ=OBConfig.ISA_INSTANCE_SHAPE;
        return;
             }
             if(relationCategory== IS_ATTRIBUTE)
             {
        //first reset color
        if(!isvariable)
        {
            borderColor=OBConfig.ATTRIB_BORDER_COLOR;
            backColor=OBConfig.ATTRIB_BACK_COLOR;
            textColor=OBConfig.ATTRIB_TEXT_COLOR;
        }
        else
        {
            borderColor=OBConfig.VARIABLE_ATTR_BORDER_COLOR;
            backColor=OBConfig.VARIABLE_ATTR_BACK_COLOR;
            textColor=OBConfig.VARIABLE_ATTR_TEXT_COLOR;
        }
        //now set shape
        if(inheritable&&multy)
        {   typ=OBConfig.MULTYATTRIB_INHERIT_SHAPE; style=OBConfig.MULTYATTRIB_INHERIT_STYLE;}
        else if(inheritable&& !multy)
         {   typ=OBConfig.ATTRIB_INHERIT_SHAPE; style=OBConfig.ATTRIB_INHERIT_STYLE;}
        else if(multy && !inheritable)
         {    typ=OBConfig.MULTYATTRIB_SHAPE;  style=OBConfig.MULTYATTRIB_STYLE; }
        else
        { typ=OBConfig.ATTRIB_SHAPE;  style =OBConfig.ATTRIB_STYLE; }
        return;
             }
             if(relationCategory== IS_SIGNATURE)
             {
        //first reset color
        if(!isvariable)
        {
            borderColor=OBConfig.SIGNATURE_BORDER_COLOR;
            backColor=OBConfig.SIGNATURE_BACK_COLOR;
            textColor=OBConfig.SIGNATURE_TEXT_COLOR;
        }
        else
        {
            borderColor=OBConfig.VARIABLE_SIGNATURE_BORDER_COLOR;
            backColor=OBConfig.VARIABLE_SIGNATURE_BACK_COLOR;
            textColor=OBConfig.VARIABLE_SIGNATURE_TEXT_COLOR;
        }
        //now set shape
        if(inheritable&&multy)
        { typ=OBConfig.MULTYATTRIB_INHERIT_SIGN_SHAPE; style=OBConfig.MULTYATTRIB_INHERIT_SIGN_STYLE; }
        else if(inheritable&& !multy)
        {    typ=OBConfig.ATTRIB_INHERIT_SIGN_SHAPE; style=OBConfig.ATTRIB_INHERIT_SIGN_STYLE; }
        else if(multy && !inheritable)
        {   typ=OBConfig.MULTYATTRIB_SIGN_SHAPE; style=OBConfig.MULTYATTRIB_SIGN_STYLE; }
        else
         {     typ=OBConfig.ATTRIB_SIGN_SHAPE;  style=OBConfig.ATTRIB_SIGN_STYLE;}
        return;
             }
             borderColor=OBConfig.CONSTRAINT_BORDER_COLOR;
             backColor=OBConfig.CONSTRAINT_BACK_COLOR;
             textColor=OBConfig.CONSTRAINT_TEXT_COLOR;
             typ=OBConfig.CONSTRAINT_OPERATOR_SHAPE;
             if(relationCategory== IS_INTERSECTION){
       this.setLabel(OBConfig.INTERSECTION_LABEL);
             } else if(relationCategory== IS_UNION){
       this.setLabel(OBConfig.UNION_LABEL);
             } else if(relationCategory== IS_DISJOINT){
        this.setLabel(OBConfig.DISJOINT_LABEL);
             } else if(relationCategory== IS_DISJOINT_UNION) {
        this.setLabel(OBConfig.DISJOINT_UNION_LABEL);
             } else if(relationCategory== IS_DISJOINT_WITH){
        this.setLabel(OBConfig.DISJOINT_WITH_LABEL);
             } else if(relationCategory== IS_SAME_AS) {
        this.setLabel(OBConfig.SAME_AS_LABEL);
             } else if(relationCategory== IS_ONE_OF){
        this.setLabel(OBConfig.ONE_OF_LABEL);
             }
     */
  }

  public void resetRelatedEdges(RenderPolicy renPolicy) {

    for (int i = 0; i < edges.size(); i++) {
      OBEdge edge = (OBEdge) edges.elementAt(i);
      renPolicy.resetEdgeCategory(edge, relationSubCategory, inheritable, multy,
                                  isvariable);
    }

  }

}
