/*
 * RTPolicyJS.java
 *
 * Created on December 5, 2003, 11:39 AM
 */

package abdn.graph.ontobrowser;

/**
 *
 * @author  skrivov
 */
import javax.swing.*;

public interface RTPolicyJS {
   public void addActionGroup(JSActionGroup actg, String className) ;
   //public JSActionGroup getActionGroup(String id);
   public void call(String funct);
   public  void updateMenu(JPopupMenu nPopup , OBNode node);

}
