/*
 * OBClusterNode.java
 *
 * Created on July 1, 2003, 11:19 AM
 */

package abdn.graph.ontobrowser;

import com.touchgraph.graphlayout.Node;
import com.touchgraph.graphlayout.Edge;
import  java.util.Vector;

/**
 *
 * @author  serguei
 */
public class OBClusterNode extends com.touchgraph.graphlayout.ClusterNode {

    public static int countclusters=0;

    /** Creates a new instance of OBClusterNode */
    public OBClusterNode(String id, String label, boolean opn) {
        super(id,label, opn);
        countclusters++;
        if(id==null) {
           String ID="Cluster"+ String.valueOf(countclusters);
           this.setID(ID);
          if(label==""||label==null) this.setLabel(ID);
        }
    }


     public Vector getOutputs(Node n) {
         Vector outobjects= new Vector();
         Vector outedges= getOutEdges(n);
         for(int i=0; i<outedges.size(); i++){
              Edge e=(Edge) outedges.elementAt(i);
              if(e.to instanceof OBObjectNode)
                  outobjects.addElement(e.to);
         }
         return outobjects;
     }

     public Vector   getInputs(Node n) {
         Vector inobjects= new Vector();
         Vector inedges= getInEdges(n);
         for(int i=0; i<inedges.size(); i++){
              Edge e=(Edge) inedges.elementAt(i);
              if(e.from instanceof OBObjectNode)
                  inobjects.addElement(e.from);
         }
         return inobjects;

     }

}
