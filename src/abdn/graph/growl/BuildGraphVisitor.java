/*
 * BuildGraphVisitor.java
 *
 * Created on December 19, 2003, 2:18 PM
 */

package abdn.graph.growl;

import java.net.URI;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import abdn.graph.ontobrowser.ESDOntoBrowser;
import abdn.graph.ontobrowser.OBEdge;
import abdn.graph.ontobrowser.OBNode;
import abdn.graph.ontobrowser.OBObjectNode;
import abdn.graph.ontobrowser.OBRelationNode;
import org.semanticweb.owl.model.OWLAnd;
import org.semanticweb.owl.model.OWLAnnotationInstance;
import org.semanticweb.owl.model.OWLAnnotationProperty;
import org.semanticweb.owl.model.OWLCardinalityRestriction;
import org.semanticweb.owl.model.OWLClass;
import org.semanticweb.owl.model.OWLDataAllRestriction;
import org.semanticweb.owl.model.OWLDataCardinalityRestriction;
import org.semanticweb.owl.model.OWLDataEnumeration;
import org.semanticweb.owl.model.OWLDataProperty;
import org.semanticweb.owl.model.OWLDataSomeRestriction;
import org.semanticweb.owl.model.OWLDataType;
import org.semanticweb.owl.model.OWLDataValue;
import org.semanticweb.owl.model.OWLDataValueRestriction;
import org.semanticweb.owl.model.OWLDescription;
import org.semanticweb.owl.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owl.model.OWLDisjointClassesAxiom;
import org.semanticweb.owl.model.OWLEnumeration;
import org.semanticweb.owl.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owl.model.OWLEquivalentPropertiesAxiom;
import org.semanticweb.owl.model.OWLException;
import org.semanticweb.owl.model.OWLFrame;
import org.semanticweb.owl.model.OWLIndividual;
import org.semanticweb.owl.model.OWLNamedObject;
import org.semanticweb.owl.model.OWLNot;
import org.semanticweb.owl.model.OWLObject;
import org.semanticweb.owl.model.OWLObjectAllRestriction;
import org.semanticweb.owl.model.OWLObjectCardinalityRestriction;
import org.semanticweb.owl.model.OWLObjectProperty;
import org.semanticweb.owl.model.OWLObjectSomeRestriction;
import org.semanticweb.owl.model.OWLObjectValueRestriction;
import org.semanticweb.owl.model.OWLOntology;
import org.semanticweb.owl.model.OWLOr;
import org.semanticweb.owl.model.OWLProperty;
import org.semanticweb.owl.model.OWLRestriction;
import org.semanticweb.owl.model.OWLSameIndividualsAxiom;
import org.semanticweb.owl.model.OWLSubClassAxiom;
import org.semanticweb.owl.model.OWLSubPropertyAxiom;
import org.semanticweb.owl.model.helper.OWLObjectVisitorAdapter;

import com.touchgraph.graphlayout.TGException;
import com.touchgraph.graphlayout.TGPanel;

/**
 *
 * @author skrivov
 */
public class BuildGraphVisitor
    extends OWLObjectVisitorAdapter { //will be
  // OWLObjectVisitorAdapter
  // instead
  // OWLObjectVisitor

  TGPanel tgPanel;

  //GraphEltSet graphEltSet;
  ESDOntoBrowser esdOntoBrowser;

  OWLtoGraph oWLtoGraph;

  public static final int SUPERCLASS = 0;

  public static final int OPERATOR = 1;

  public static final int SAME_AS = 2;

  public static final int INSTANCE = 3;

  public static final int GETID = 4;

  public static int TYPE = 0;

  private int mode = 0;

  public static int anonimousCounter = 0;

  public static int datatypeCounter = 0;

  // OWLOntology ontology;

  // to store output if visitor visits some OWLRestriciton, there are many of
  // them
  //OBRelationNode restrictNode = null;

  //OBRelationNode relationNode = null;

  // the flag set to true if visitor visits some OWLRestriciton
  boolean propertyRestriction = false;

  // to store output if visitor visits something else than OWLRestriciton,
  // there are many of them
  OBNode outputBaseNode = null;

  //to store input. this is a node where OWLDescription belongs
  //OBObjectNode fromNode = null;

  String id = null;

  String getID() {
    return id;
  }

  Hashtable propRangeIDs = new Hashtable();

  /** Creates a new instance of BuildGraphVisitor */
  public BuildGraphVisitor() {
  }

  public BuildGraphVisitor(ESDOntoBrowser ob, OWLtoGraph otg) {
    //this.ontology=ontology;
    esdOntoBrowser = ob;
    tgPanel = esdOntoBrowser.getTGPanel();
    oWLtoGraph = otg;
    //graphEltSet = krPolicyOWL.tgPanel.getCompleteEltSet();
  }

  //I do not know if it is needed
  public void reset() {
    //fromNode = null;
    //restrictNode = null;
    outputBaseNode = null;
    mode = 0;

  }

  public void setMode(int mod) {
    mode = mod;
  }

  public OBNode getBaseNode() {
    return outputBaseNode;
  }

  public void putRangeID(String propURI, String rangerID) {
    propRangeIDs.put(propURI, rangerID);
  }

  public OBObjectNode findNode(OWLNamedObject obj) throws OWLException,
      TGException {
    if (obj.getURI() == null) {
      return null;
    }
    if (mode == GETID) {
      id = (obj.getURI()).toString();
      return null;
    }
    OBObjectNode objNode = (OBObjectNode) tgPanel.getCompleteEltSet()
        .findNode( (obj.getURI()).toString());
    if (objNode == null) {
      objNode = oWLtoGraph.createObject(obj.getURI(),
                                        RenderGrowl.OBJ_CLASS);
    }
    return objNode;
  }

  public OBObjectNode findNode(OWLDataValue obj) throws OWLException,
      TGException {
    String Name = (obj.getValue()).toString();
    String ID;
    if (obj.getURI() != null) {
      ID = Name + (obj.getURI()).toString();
    }
    else {
      ID = Name;

    }
    id = ID;
    if (mode == GETID) {
      return null;
    }

    OBObjectNode objNode = (OBObjectNode) tgPanel.getCompleteEltSet()
        .findNode(ID);
    if (objNode == null) {
      objNode = new OBObjectNode(ID, "", "");
      esdOntoBrowser.renPolicy.initObject(objNode,
                                          RenderGrowl.OBJ_DATAVALUE);

      if (Name.length() > RenderGrowl.attrValueMaxLength) {
        objNode.setNodeLabel(Name.substring(0,
                                            RenderGrowl.attrValueMaxLength)
                             + "...");
        objNode.setHint(Name);
      }
      else {
        objNode.setNodeLabel(Name);
      }
      if (obj.getURI() != null) {

        //				objNode.setNamespace(obj.getURI().getFragment());
        objNode.setURI(obj.getURI());
      }
      oWLtoGraph.addNode(objNode);
      // objNode= krPolicyOWL.createObject( obj.getValue(),
      // RenderGrowl.DATAVALUE) ;
    }
    return objNode;
  }

  public OBRelationNode findRelationNode(String ID) {

    return (OBRelationNode) tgPanel.getCompleteEltSet().findNode(ID);

  }

  /*
   * public OBRelationNode getRelationNode(String ID, URI uri) throws
   * OWLException {
   *
   * OBRelationNode rNode = (OBRelationNode)
   * tgPanel.getCompleteEltSet().findNode(ID); if (rNode == null) { rNode =
   * new OBRelationNode(ID, "", "");
   * esdOntoBrowser.renPolicy.initRelation(rNode);
   * rNode.setLabel(krPolicyOWL.getMetadata().shortForm(uri));
   * krPolicyOWL.addNode(rNode); } return rNode; }
   */
  /*
   * public void setRelationNode(OBRelationNode rNode) { relationNode=rNode; }
   */

  /*
   * public OBRelationNode getRestrictionNode() { return restrictNode; }
   *
   * public void setFromNode(OBObjectNode node) { fromNode = node; }
   */

  /*
   * public boolean isProperyRestriction() { return propertyRestriction; }
   */

  //THESE FUNCtions build supercalsses and types , they belong to
  // superinterface OWLDescriptionVisitor
  //DATA Properties
  public void visit(OWLDataSomeRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    restriction.getDataType().accept(this);
    URI uri = (restriction.getProperty()).getURI();
    id = "\u2203" + uri.toString() + "." + this.getID();
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "\u2203:", id);
    //oWLtoGraph.setDataPropertyParameters(restriction.getDataProperty(),relNode);
    //OBObjectNode temp = this.getBaseNode();

    this.reset();
    this.setMode(OPERATOR);
    //this.setFromNode(temp); //??
    restriction.getDataType().accept(this);
    oWLtoGraph.createEdge(relNode, this.getBaseNode(), OBEdge.END_EDGE);
    relNode
        .setRelationSubCategory(RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    //restrictNode = relNode;
    outputBaseNode = relNode;
  }

  public OBRelationNode processRestriction(OWLRestriction restriction,
                                           String modifier, String ID) throws
      OWLException {
    //propertyRestriction = true;
    URI uri = (restriction.getProperty()).getURI();
    //String ID;
    /*
     * if(fromNode==null||mode==OPERATOR){ anonimousCounter++; fromNode= new
     * OBObjectNode("anonimous"+String.valueOf(anonimousCounter),"","");
     * esdOntoBrowser.renPolicy.initObject(fromNode, RenderGrowl.CLASS);
     * krPolicyOWL.addNode(fromNode); fromNode.setLabel("restriction");
     * outputNode=fromNode; }
     */

    OBRelationNode relNode = findRelationNode(ID);
    if (relNode == null) {
      relNode = new OBRelationNode(ID, "", "");
      esdOntoBrowser.renPolicy.initRelation(relNode);
      oWLtoGraph.addNode(relNode);
      relNode.setName(oWLtoGraph.getMetadata().shortForm(uri));
      relNode.setNodeLabel(modifier
                           + oWLtoGraph.getMetadata().shortForm(uri));
      //String rdfsLabel = OWLtoGraph.getRDFSLabel(restriction.getProperty(), oWLtoGraph.getOntology());

      //relNode.setHint(OWLtoGraph.getCommment(restriction.getProperty(),oWLtoGraph.getOntology()));

      relNode.setURI(uri);
      relNode.setNamespace(oWLtoGraph.getMetadata().getPrefixOfFullURI(
          uri));

      esdOntoBrowser.renPolicy.initRelation(relNode);

    }

    return relNode;

  }

  boolean isOperator(OBObjectNode n) {
    if (n.objectCategory > 4) {
      return true;
    }
    else {
      return false;
    }
  }

  public void visit(OWLDataAllRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    restriction.getDataType().accept(this);
    URI uri = (restriction.getProperty()).getURI();
    id = "\u2200" + uri.toString() + "." + this.getID();
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "\u2200:", id);
    //oWLtoGraph.setDataPropertyParameters(restriction.getDataProperty(),relNode);
    //OBObjectNode temp = this.getBaseNode();

    this.reset();
    this.setMode(OPERATOR);
    //this.setFromNode(temp); //??
    restriction.getDataType().accept(this);
    oWLtoGraph.createEdge(relNode, this.getBaseNode(), OBEdge.END_EDGE);
    relNode
        .setRelationSubCategory(RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    //restrictNode = relNode;
    outputBaseNode = relNode;
  }

  public void visit(OWLDataValueRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    (restriction.getValue()).accept(this);
    URI uri = (restriction.getProperty()).getURI();
    id = "=" + uri.toString() + "." + this.getID();
    mode = modeBuffer;
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "", id);
    //oWLtoGraph.setDataPropertyParameters(restriction.getDataProperty(),relNode);
    //should be attribute not signature!!!
    //OBObjectNode temp = this.getBaseNode();
    try {
      this.reset();
      this.setMode(OPERATOR);
      //this.setFromNode(null);
      //this.setFromNode(temp); //??
      (restriction.getValue()).accept(this);
      OBNode valNode = this.getBaseNode();
      oWLtoGraph.createEdge(relNode, valNode, OBEdge.END_EDGE);
      //Bugg???
    } catch (Exception e) {
      e.printStackTrace();
    }
    relNode
        .setRelationSubCategory(RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    //restrictNode = relNode;
    outputBaseNode = relNode;
  }

  public void visit(OWLDataCardinalityRestriction restriction) throws
      OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    processCardRestrictionID(restriction);
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode restrictNode = processCardRestriction(restriction,
        false, id);
    //oWLtoGraph.setDataPropertyParameters(restriction.getDataProperty(),restrictNode);
    restrictNode.resetCategory(esdOntoBrowser.renPolicy);
    outputBaseNode = restrictNode;
  }

  public void processCardRestrictionID(OWLCardinalityRestriction restriction) throws
      OWLException {
    String URI = "." + (restriction.getProperty()).getURI().toString();
    if (restriction.isExactly()) {
      id = "=" + String.valueOf(restriction.getAtMost()) + URI;
    } else if (restriction.isAtLeast() && restriction.isAtMost()) {
      id = "[" + String.valueOf(restriction.getAtLeast()) + ","
          + String.valueOf(restriction.getAtMost()) + "]" + URI;
    } else if (restriction.isAtLeast()) {
      id = "\u2265" + String.valueOf(restriction.getAtLeast()) + URI;
    } else if (restriction.isAtMost()) {
      id = "\u2264" + String.valueOf(restriction.getAtMost()) + URI;
    }
  }

  public OBRelationNode processCardRestriction(
      OWLCardinalityRestriction restriction, boolean isObjectProperty,
      String ID1) throws OWLException {
    //propertyRestriction = true;
    URI uri = (restriction.getProperty()).getURI();
    anonimousCounter++;
    String ID = ID1 + String.valueOf(anonimousCounter);
    OBRelationNode rNode = new OBRelationNode(ID, "", "");
    rNode.setName(oWLtoGraph.getMetadata().shortForm(uri));
    rNode.setNodeLabel(oWLtoGraph.getMetadata().shortForm(uri));

    /*String rdfsLabel = OWLtoGraph.getRDFSLabel(restriction.getProperty(),oWLtoGraph.getOntology());
    if (rdfsLabel != null) {
      rNode.setNodeLabel(oWLtoGraph.getMetadata()
                         .getNamespacePrefix(uri)
                         + rdfsLabel);

    }*/

    //rNode.setHint(OWLtoGraph.getCommment(restriction.getProperty(), oWLtoGraph.getOntology()));

    rNode.setURI(uri);
    rNode.setNamespace(oWLtoGraph.getMetadata().getPrefixOfFullURI(uri));

    oWLtoGraph.addNode(rNode);
    esdOntoBrowser.renPolicy.initRelation(rNode);
    /*
     * if (isOperator(fromNode)) krPolicyOWL.createEdge(fromNode, rNode,
     * OBEdge.ARGUMENT_EDGE); else krPolicyOWL.createEdge(fromNode, rNode,
     * OBEdge.START_EDGE);
     */

    ID = (String) propRangeIDs.get(uri.toString());
    OBObjectNode toNode = (OBObjectNode) tgPanel.getCompleteEltSet()
        .findNode(ID);
    if (toNode != null) {
      oWLtoGraph.createEdge(rNode, toNode, OBEdge.END_EDGE);
    }
    if (restriction.isExactly()) {

      rNode.setNodeLabel(String.valueOf(restriction.getAtMost()) + ":"
                         + rNode.getNodeLabel());
      rNode.minCardinality = rNode.maxCardinality = restriction.getAtMost();
    } else if (restriction.isAtLeast() && restriction.isAtMost()) {
      rNode.setNodeLabel(" [" + String.valueOf(restriction.getAtLeast())
                         + "," + String.valueOf(restriction.getAtMost()) + "]:"
                         + rNode.getNodeLabel());
      rNode.minCardinality = restriction.getAtLeast();
      rNode.maxCardinality = restriction.getAtMost();
    } else if (restriction.isAtLeast()) {
      rNode.setNodeLabel("\u2265"
                         + String.valueOf(restriction.getAtLeast()) + ':'
                         + rNode.getNodeLabel());
      rNode.minCardinality = restriction.getAtLeast();
    } else if (restriction.isAtMost()) {
      rNode.setNodeLabel("\u2264"
                         + String.valueOf(restriction.getAtMost()) + ':'
                         + rNode.getNodeLabel());
      rNode.maxCardinality = restriction.getAtMost();
    }
    if (isObjectProperty) {
      rNode
          .setRelationSubCategory(RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD);
    } else {
      rNode
          .setRelationSubCategory(RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD);

    }
    return rNode;
  }

  //Object Properties
  public void visit(OWLObjectSomeRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    restriction.getDescription().accept(this);
    URI uri = (restriction.getProperty()).getURI();
    id = "\u2203" + uri.toString() + "." + this.getID();
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "\u2203:", id);
    //oWLtoGraph.setObjPropertyParameters(restriction.getObjectProperty(),relNode);
    //OBObjectNode temp = this.getBaseNode();
    this.reset();
    this.setMode(OPERATOR);
    //this.setFromNode(temp); //??
    restriction.getDescription().accept(this);
    oWLtoGraph.createEdge(relNode, this.getBaseNode(), OBEdge.END_EDGE);
    relNode
        .setRelationSubCategory(RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    outputBaseNode = relNode;

  }

  public void visit(OWLObjectAllRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    restriction.getDescription().accept(this);
    URI uri = (restriction.getProperty()).getURI();
    id = "\u2200" + uri.toString() + "." + this.getID();
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "\u2200:", id);
    //oWLtoGraph.setObjPropertyParameters(restriction.getObjectProperty(),relNode);
    //OBObjectNode temp = this.getBaseNode();
    this.reset();
    this.setMode(OPERATOR);
    //this.setFromNode(temp); //???
    restriction.getDescription().accept(this);
    oWLtoGraph.createEdge(relNode, this.getBaseNode(), OBEdge.END_EDGE);
    relNode
        .setRelationSubCategory(RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    //restrictNode = relNode;
    outputBaseNode = relNode;
  }

  public void visit(OWLObjectCardinalityRestriction restriction) throws
      OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    processCardRestrictionID(restriction);
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode restrictNode = processCardRestriction(restriction, true, id);
    //oWLtoGraph.setObjPropertyParameters(restriction.getObjectProperty(), restrictNode);
    restrictNode.resetCategory(esdOntoBrowser.renPolicy);
    outputBaseNode = restrictNode;
  }

  public void visit(OWLObjectValueRestriction restriction) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    this.reset();
    this.setMode(GETID);
    try {
      findNode(restriction.getIndividual());
    } catch (OWLException e1) {
      e1.printStackTrace();
    } catch (TGException e1) {
      e1.printStackTrace();
    }
    URI uri = (restriction.getProperty()).getURI();
    id = "\u2200" + uri.toString() + "." + this.getID();
    mode = modeBuffer;
    if (modeBuffer == GETID) {
      return;
    }
    //this.setFromNode(fromNodeBuffer);
    OBRelationNode relNode = processRestriction(restriction, "", id);
    //oWLtoGraph.setObjPropertyParameters(restriction.getObjectProperty(), relNode);
    //OBObjectNode temp = this.getBaseNode();
    try {
      oWLtoGraph.createEdge(relNode, findNode(restriction
                                              .getIndividual()),
                            OBEdge.END_EDGE);
    } catch (Exception e) {
      e.printStackTrace();
    }
    relNode
        .setRelationSubCategory(RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE);
    relNode.resetCategory(esdOntoBrowser.renPolicy);
    //restrictNode = relNode;
    outputBaseNode = relNode;
  }

  //Set Theoretic

  public void visit(OWLNot oWLNot) throws OWLException {
    //OBObjectNode temp = fromNode;
    int modeBuffer = mode;
    boolean label = false;
    //finding ID
    this.reset();
    this.setMode(GETID);
    oWLNot.getOperand().accept(this);
    String ID = "not" + this.getID();
    if (modeBuffer == GETID) {
      return;
    }
    OBObjectNode notNode = (OBObjectNode) tgPanel.getCompleteEltSet()
        .findNode(ID);

    if (notNode == null) {
      notNode = new OBObjectNode(ID, "", "");
      oWLtoGraph.addNode(notNode);
      this.reset();
      this.setMode(OPERATOR);
      oWLNot.getOperand().accept(this);
      OBNode operandNode = this.getBaseNode();

      //OBRelationNode newRNode= new OBRelationNode(
      // "relation"+ID,"","");
      // esdOntoBrowser.renPolicy.initRelation(newRNode,
      // OBRelationNode.IS_DISJOINT_WITH);
      //krPolicyOWL.addNode(newRNode);
      oWLtoGraph.createEdge(notNode, operandNode, OBEdge.END_EDGE);
      //  krPolicyOWL.createEdge(newRNode,operandNode, OBEdge.END_EDGE);
      //  newRNode.setRelationCategory(OBRelationNode.IS_DISJOINT_WITH);
      //  newRNode.resetCategory(esdOntoBrowser.renPolicy);

      ( (RenderGrowl) esdOntoBrowser.renPolicy).initOperator(notNode,
          RenderGrowl.OBJ_COMPLEMENT);
    }
    outputBaseNode = notNode;
    /*
     * if (modeBuffer == SUPERCLASS) { createRelation(temp, notNode,
     * RenderGrowl.REL_SUBCLASS); } else if (modeBuffer == INSTANCE) {
     * createRelation(temp, notNode, RenderGrowl.REL_INSTANCE); }
     */

  }

  public void visit(OWLAnd oWLAnd) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode temp = fromNode;
    String OPID = createOperatorID(oWLAnd.getOperands(), "and");
    id = OPID;

    /*
     * if (mode == SAME_AS) { Set ops = oWLAnd.getOperands(); for (Iterator
     * it0 = ops.iterator(); it0.hasNext();) { OWLDescription operand =
     * (OWLDescription) it0.next(); //buildRelation((OWLNamedObject)clazz,
     * superclass, // OBRelationNode.IS_SUBCLASS); this.reset();
     * this.setMode(SUPERCLASS); this.setFromNode(temp);
     * operand.accept(this); } } else
     */
    mode = modeBuffer;
    if (modeBuffer == GETID) {
      return;
    } else {
      outputBaseNode = createOperator(oWLAnd.getOperands(),
                                      RenderGrowl.OBJ_INTERSECTION, OPID);
      /*
       * this.setFromNode(temp); if (mode == SAME_AS) { outputBaseNode =
       * createOperator(oWLAnd.getOperands(), RenderGrowl.OBJ_INTERSECTION,
       * OPID);
       *
       * createEqRelation(temp, outputBaseNode);
       *  } else if (mode == SUPERCLASS || mode == INSTANCE) { int type; if
       * (mode == SUPERCLASS) type = RenderGrowl.REL_SUBCLASS; else type =
       * RenderGrowl.REL_INSTANCE;
       *
       * outputBaseNode = createOperator(oWLAnd.getOperands(),
       * RenderGrowl.OBJ_INTERSECTION, OPID); createRelation(temp,
       * outputBaseNode, type); } else { //propertyRestriction = false;
       * outputBaseNode = createOperator(oWLAnd.getOperands(),
       * RenderGrowl.OBJ_INTERSECTION, OPID); }
       */
    }
  }

  public void visit(OWLOr oWLOr) throws OWLException {
    int modeBuffer = mode;
    //OBObjectNode temp = fromNode;
    String OPID = createOperatorID(oWLOr.getOperands(), "or");
    id = OPID;
    mode = modeBuffer;
    if (mode == GETID) {
      return;
    }
    else {
      outputBaseNode = createOperator(oWLOr.getOperands(),
                                      RenderGrowl.OBJ_UNION, OPID);
      /*
       * this.setFromNode(temp); if (mode == SUPERCLASS || mode == INSTANCE) {
       * int type; if (mode == SUPERCLASS) type = RenderGrowl.REL_SUBCLASS;
       * else type = RenderGrowl.REL_INSTANCE; outputBaseNode =
       * createOperator(oWLOr.getOperands(), RenderGrowl.OBJ_UNION, OPID);
       * createRelation(fromNode, outputBaseNode, type); } else if (mode ==
       * SAME_AS) { //propertyRestriction = false; outputBaseNode =
       * createOperator(oWLOr.getOperands(), RenderGrowl.OBJ_UNION, OPID);
       * createEqRelation(temp, outputBaseNode); //?? } else { outputBaseNode =
       * createOperator(oWLOr.getOperands(), RenderGrowl.OBJ_UNION, OPID); }
       */
    }
  }

  public String createOperatorID(Set ops, String opName) {
    SortedSet opIDs = new TreeSet();
    String ID = opName;
    try {
      for (Iterator it = ops.iterator(); it.hasNext(); ) {
        OWLObject opDescr = (OWLObject) it.next();
        this.reset();
        this.setMode(GETID);
        opDescr.accept(this);
        opIDs.add(this.getID());
      }
    }
    catch (OWLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    //compute ID
    for (Iterator itID = opIDs.iterator(); itID.hasNext(); ) {
      ID = ID + (String) itID.next();
    }

    return ID;

  }

  public OBObjectNode createOperator(Set ops, int type, String ID) {
    OBObjectNode outNode = (OBObjectNode) tgPanel.getCompleteEltSet()
        .findNode(ID);

    if (outNode != null) {
      return outNode;
    }
    outNode = new OBObjectNode(ID, "", "");
    oWLtoGraph.addNode(outNode);

    try {
      for (Iterator it = ops.iterator(); it.hasNext(); ) {
        OWLObject opDescr = (OWLObject) it.next();
        this.reset();
        this.setMode(OPERATOR);
        //this.setFromNode(outNode);
        opDescr.accept(this);
        //if it was not restriction node , create argument edge
        //if (restrictNode == null) {
        OBNode operandNode = this.getBaseNode();
        OBEdge theEdge = new OBEdge(outNode, operandNode,
                                    OBEdge.ARGUMENT_EDGE);
        tgPanel.getCompleteEltSet().addEdge(theEdge);
        theEdge.setVisible(true);
        //}

      }
    }
    catch (OWLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    ( (RenderGrowl) esdOntoBrowser.renPolicy).initOperator(outNode, type);
    // newRNode.setRelationCategory(type);
    //newRNode.resetCategory(esdOntoBrowser.renPolicy);
    //outputBaseNode = outNode;
    return outNode;
  }

  //

  public void createEqRelation(OBNode fromNode, OBNode toNode) {
    oWLtoGraph.createEdge(fromNode, toNode, OBEdge.EQUIVALENT_EDGE);

    /*
     * String ID = fromNode.getID() + "AreEquivalent" + toNode.getID();
     * OBRelationNode newRNode = new OBRelationNode(ID, "", "");
     * esdOntoBrowser.renPolicy.initRelation( newRNode,
     * RenderGrowl.REL_ARE_EQUIVALENT); //add them
     * krPolicyOWL.addNode(newRNode); krPolicyOWL.createEdge(newRNode,
     * fromNode, OBEdge.END_EDGE); krPolicyOWL.createEdge(newRNode, toNode,
     * OBEdge.END_EDGE); newRNode.resetCategory(esdOntoBrowser.renPolicy);
     * return newRNode;
     */
  }

  public void visit(OWLClass clazz) throws OWLException {
    //propertyRestriction = false;
    OBObjectNode toNode;
    if (mode == GETID) {
      try {
        outputBaseNode = findNode(clazz);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      return;

    }
    else {
      try {
        outputBaseNode = findNode(clazz);
      }
      catch (Exception e) {
        e.printStackTrace();
      }

    }
    /*
     * else if (mode == SAME_AS) { try { toNode = findNode(clazz);
     *
     * createEqRelation(fromNode, toNode); } catch (Exception e) {
     * e.printStackTrace(); }
     *  } else if (mode == SUPERCLASS || mode == INSTANCE) { int type; if
     * (mode == SUPERCLASS) type = RenderGrowl.REL_SUBCLASS; else type =
     * RenderGrowl.REL_INSTANCE; try { toNode = findNode(clazz);
     * createRelation(fromNode, toNode, type); } catch (Exception e) {
     * e.printStackTrace(); }
     *  }
     */
  }

  public void createRelation(OBObjectNode fromNode, OBObjectNode toNode,
                             int type) {

    String ID = fromNode.getID() + "isa" + String.valueOf(type)
        + toNode.getID();
    if (findRelationNode(ID) != null) {
      return;
    }
    OBRelationNode newRNode = new OBRelationNode(ID, "", "");
    esdOntoBrowser.renPolicy.initRelation(newRNode, type);

    //add them
    oWLtoGraph.addNode(newRNode);
    oWLtoGraph.createEdge(fromNode, newRNode, OBEdge.START_EDGE);
    oWLtoGraph.createEdge(newRNode, toNode, OBEdge.END_EDGE);
    newRNode.resetCategory(esdOntoBrowser.renPolicy);

  }

  //needed
  public void visit(OWLDataType oWLDataType) throws OWLException {
    URI uri = oWLDataType.getURI();
    id = uri.toString();
    if (mode == GETID) {
      return;
    }
    datatypeCounter++;
    String ID = uri.toString() + String.valueOf(datatypeCounter);
    OBObjectNode newObj = new OBObjectNode(ID, "", "");
    esdOntoBrowser.renPolicy.initObject(newObj, RenderGrowl.OBJ_DATATYPE);
    newObj.setName(uri.getFragment());
    //krPolicyOWL.getMetadata().shortForm(uri));

    newObj.setURI(uri);
    newObj.setNamespace(oWLtoGraph.getMetadata().getPrefixOfFullURI(uri));
    oWLtoGraph.addNode(newObj);
    outputBaseNode = newObj;
  }

  public void visit(OWLDataEnumeration enumeration) throws OWLException {
    int mode = this.mode;
    id = createOperatorID(enumeration.getValues(), "oneOf");
    if (mode == GETID) {
      return;
    }
    outputBaseNode = createOperator(enumeration.getValues(),
                                    RenderGrowl.OBJ_ONE_OF, id);
    /*
     * try { anonimousCounter++; OBObjectNode enNode = new OBObjectNode(
     * "anonimous" + String.valueOf(anonimousCounter), "", "");
     * esdOntoBrowser.renPolicy.initObject( enNode,
     * RenderGrowl.OBJ_DATATYPE); krPolicyOWL.addNode(enNode);
     * enNode.setLabel("dataRange"); anonimousCounter++; OBRelationNode
     * newRNode = new OBRelationNode( "anonimous" +
     * String.valueOf(anonimousCounter), "", "");
     * esdOntoBrowser.renPolicy.initRelation( newRNode,
     * RenderGrowl.REL_ONE_OF); krPolicyOWL.addNode(newRNode);
     *
     * krPolicyOWL.createEdge(enNode, newRNode, OBEdge.START_EDGE);
     *
     * for (Iterator it = enumeration.getValues().iterator(); it.hasNext(); ) {
     * OWLDataValue val = (OWLDataValue) it.next(); this.reset();
     * this.setMode(OPERATOR); this.setFromNode(null); val.accept(this);
     * OBObjectNode valNode = this.getObject();
     * krPolicyOWL.createEdge(newRNode, valNode, OBEdge.END_EDGE); }
     * newRNode.setRelationSubCategory(RenderGrowl.REL_ONE_OF);
     * newRNode.resetCategory(esdOntoBrowser.renPolicy);
     *
     * outputNode = enNode; } catch (Exception e) { e.printStackTrace(); }
     */

  }

  public void visit(OWLDataValue val) throws OWLException {
    /*
     * anonimousCounter++; OBObjectNode newObj = new OBObjectNode(
     * "anonimous" + String.valueOf(anonimousCounter), "", "");
     *
     * esdOntoBrowser.renPolicy.initObject(newObj,
     * RenderGrowl.OBJ_DATAVALUE);
     * newObj.setLabel(String.valueOf(val.getValue()));
     * newObj.setURI(val.getURI()); krPolicyOWL.addNode(newObj); outputNode =
     * newObj;
     */
    try {
      outputBaseNode = findNode(val);
    }
    catch (OWLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (TGException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void visit(OWLSubClassAxiom axiom) throws OWLException {
    this.reset();
    this.setMode(OPERATOR);
    axiom.getSubClass().accept(this);
    OBNode sub = this.getBaseNode();

    this.reset();
    this.setMode(OPERATOR);
    axiom.getSuperClass().accept(this);
    OBNode superClass = this.getBaseNode();
    //createRelation(sub, superClass, RenderGrowl.REL_SUBCLASS);
    oWLtoGraph.createEdge(sub, superClass, OBEdge.SUBCLASS_OF_EDGE);

  }

  public void visit(OWLEquivalentClassesAxiom axiom) throws OWLException {
    createMultiRelation(axiom.getEquivalentClasses(),
                        RenderGrowl.REL_ARE_EQUIVALENT, false);

  }

  public void visit(OWLDisjointClassesAxiom axiom) throws OWLException {
    createMultiRelation(axiom.getDisjointClasses(),
                        RenderGrowl.REL_ARE_DISJOINT, false);

  }

  public void visit(OWLDifferentIndividualsAxiom ax) throws OWLException {
    createMultiRelation(ax.getIndividuals(), RenderGrowl.REL_ARE_DIFFERENT,
                        true);

  }

  public void visit(OWLSameIndividualsAxiom ax) throws OWLException {
    createMultiRelation(ax.getIndividuals(), RenderGrowl.REL_ARE_SAME, true);
  }

  private void createMultiRelation(Set ops, int type,
                                   boolean operandsAreIndividuals) throws
      OWLException {
    if (ops.size() < 2) {
      return;
    }
    anonimousCounter++;
    Set operandNodes = new HashSet();
    SortedSet opIDs = new TreeSet();

    if (operandsAreIndividuals) {
      for (Iterator it = ops.iterator(); it.hasNext(); ) {
        OWLIndividual desc = (OWLIndividual) it.next();
        this.reset();
        this.setMode(OPERATOR);
        desc.accept(this);
        OBNode operandNode = this.getBaseNode();
        operandNodes.add(operandNode);
        opIDs.add(operandNode.getID());
        //krPolicyOWL.createEdge( newRNode,this.getObject(),
        // OBEdge.END_EDGE);

      }
    }
    else {
      for (Iterator it = ops.iterator(); it.hasNext(); ) {
        OWLDescription desc = (OWLDescription) it.next();
        this.reset();
        this.setMode(OPERATOR);
        desc.accept(this);
        OBNode operandNode = this.getBaseNode();
        operandNodes.add(operandNode);
        opIDs.add(operandNode.getID());
        //krPolicyOWL.createEdge( newRNode,this.getObject(),
        // OBEdge.END_EDGE);

      }
    }

    String ID = String.valueOf(type);
    //compute ID
    for (Iterator itID = opIDs.iterator(); itID.hasNext(); ) {
      ID = ID + (String) itID.next();
    }

    OBRelationNode relNode = (OBRelationNode) tgPanel.getCompleteEltSet()
        .findNode(ID);
    if (relNode != null) {
      return;
    }
    else {
      relNode = new OBRelationNode(ID, "", "");
      esdOntoBrowser.renPolicy.initRelation(relNode, type);
      oWLtoGraph.addNode(relNode);
      for (Iterator it = operandNodes.iterator(); it.hasNext(); ) {

        OBObjectNode operandNode = (OBObjectNode) it.next();
        OBEdge theEdge = new OBEdge(relNode, operandNode,
                                    OBEdge.END_EDGE);
        tgPanel.getCompleteEltSet().addEdge(theEdge);
        theEdge.setVisible(true);

      }

      relNode.setRelationSubCategory(type);
      relNode.resetCategory(esdOntoBrowser.renPolicy);

    }

  }

  public void visit(OWLIndividual ind) throws OWLException {
    try {
      outputBaseNode = findNode(ind);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  public void visit(OWLSubPropertyAxiom oWLSubPropertyAxiom) throws
      OWLException {
    try {
      OBRelationNode from = findRelationNode(oWLSubPropertyAxiom
                                             .getSubProperty().getURI().
                                             toString());
      OBRelationNode to = findRelationNode(oWLSubPropertyAxiom
                                           .getSuperProperty().getURI().
                                           toString());
      oWLtoGraph.createEdge(from, to, OBEdge.SUBCLASS_OF_EDGE);
    }
    catch (OWLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void visit(OWLEquivalentPropertiesAxiom oWLEquivalentPropertiesAxiom) throws
      OWLException {
    Set props = oWLEquivalentPropertiesAxiom.getProperties();

    anonimousCounter++;
    OBRelationNode rNode = new OBRelationNode("EQ"
                                              + String.valueOf(anonimousCounter),
                                              "", "");
    esdOntoBrowser.renPolicy.initRelation(rNode,
                                          RenderGrowl.REL_ARE_EQUIVALENT);

    oWLtoGraph.addNode(rNode);
    for (Iterator iter = props.iterator(); iter.hasNext(); ) {
      OWLProperty prop = (OWLProperty) iter.next();
      OBNode propNode = findRelationNode(prop.getURI().toString());
      OBEdge theEdge = new OBEdge(rNode, propNode, OBEdge.END_EDGE);
      tgPanel.getCompleteEltSet().addEdge(theEdge);
      theEdge.setVisible(true);

    }

    rNode.resetCategory(esdOntoBrowser.renPolicy);

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLAnnotationProperty)
   */
  public void visit(OWLAnnotationProperty node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLAnnotationInstance)
   */
  public void visit(OWLAnnotationInstance node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLDataProperty)
   */
  public void visit(OWLDataProperty node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLFrame)
   */
  public void visit(OWLFrame node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLObjectProperty)
   */
  public void visit(OWLObjectProperty node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLOntology)
   */
  public void visit(OWLOntology node) throws OWLException {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.semanticweb.owl.model.OWLObjectVisitor#visit(org.semanticweb.owl.model.OWLEnumeration)
   */
  public void visit(OWLEnumeration enumeration) throws OWLException {
    int modebuffer = mode;
    //OBObjectNode fromNodeBuffer = fromNode;
    id = createOperatorID(enumeration.getIndividuals(), "oneOf");
    if (modebuffer == GETID) {
      return;
    }
    OBObjectNode oneOfNode = createOperator(enumeration.getIndividuals(),
                                            RenderGrowl.OBJ_ONE_OF, id);
    /*
     * if (modebuffer == SAME_AS) createEqRelation(fromNodeBuffer,
     * outputBaseNode); else if (modebuffer == SUPERCLASS || modebuffer ==
     * INSTANCE) { int type; if (modebuffer == SUPERCLASS) type =
     * RenderGrowl.REL_SUBCLASS; else type = RenderGrowl.REL_INSTANCE;
     *
     * createRelation(fromNodeBuffer, outputBaseNode, type); }
     */
    RenderGrowl.resetOperatorEdges(oneOfNode);
    outputBaseNode = oneOfNode;
    /*
     * try { // OBRelationNode newRNode = new OBRelationNode(null, "", "");
     * esdOntoBrowser.renPolicy.initRelation(newRNode);
     *
     * //add them krPolicyOWL.addNode(newRNode);
     * krPolicyOWL.createEdge(fromNode, newRNode, OBEdge.START_EDGE); for
     * (Iterator iit = enumeration.getIndividuals().iterator();
     * iit.hasNext(); ) { OWLIndividual owlInd = (OWLIndividual) iit.next();
     * OBObjectNode toNode = this.findNode(owlInd);
     *
     * //add start edge krPolicyOWL.createEdge(newRNode, toNode,
     * OBEdge.END_EDGE); }
     * newRNode.setRelationSubCategory(RenderGrowl.REL_ONE_OF);
     * newRNode.resetCategory(esdOntoBrowser.renPolicy); } catch
     * (OWLException e) {
     *
     * e.printStackTrace(); } catch (TGException e) {
     *
     * e.printStackTrace(); }
     */

  }

  /*
   * not needed public void visit(OWLAnnotationInstance oWLAnnotationInstance)
   * throws OWLException { }
   *
   * public void visit(OWLDisjointClassesAxiom oWLDisjointClassesAxiom) throws
   * OWLException { }
   *
   * public void visit(OWLSameIndividualsAxiom oWLSameIndividualsAxiom) throws
   * OWLException { }
   *
   *
   * public void visit(OWLDataProperty oWLDataProperty) throws OWLException { }
   *
   * public void visit(OWLDifferentIndividualsAxiom
   * oWLDifferentIndividualsAxiom) throws OWLException { }
   *
   *
   *
   * public void visit(OWLOntology oWLOntology) throws OWLException { }
   *
   *
   *
       * public void visit(OWLSubClassAxiom oWLSubClassAxiom) throws OWLException { }
   *
   * public void visit(OWLEquivalentClassesAxiom oWLEquivalentClassesAxiom)
   * throws OWLException { }
   *
   *
   *
   * public void visit(OWLAnnotationProperty oWLAnnotationProperty) throws
   * OWLException { }
   *
   * public void visit(OWLObjectProperty oWLObjectProperty) throws
   * OWLException { }
   *
   * public void visit(OWLIndividual oWLIndividual) throws OWLException { }
   *
   */

}