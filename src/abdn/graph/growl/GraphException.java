/*
 * GraphException.java
 * Created on Aug 19, 2004
 *
 */
package abdn.graph.growl;

import abdn.graph.ontobrowser.OBNode;

/**
 * GraphException.java
 * @author Rich
 *
 */
public class GraphException extends Exception
{
  OBNode node;

  public GraphException(String msg, OBNode n)
  {
    super(msg);
    node = n;
  }

  public OBNode getNode()
  {
    return node;
  }
}
