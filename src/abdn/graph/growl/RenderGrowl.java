/*
 * RenderGrowl.java
 *
 * Created on December 17, 2003, 10:20 AM
 */

package abdn.graph.growl;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import abdn.graph.ontobrowser.OBEdge;
import abdn.graph.ontobrowser.OBNode;
import abdn.graph.ontobrowser.OBObjectNode;
import abdn.graph.ontobrowser.OBRelationNode;
import abdn.graph.ontobrowser.RenderPolicy;

import com.touchgraph.graphlayout.Edge;
import com.touchgraph.graphlayout.Node;

/**
 *
 * @author skrivov
 */

public class RenderGrowl
    implements RenderPolicy {
  //object node constants (Enumeration)

  public static final int OBJ_INDIVIDUAL = 1;

  public static final int OBJ_DATAVALUE = 2;

  public static final int OBJ_CLASS = 4;

  public static final int OBJ_DATATYPE = 3;

  public static final int OBJ_INTERSECTION = 5;

  public static final int OBJ_UNION = 6;

  public static final int OBJ_COMPLEMENT = 7;

  public static final int OBJ_ONE_OF = 8;

  public static final int OBJ_DISJOINT = 9;

  public static final int OBJ_ATTRIBUTE = 10;

  public static final int OBJ_CONSISTENT = 11;

  //relation node constants (Enumeration)

  public static final int REL_OBJECT_PROPERTY_VALUE = 1;

  public static final int REL_DATA_PROPERTY_VALUE = 2;

  public static final int REL_OBJECT_PROPERTY = 3;

  public static final int REL_DATA_PROPERTY = 4;

  public static final int REL_OBJECT_PROPERTY_RESTRICT_SOME = 5;

  public static final int REL_OBJECT_PROPERTY_RESTRICT_ALL = 6;

  public static final int REL_OBJECT_PROPERTY_RESTRICT_VALUE = 7;

  public static final int REL_OBJECT_PROPERTY_RESTRICT_CARD = 8;

  public static final int REL_DATA_PROPERTY_RESTRICT_SOME = 9;

  public static final int REL_DATA_PROPERTY_RESTRICT_ALL = 10;

  public static final int REL_DATA_PROPERTY_RESTRICT_VALUE = 11;

  public static final int REL_DATA_PROPERTY_RESTRICT_CARD = 12;

  //public static final int REL_SUBCLASS = 13;

  //public static final int REL_INSTANCE = 14;

  public static final int REL_ARE_EQUIVALENT = 15;

  public static final int REL_ARE_SAME = 16;

  public static final int REL_ARE_DIFFERENT = 17;

  public static final int REL_ARE_DISJOINT = 18;

  //public static final int REL_IS_SAME_AS = 19;
  //public static final int REL_ONE_OF = 20;
  public static final int REL_INVERSE_OF = 21;

  public static int attrValueMaxLength = 12;

  ///OLD THINGS

  //public static String INTERSECTION_LABEL = "Intersection";
  //public static String UNION_LABEL = "Union";

  //public static String DISJOINT_UNION_LABEL = "DUnion";
  //public static String DISJOINT_WITH_LABEL = "DisjointWith";
  public static String SAME_AS_LABEL = "=";

  public static String INSTANCE_LABEL = "is-a";

  public static String SUBCLASS_LABEL = "subclass-of";

  public static String EQUIVALENT_LABEL = "=";

  public static String DIFFERENT_LABEL = "Different";

  public static String SAME_LABEL = "=";

  public static String DISJOINT_LABEL = "Disjoint";

  public static String ONE_OF_LABEL = "OneOf";

  public static String INVERSE_OF_LABEL = "InverseOf";

  public static String UNION_LABEL = "Union";

  public static String INTERSECTION_LABEL = "Intersect";

  public static String COMPLEMENT_LABEL = "Complement";

  static String STRING_LABEL = "string";

  public static final String defaultNS = "<default>";

  public static Image UNION_ICON = null;

  public static Image INTERSECTION_ICON = null;

  public static Image COMPLEMENT_ICON = null;

  public static Image ONE_OF_ICON = null;

  public static Image SUBCLASS_ICON = null;

  public static Image INSTANCE_ICON = null;

  public static Image DISJOINT_ICON = null;

  public static Image SAME_ICON = null;

  public static Image DIFFERENT_ICON = null;

  public static Image EQUIVALENT_ICON = null;

  public static Image INVERSE_OF_ICON = null;

  public void LoadImages() {
    URL url = null;
    Class ThisClass = this.getClass();
    Toolkit toolkit = Toolkit.getDefaultToolkit();

    url = ThisClass.getResource("images/union.gif");
    RenderGrowl.UNION_ICON = toolkit.getImage(url);
    Node.RECTANGLE_H = 10;
    Node.RECTANGLE_W = 20;
    url = ThisClass.getResource("images/intersection.gif");
    RenderGrowl.INTERSECTION_ICON = toolkit.getImage(url);

    url = ThisClass.getResource("images/complement.gif");
    RenderGrowl.COMPLEMENT_ICON = toolkit.getImage(url);

    url = ThisClass.getResource("images/oneOf.png");
    RenderGrowl.ONE_OF_ICON = toolkit.getImage(url);

    url = ThisClass.getResource("images/individAreSame.png");
    RenderGrowl.SAME_ICON = toolkit.getImage(url);
    url = ThisClass.getResource("images/individAreDifferent.png");
    RenderGrowl.DIFFERENT_ICON = toolkit.getImage(url);
    url = ThisClass.getResource("images/classesAreEq.png");
    RenderGrowl.EQUIVALENT_ICON = toolkit.getImage(url);
    url = ThisClass.getResource("images/classesAreDisjoint.png");
    RenderGrowl.DISJOINT_ICON = toolkit.getImage(url);
    url = ThisClass.getResource("images/inverse1.png");
    RenderGrowl.INVERSE_OF_ICON = toolkit.getImage(url);

  }

  public static int INSTANCE_SHAPE = OBNode.TYPE_RECTANGLE;

  public static int CLASS_SHAPE = OBNode.TYPE_RECTANGLE;

  //public static int ISA_INSTANCE_SHAPE = OBNode.TYPE_RECTANGLE;

  //public static int ISA_SUBCLASS_SHAPE = OBNode.TYPE_RECTANGLE;

  public static int ATTRIB_SHAPE = OBNode.TYPE_ELLIPSE;

  //public static int MULTYATTRIB_SHAPE = OBNode.TYPE_ELLIPSE;
  //public static int ATTRIB_INHERIT_SHAPE = OBNode.TYPE_ELLIPSE;
  //public static int MULTYATTRIB_INHERIT_SHAPE = OBNode.TYPE_ELLIPSE;
  public static int ATTRIB_SIGN_SHAPE = OBNode.TYPE_ELLIPSE;

  //public static int MULTYATTRIB_SIGN_SHAPE = OBNode.TYPE_ELLIPSE;
  //public static int ATTRIB_INHERIT_SIGN_SHAPE = OBNode.TYPE_ELLIPSE;
  //public static int MULTYATTRIB_INHERIT_SIGN_SHAPE = OBNode.TYPE_ELLIPSE;

  public static int CLASS_STYLE = OBNode.STYLE_NORMAL;

  public static int INSTANCE_STYLE = OBNode.STYLE_NORMAL;

  //public static int ISA_INSTANCE_STYLE = OBNode.STYLE_NORMAL;

  //public static int ISA_SUBCLASS_STYLE = OBNode.STYLE_NORMAL;

  public static int ATTRIB_STYLE = OBNode.STYLE_NORMAL;

  //public static int MULTYATTRIB_STYLE = OBNode.STYLE_NORMAL;
  //public static int ATTRIB_INHERIT_STYLE = OBNode.STYLE_NORMAL;
  //public static int MULTYATTRIB_INHERIT_STYLE = OBNode.STYLE_NORMAL;
  public static int ATTRIB_SIGN_STYLE = OBNode.STYLE_NORMAL;

  //public static int MULTYATTRIB_SIGN_STYLE = OBNode.STYLE_NORMAL;
  //public static int ATTRIB_INHERIT_SIGN_STYLE = OBNode.STYLE_NORMAL;
  //public static int MULTYATTRIB_INHERIT_SIGN_STYLE = OBNode.STYLE_NORMAL;

  public static Color CLASS_BACK_COLOR = new Color(175, 200, 255);
  public static Color DATATYPE_BACK_COLOR = new Color(217, 198, 123);

  public static Color INSTANCE_BACK_COLOR = Color.white;
  public static Color DATAVALUE_BACK_COLOR = Color.white;

  //public static Color ISA_INSTANCE_BACK_COLOR = Color.white;

  //public static Color ISA_SUBCLASS_BACK_COLOR = Color.white;

  public static Color ATTRIB_BACK_COLOR = Color.white;
  public static Color DATA_ATTRIB_BACK_COLOR = Color.white;

  public static Color SIGNATURE_BACK_COLOR = new Color(175, 200, 255);
  public static Color DATA_SIGNATURE_BACK_COLOR = new Color(217, 198, 123);

  /*
   * public static Color VARIABLE_OBJ_BACK_COLOR = Color.white; public static
   * Color VARIABLE_ATTR_BACK_COLOR = Color.white; public static Color
   * VARIABLE_SIGNATURE_BACK_COLOR = Color.green;
   */
  public static Color CLASS_BORDER_COLOR = Color.white;
  public static Color DATATYPE_BORDER_COLOR = Color.white;

  public static Color INSTANCE_BORDER_COLOR = Color.blue;
  public static Color DATAVALUE_BORDER_COLOR = new Color(217, 198, 123);

  //public static Color ISA_INSTANCE_BORDER_COLOR = Color.white;

  //public static Color ISA_SUBCLASS_BORDER_COLOR = Color.white;

  public static Color ATTRIB_BORDER_COLOR = Color.blue;
  public static Color DATA_ATTRIB_BORDER_COLOR = new Color(217, 198, 123);

  public static Color SIGNATURE_BORDER_COLOR = Color.white;
  public static Color DATA_SIGNATURE_BORDER_COLOR = Color.white;

  /*
   * public static Color VARIABLE_OBJ_BORDER_COLOR = Color.green; public
   * static Color VARIABLE_ATTR_BORDER_COLOR = Color.green; public static
   * Color VARIABLE_SIGNATURE_BORDER_COLOR = Color.white;
   */
  public static Color CLASS_TEXT_COLOR = Color.black;
  public static Color DATATYPE_TEXT_COLOR = Color.black;

  public static Color INSTANCE_TEXT_COLOR = Color.black;
  public static Color DATAVALUE_TEXT_COLOR = Color.black;

  //public static Color ISA_INSTANCE_TEXT_COLOR = Color.black;

  //public static Color ISA_SUBCLASS_TEXT_COLOR = Color.black;

  public static Color ATTRIB_TEXT_COLOR = Color.black;
  public static Color DATA_ATTRIB_TEXT_COLOR = Color.black;

  public static Color SIGNATURE_TEXT_COLOR = Color.black;
  public static Color DATA_SIGNATURE_TEXT_COLOR = Color.black;

  /*
   * public static Color VARIABLE_OBJ_TEXT_COLOR = Color.black; public static
   * Color VARIABLE_ATTR_TEXT_COLOR = Color.black; public static Color
   * VARIABLE_SIGNATURE_TEXT_COLOR = Color.black;
   */

  public static Color ISA_INSTANCE_EDGE_COLOR = Color.blue;

  public static Color ISA_SUBCLASS_EDGE_COLOR = Color.blue;

  public static Color ATTRIB_EDGE_COLOR = Color.blue;
  public static Color DATA_ATTRIB_EDGE_COLOR = new Color(217, 198, 123);

  public static Color VAR_ATTRIB_EDGE_COLOR = Color.green;

  public static Color CONSTRAINT_EDGE_COLOR = Color.orange;

  public static Color SIGN_EDGE_COLOR = Color.blue;
  public static Color DATA_SIGN_EDGE_COLOR = new Color(217, 198, 123);

  public static Color VAR_SIGN_EDGE_COLOR = Color.green;

  public static Color CONSTRAINT_BORDER_COLOR = Color.orange;

  public static Color CONSTRAINT_BACK_COLOR = Color.white;

  public static Color CONSTRAINT_TEXT_COLOR = Color.black;

  public static int CONSTRAINT_OPERATOR_SHAPE = OBNode.TYPE_ELLIPSE;

  //edges shape types for various links
  //public static int ISA_START_TYPE = 0;

  //public static int ISA_END_TYPE = 1;

  public static int ISA_TYPE = 2;
  public static final int SUBCLASS_TYPE = 2;
  public static final int SUBCLASS_STYLE = 2;

  public static int ATTR_START_TYPE = 0;

  public static int ATTR_END_TYPE = 2;

  public static int INHATTR_START_TYPE = 0;

  public static int INHATTR_END_TYPE = 2;

  public static int MULTYATTR_START_TYPE = 0;

  public static int MULTYATTR_END_TYPE = 2;

  public static int SIGN_START_TYPE = 0;

  public static int SIGN_END_TYPE = 2;

  public static int INHSIGN_START_TYPE = 0;

  public static int INHSIGN_END_TYPE = 2;

  public static int MULTYSIGN_START_TYPE = 0;

  public static int MULTYSIGN_END_TYPE = 3;

  public static int CONSTRAINT_OPERATOR_START_TYPE = 2;

  public static int CONSTRAINT_OPERATOR_END_TYPE = 2;

  public static int CONSTRAINT_ATTRIBUTE_START_TYPE = 0;

  public static int CONSTRAINT_ATTRIBUTE_END_TYPE = 2;

  public static int CONSTRAINT_MULTY_OPERATOR_END_TYPE = 2;

  public static int INHSIGN_TILE = 1;

  public static int INHATTR_TILE = 1;

  /** Creates a new instance of RenderGrowl */
  public RenderGrowl() {
  }

  public void initClass(OBObjectNode on) {
    on.borderColor = CLASS_BORDER_COLOR;
    on.backColor = CLASS_BACK_COLOR;
    on.textColor = CLASS_TEXT_COLOR;
    on.typ = CLASS_SHAPE;
    on.style = CLASS_STYLE;
    on.objectCategory = OBJ_CLASS;
    on.isClass = true;
  }

  public void initDatatype(OBObjectNode on) {
    on.borderColor = DATATYPE_BORDER_COLOR;
    on.backColor = DATATYPE_BACK_COLOR;
    on.textColor = DATATYPE_TEXT_COLOR;
    on.typ = CLASS_SHAPE;
    on.style = CLASS_STYLE;
    on.objectCategory = OBJ_CLASS;
    on.isClass = true;
  }

  public void initInstance(OBObjectNode on) {
    on.borderColor = INSTANCE_BORDER_COLOR;
    on.backColor = INSTANCE_BACK_COLOR;
    on.textColor = INSTANCE_TEXT_COLOR;
    on.typ = INSTANCE_SHAPE;
    on.style = INSTANCE_STYLE;
    on.objectCategory = OBJ_INDIVIDUAL;
    on.isClass = false;
  }

  public void initDatavalue(OBObjectNode on) {
    on.borderColor = DATAVALUE_BORDER_COLOR;
    on.backColor = DATAVALUE_BACK_COLOR;
    on.textColor = DATAVALUE_TEXT_COLOR;
    on.typ = INSTANCE_SHAPE;
    on.style = INSTANCE_STYLE;
    on.objectCategory = OBJ_INDIVIDUAL;
    on.isClass = false;
  }

  public void initAttribute( OBObjectNode on ) {
    on.borderColor = CLASS_BORDER_COLOR;
    on.backColor = CLASS_BACK_COLOR;
    on.textColor = CLASS_TEXT_COLOR;
    on.typ = ATTRIB_SHAPE;
    on.style = ATTRIB_STYLE;
    on.objectCategory = OBJ_ATTRIBUTE;
    on.isClass = false;
  }

  public void initRelation(OBRelationNode rn) {
    rn.borderColor = ATTRIB_BORDER_COLOR;
    rn.backColor = ATTRIB_BACK_COLOR;
    rn.textColor = ATTRIB_TEXT_COLOR;
    rn.typ = ATTRIB_SHAPE;
    rn.style = ATTRIB_STYLE;
    rn.setRelationSubCategory(REL_DATA_PROPERTY_VALUE);
  }

  public void initObject(OBObjectNode on, int type) {
    if (type == OBJ_CLASS) {
      initClass(on);
    }
    else if (type == OBJ_DATATYPE) {
      initDatatype(on);
    }
    else if (type == OBJ_INDIVIDUAL) {
      initInstance(on);

    }
    else if (type == OBJ_DATAVALUE) {
      initDatavalue(on);

    }
    else if (type > 4) {
      initOperator(on, type);

    }
    on.objectCategory = type;
    if (type == OBJ_DATATYPE) {
      on.setNamespace("xsd");
      on.setLabel(STRING_LABEL);
    }
  }

  public void initOperator(OBObjectNode on, int type) {
    //on.localityIndex=0;
    on.borderColor = Color.WHITE;
    on.backColor = Color.WHITE;

    on.typ = Node.TYPE_ICON;
    on.objectCategory = type;
    on.isClass = true;
    if (type == OBJ_INTERSECTION) {
      if (INTERSECTION_ICON != null) {
        on.setImage(INTERSECTION_ICON);
      }
      else {
        on.setLabel(INTERSECTION_LABEL);
        on.typ = Node.TYPE_RECTANGLE;
      }
      on.setHint(
          "Class of   objects that belong to every  clas pointed by argument arrow");
    }
    else if (type == OBJ_UNION) {
      //on.setLabel("UNION");
      if (UNION_ICON != null) {
        on.setImage(UNION_ICON);
      }
      else {
        on.typ = Node.TYPE_RECTANGLE;
        on.setLabel(UNION_LABEL);
      }
      on.setHint(
          "Class of   objects that belong to at least one  class pointed by argument arrow");
    }
    else if (type == OBJ_COMPLEMENT) {
      if (COMPLEMENT_ICON != null) {
        on.setImage(COMPLEMENT_ICON);
      }
      else {
        on.setLabel(COMPLEMENT_LABEL);
        on.typ = Node.TYPE_RECTANGLE;
      }

      on.setHint(
          "Class of   objects that  does not belong to the  class pointed by argument arrow");
    }
    else if (type == OBJ_ONE_OF) {

      if (ONE_OF_ICON != null) {
        on.setImage(ONE_OF_ICON);
      }
      else {
        on.setLabel(ONE_OF_LABEL);
        on.typ = Node.TYPE_RECTANGLE;
      }

      on.setHint(
          "A class   that  contains only those objects that are pointed by argument arrows");
    }
    else if (type == OBJ_DISJOINT) {
      if (DISJOINT_ICON != null) {
        on.setImage(DISJOINT_ICON);
      }
      else {
        on.typ = Node.TYPE_RECTANGLE;
        on.setLabel(DISJOINT_LABEL);
      }
      on.setHint("A class that contains disjoint objects");
    }
    initOperatorEdges(on);
  }

  public static void resetOperatorEdges(OBObjectNode on) {
    for (int i = 0; i < on.edges.size(); i++) {
      OBEdge edge = (OBEdge) on.edges.elementAt(i);
      if (on.isEdgeFromThis(edge)
          && edge.edgeOrder == OBEdge.ARGUMENT_EDGE) {
        edge.type = CONSTRAINT_OPERATOR_END_TYPE;
        edge.col = CONSTRAINT_EDGE_COLOR;
      }
    }
  }

  public static void initOperatorEdges(OBObjectNode on) {
    for (int i = 0; i < on.edges.size(); i++) {
      OBEdge edge = (OBEdge) on.edges.elementAt(i);
      if (on.isEdgeFromThis(edge)) {
        edge.edgeOrder = OBEdge.ARGUMENT_EDGE;
        edge.type = CONSTRAINT_OPERATOR_END_TYPE;
        edge.col = CONSTRAINT_EDGE_COLOR;
      }
    }
  }

  public void resetEdge(OBEdge ed) {

    if (ed.edgeOrder == OBEdge.SUBCLASS_OF_EDGE
        || ed.edgeOrder == OBEdge.INSTANCE_OF_EDGE
        || ed.edgeOrder == OBEdge.EQUIVALENT_EDGE) {

      ed.type = SUBCLASS_TYPE;
      //ed.col = ISA_SUBCLASS_EDGE_COLOR;
      ed.setStyle(Edge.STYLE_THICK);
      ed.setLength(5 * Edge.DEFAULT_LENGTH);
      if (ed.edgeOrder == OBEdge.EQUIVALENT_EDGE) {
        ed.setLength(Edge.DEFAULT_LENGTH);
        ed.edgeTile = Edge.TILE_ARROW;
      }

    }
    else if (ed.edgeOrder == OBEdge.ARGUMENT_EDGE) {
      ed.type = CONSTRAINT_OPERATOR_END_TYPE;
      ed.col = CONSTRAINT_EDGE_COLOR;

    }
    else if (
        (ed.to instanceof OBRelationNode)
        && (ed.from instanceof OBObjectNode)) {
      resetEdgeCategory(
          ed,
          ( (OBRelationNode) ed.to).relationSubCategory,
          false,
          false,
          false);

    }
    else if ( (ed.from instanceof OBRelationNode)) {

      resetEdgeCategory(
          ed,
          ( (OBRelationNode) ed.from).relationSubCategory,
          false,
          false,
          false);

    }

    /*else if (
     (ed.to instanceof OBRelationNode)
      && ((OBRelationNode) ed.to).relationSubCategory
       == REL_INVERSE_OF) {
     resetEdgeCategory(
      ed,
      ((OBRelationNode) ed.to).relationSubCategory,
      false,
      false,
      false);
       }*/
  }

  /*
    public void resetEdge1(OBEdge ed) {
   if (ed.edgeOrder == OBEdge.SUBCLASS_EDGE
    || ed.edgeOrder == OBEdge.ISA_EDGE
    || ed.edgeOrder == OBEdge.EQVIVALENT_EDGE) {
    ed.type = SUBCLASS_TYPE;
    ed.col = ISA_SUBCLASS_EDGE_COLOR;
    ed.setStyle(Edge.STYLE_THICK);
    ed.setLength(3*Edge.DEFAULT_LENGTH);
    if (ed.edgeOrder == OBEdge.EQVIVALENT_EDGE){
     ed.setLength(Edge.DEFAULT_LENGTH);
     ed.edgeTile = Edge.TILE_ARROW;
    }
   } else if (
    (ed.from instanceof OBRelationNode)
     && (ed.to instanceof OBObjectNode)) {
    ed.edgeOrder = OBEdge.END_EDGE;
    //ed.type =2;
    resetCategory(
     ed,
     ((OBRelationNode) ed.from).relationSubCategory,
     false,
     false,
     false);
    //((OBRelationNode) ed.from).resetRelatedEdges(this);
   } else if (
    (ed.to instanceof OBRelationNode)
     && (ed.from instanceof OBObjectNode)) {
    if (((OBObjectNode) (ed.from)).objectCategory > 4) {
     ed.edgeOrder = OBEdge.ARGUMENT_EDGE;
     ed.type = CONSTRAINT_OPERATOR_END_TYPE;
     ed.col = CONSTRAINT_EDGE_COLOR;
    } else {
     ed.edgeOrder = OBEdge.START_EDGE;
     resetCategory(
      ed,
      ((OBRelationNode) ed.to).relationSubCategory,
      false,
      false,
      false);
     //((OBRelationNode) ed.to).resetRelatedEdges(this);
    }
   } else if (
    (ed.from instanceof OBObjectNode)
     && (ed.to instanceof OBObjectNode)) {
    if (((OBObjectNode) (ed.from)).objectCategory > 4) {
     ed.edgeOrder = OBEdge.ARGUMENT_EDGE;
     ed.type = CONSTRAINT_OPERATOR_END_TYPE;
     ed.col = CONSTRAINT_EDGE_COLOR;
    }
   }
    }*/

  public void initRelation(OBRelationNode rn, int category) {
    //rn.localityIndex=0;
    rn.borderColor = ATTRIB_BORDER_COLOR;
    rn.backColor = ATTRIB_BACK_COLOR;
    rn.textColor = ATTRIB_TEXT_COLOR;
    rn.typ = ATTRIB_SHAPE;
    rn.style = ATTRIB_STYLE;
    rn.setRelationSubCategory(category);
  }

  public void resetRelationCategory(OBRelationNode rn) {
    switch (rn.relationSubCategory) {

      /*case REL_SUBCLASS :
       {
        //rn.setLabel(SUBCLASS_LABEL);
        rn.typ = ISA_SUBCLASS_SHAPE;
        if (SUBCLASS_ICON != null) {
         rn.typ = OBNode.TYPE_ICON;
         rn.setImage(SUBCLASS_ICON);
        } else {
         rn.setLabel(SUBCLASS_LABEL);
         rn.borderColor = ISA_SUBCLASS_BORDER_COLOR;
         rn.backColor = ISA_SUBCLASS_BACK_COLOR;
         rn.textColor = ISA_SUBCLASS_TEXT_COLOR;
        }
        rn.relationCategory = OBRelationNode.IS_SUBCLASS;
        break;
       }
          case REL_INSTANCE :
       {
        rn.typ = ISA_INSTANCE_SHAPE;
        if (INSTANCE_ICON != null) {
         rn.typ = OBNode.TYPE_ICON;
         rn.setImage(INSTANCE_ICON);
        } else {
         rn.setLabel(INSTANCE_LABEL);
         rn.borderColor = ISA_INSTANCE_BORDER_COLOR;
         rn.backColor = ISA_INSTANCE_BACK_COLOR;
         rn.textColor = ISA_INSTANCE_TEXT_COLOR;
        }
        rn.relationCategory = OBRelationNode.IS_INSTANCE;
        break;
       }*/

      case REL_OBJECT_PROPERTY_VALUE:
      case REL_OBJECT_PROPERTY_RESTRICT_VALUE: {
        //first reset color
        if (!rn.isvariable) {
          rn.borderColor = ATTRIB_BORDER_COLOR;
          rn.backColor = ATTRIB_BACK_COLOR;
          rn.textColor = ATTRIB_TEXT_COLOR;

        }
        else {
          /*
           * rn.borderColor=VARIABLE_ATTR_BORDER_COLOR;
           * rn.backColor=VARIABLE_ATTR_BACK_COLOR;
           * rn.textColor=VARIABLE_ATTR_TEXT_COLOR;
           */
        }

        //now set shape
        rn.typ = ATTRIB_SHAPE;
        rn.style = ATTRIB_STYLE;
        rn.relationCategory = OBRelationNode.IS_ATTRIBUTE;
        break;
      }

      case REL_DATA_PROPERTY_VALUE:
      case REL_DATA_PROPERTY_RESTRICT_VALUE:

      {
        //first reset color
        if (!rn.isvariable) {
          rn.borderColor = DATA_ATTRIB_BORDER_COLOR;
          rn.backColor = DATA_ATTRIB_BACK_COLOR;
          rn.textColor = DATA_ATTRIB_TEXT_COLOR;

        }
        else {
          /*
           * rn.borderColor=VARIABLE_ATTR_BORDER_COLOR;
           * rn.backColor=VARIABLE_ATTR_BACK_COLOR;
           * rn.textColor=VARIABLE_ATTR_TEXT_COLOR;
           */
        }

        //now set shape
        rn.typ = ATTRIB_SHAPE;
        rn.style = ATTRIB_STYLE;
        rn.relationCategory = OBRelationNode.IS_ATTRIBUTE;
        break;
      }

      case REL_OBJECT_PROPERTY:
      case REL_OBJECT_PROPERTY_RESTRICT_SOME:
      case REL_OBJECT_PROPERTY_RESTRICT_ALL:
      case REL_OBJECT_PROPERTY_RESTRICT_CARD: {
        //first reset color
        if (!rn.isvariable) {
          rn.borderColor = SIGNATURE_BORDER_COLOR;
          rn.backColor = SIGNATURE_BACK_COLOR;
          rn.textColor = SIGNATURE_TEXT_COLOR;

        }
        else {
          /*
           * rn.borderColor=VARIABLE_SIGNATURE_BORDER_COLOR;
           * rn.backColor=VARIABLE_SIGNATURE_BACK_COLOR;
           * rn.textColor=VARIABLE_SIGNATURE_TEXT_COLOR;
           */
        }

        //now set shape
        rn.typ = ATTRIB_SIGN_SHAPE;
        rn.style = ATTRIB_SIGN_STYLE;
        rn.relationCategory = OBRelationNode.IS_SIGNATURE;

        break;
      }

      case REL_DATA_PROPERTY:
      case REL_DATA_PROPERTY_RESTRICT_SOME:
      case REL_DATA_PROPERTY_RESTRICT_ALL:
      case REL_DATA_PROPERTY_RESTRICT_CARD: {
        //first reset color
        if (!rn.isvariable) {
          rn.borderColor = DATA_SIGNATURE_BORDER_COLOR;
          rn.backColor = DATA_SIGNATURE_BACK_COLOR;
          rn.textColor = DATA_SIGNATURE_TEXT_COLOR;

        }
        else {
          /*
           * rn.borderColor=VARIABLE_SIGNATURE_BORDER_COLOR;
           * rn.backColor=VARIABLE_SIGNATURE_BACK_COLOR;
           * rn.textColor=VARIABLE_SIGNATURE_TEXT_COLOR;
           */
        }

        //now set shape
        rn.typ = ATTRIB_SIGN_SHAPE;
        rn.style = ATTRIB_SIGN_STYLE;
        rn.relationCategory = OBRelationNode.IS_SIGNATURE;

        break;
      }
      case REL_ARE_EQUIVALENT:
      case REL_ARE_SAME:
        //case REL_IS_SAME_AS :
      case REL_ARE_DIFFERENT:
      case REL_ARE_DISJOINT:
      //case REL_ONE_OF :
      {

        rn.borderColor = CONSTRAINT_BORDER_COLOR;
        rn.backColor = CONSTRAINT_BACK_COLOR;
        rn.textColor = CONSTRAINT_TEXT_COLOR;
        rn.typ = CONSTRAINT_OPERATOR_SHAPE;
        rn.relationCategory = OBRelationNode.IS_CLASS_OPERATOR;
        if (rn.relationSubCategory == REL_ARE_EQUIVALENT) {
          if (EQUIVALENT_ICON != null) {
            rn.typ = OBNode.TYPE_ICON;
            rn.setImage(EQUIVALENT_ICON);
          }
          else {
            rn.setLabel(EQUIVALENT_LABEL);

          }
        }
        else if (rn.relationSubCategory == REL_ARE_DIFFERENT) {
          if (DIFFERENT_ICON != null) {
            rn.typ = OBNode.TYPE_ICON;
            rn.setImage(DIFFERENT_ICON);
          }
          else {
            rn.setLabel(DIFFERENT_LABEL);
          }
          rn.relationCategory = OBRelationNode.IS_OBJECT_OPERATOR;

        }
        else if (rn.relationSubCategory == REL_ARE_SAME) {
          if (SAME_ICON != null) {
            rn.typ = OBNode.TYPE_ICON;
            rn.setImage(SAME_ICON);
          }
          else {
            rn.setLabel(SAME_LABEL);
          }
          rn.relationCategory = OBRelationNode.IS_OBJECT_OPERATOR;

        }
        else if (rn.relationSubCategory == REL_ARE_DISJOINT) {
          if (DISJOINT_ICON != null) {
            rn.typ = OBNode.TYPE_ICON;
            rn.setImage(DISJOINT_ICON);
          }
          else {
            rn.setLabel(DISJOINT_LABEL);

          }
        }
        break;
      }
      case REL_INVERSE_OF: {
        rn.typ = Node.TYPE_RECTANGLE2;
        rn.borderColor = CONSTRAINT_BORDER_COLOR;
        rn.backColor = CONSTRAINT_BACK_COLOR;
        rn.textColor = CONSTRAINT_TEXT_COLOR;
        if (INVERSE_OF_ICON != null) {
          rn.typ = OBNode.TYPE_ICON;
          rn.setImage(INVERSE_OF_ICON);
        }
        else {
          rn.setLabel(INVERSE_OF_LABEL);

          //rn.typ = ISA_SUBCLASS_SHAPE;
          //rn.relationCategory = OBRelationNode.IS_SUBCLASS;
        }
        break;
      }
    }

  }

  /*
   * public void resetRelationCategory(OBRelationNode rn) {
   *
   * if (rn.relationCategory == OBRelationNode.IS_SUBCLASS) {
   * //rn.setLabel(SUBCLASS_LABEL); rn.typ = Node.TYPE_RECTANGLE2;
   * rn.setImage(SUBCLASS_ICON); rn.borderColor = ISA_SUBCLASS_BORDER_COLOR;
   * rn.backColor = ISA_SUBCLASS_BACK_COLOR; rn.textColor =
   * ISA_SUBCLASS_TEXT_COLOR; rn.typ = ISA_SUBCLASS_SHAPE; return; }
   *
   * if (rn.relationCategory == OBRelationNode.IS_INSTANCE) {
   * rn.setLabel(INSTANCE_LABEL); rn.borderColor = ISA_INSTANCE_BORDER_COLOR;
   * rn.backColor = ISA_INSTANCE_BACK_COLOR; rn.textColor =
   * ISA_INSTANCE_TEXT_COLOR; rn.typ = ISA_INSTANCE_SHAPE; return; }
   *
   * if (rn.relationCategory == OBRelationNode.IS_ATTRIBUTE) { //first reset
   * color if (!rn.isvariable) { rn.borderColor = ATTRIB_BORDER_COLOR;
   * rn.backColor = ATTRIB_BACK_COLOR; rn.textColor = ATTRIB_TEXT_COLOR;
   *  } else { /* rn.borderColor=VARIABLE_ATTR_BORDER_COLOR;
    * rn.backColor=VARIABLE_ATTR_BACK_COLOR;
    * rn.textColor=VARIABLE_ATTR_TEXT_COLOR; end comment }
    *
    * //now set shape if (rn.inheritable && rn.multy) { rn.typ =
    * MULTYATTRIB_INHERIT_SHAPE; rn.style = MULTYATTRIB_INHERIT_STYLE; } else
    * if (rn.inheritable && !rn.multy) { rn.typ = ATTRIB_INHERIT_SHAPE;
        * rn.style = ATTRIB_INHERIT_STYLE; } else if (rn.multy && !rn.inheritable) {
        * rn.typ = MULTYATTRIB_SHAPE; rn.style = MULTYATTRIB_STYLE; } else { rn.typ =
    * ATTRIB_SHAPE; rn.style = ATTRIB_STYLE; } return; }
    *
    * if (rn.relationCategory == OBRelationNode.IS_SIGNATURE) { //first reset
    * color if (!rn.isvariable) { rn.borderColor = SIGNATURE_BORDER_COLOR;
    * rn.backColor = SIGNATURE_BACK_COLOR; rn.textColor = SIGNATURE_TEXT_COLOR;
    *  } else { /* rn.borderColor=VARIABLE_SIGNATURE_BORDER_COLOR;
     * rn.backColor=VARIABLE_SIGNATURE_BACK_COLOR;
     * rn.textColor=VARIABLE_SIGNATURE_TEXT_COLOR; end comment }
     *
     * //now set shape if (rn.inheritable && rn.multy) { rn.typ =
     * MULTYATTRIB_INHERIT_SIGN_SHAPE; rn.style =
         * MULTYATTRIB_INHERIT_SIGN_STYLE; } else if (rn.inheritable && !rn.multy) {
         * rn.typ = ATTRIB_INHERIT_SIGN_SHAPE; rn.style = ATTRIB_INHERIT_SIGN_STYLE; }
     * else if (rn.multy && !rn.inheritable) { rn.typ = MULTYATTRIB_SIGN_SHAPE;
     * rn.style = MULTYATTRIB_SIGN_STYLE; } else { rn.typ = ATTRIB_SIGN_SHAPE;
     * rn.style = ATTRIB_SIGN_STYLE; } return; }
     *
     * rn.borderColor = CONSTRAINT_BORDER_COLOR; rn.backColor =
     * CONSTRAINT_BACK_COLOR; rn.textColor = CONSTRAINT_TEXT_COLOR; rn.typ =
     * CONSTRAINT_OPERATOR_SHAPE;
     *
     * if (rn.relationCategory == OBRelationNode.IS_INTERSECTION) {
     * rn.setLabel(INTERSECTION_LABEL); } else if (rn.relationCategory ==
     * OBRelationNode.IS_UNION) { rn.setLabel(UNION_LABEL); } else if
     * (rn.relationCategory == OBRelationNode.ARE_DISJOINT) {
     * rn.setLabel(DISJOINT_LABEL); } else if (rn.relationCategory ==
     * OBRelationNode.IS_DISJOINT_UNION) { rn.setLabel(DISJOINT_UNION_LABEL); }
     * else if (rn.relationCategory == OBRelationNode.IS_DISJOINT_WITH) {
     * rn.setLabel(DISJOINT_WITH_LABEL); } else if (rn.relationCategory ==
     * OBRelationNode.IS_SAME_AS) { rn.setLabel(SAME_AS_LABEL); } else if
     * (rn.relationCategory == OBRelationNode.IS_ONE_OF) {
     * rn.setLabel(ONE_OF_LABEL);
     *  } else if (rn.relationCategory == OBRelationNode.ARE_EQUIVALENT) {
     * rn.setLabel(EQUIVALENT_LABEL);
     *  } else if (rn.relationCategory == OBRelationNode.ARE_DIFFERENT) {
     * rn.setLabel(DIFFERENT_LABEL);
     *  } else if (rn.relationCategory == OBRelationNode.ARE_SAME) {
     * rn.setLabel(SAME_LABEL);
     *  }
     *  }
     */

    /*
     * public static final int SUBCLASS_RELATION = 13; public static final int
     * OBJECT_PROPERTY_VALUE = 1; public static final int DATA_PROPERTY_VALUE =
     * 2;
     *
     * public static final int OBJECT_PROPERTY = 3; public static final int
         * DATA_PROPERTY = 4; public static final int OBJECT_PROPERTY_RESTRICT_SOME =
     * 5; public static final int OBJECT_PROPERTY_RESTRICT_ALL = 6; public
     * static final int OBJECT_PROPERTY_RESTRICT_VALUE = 7; public static final
     * int OBJECT_PROPERTY_RESTRICT_CARD = 8;
     *
     * public static final int DATA_PROPERTY_RESTRICT_SOME = 9; public static
     * final int DATA_PROPERTY_RESTRICT_ALL = 10; public static final int
     * DATA_PROPERTY_RESTRICT_VALUE = 11; public static final int
     * DATA_PROPERTY_RESTRICT_CARD = 12;
     *
     * public static final int SUBCLASS_RELATION = 13; public static final int
     * INSTANCE_RELATION = 14;
     *
         * public static final int ARE_EQUIVALENT_RELATION = 15; public static final
         * int ARE_DIFFERENT_RELATION = 16; public static final int ONE_OF_RELATION =
     * 20;
     */
    public void resetEdgeCategory(
        OBEdge ed,
        int relationCategory,
        boolean inheritable,
        boolean multy,
        boolean isvariable) {
      if (ed.edgeOrder == OBEdge.START_EDGE) {
        switch (relationCategory) {
          //case REL_SUBCLASS :
          case REL_INVERSE_OF: {
            ed.type = ATTR_START_TYPE;
            ed.col = ISA_SUBCLASS_EDGE_COLOR;
            return;
          }
          /*case REL_INSTANCE :
           {
            ed.type = ATTR_START_TYPE;
            ed.col = ISA_INSTANCE_EDGE_COLOR;
            return;
           }*/
          case REL_OBJECT_PROPERTY_VALUE:
          case REL_OBJECT_PROPERTY_RESTRICT_VALUE: {
            ed.type = ATTR_START_TYPE;
            if (inheritable) {
              ed.edgeTile = INHATTR_TILE;
            }
            if (isvariable) {
              ed.col = VAR_ATTRIB_EDGE_COLOR;
            }
            else {
              ed.col = ATTRIB_EDGE_COLOR;
            }
            return;
          }

          case REL_DATA_PROPERTY_VALUE:
          case REL_DATA_PROPERTY_RESTRICT_VALUE:

          {
            ed.type = ATTR_START_TYPE;
            if (inheritable) {
              ed.edgeTile = INHATTR_TILE;
            }
            if (isvariable) {
              ed.col = VAR_ATTRIB_EDGE_COLOR;
            }
            else {
              ed.col = DATA_ATTRIB_EDGE_COLOR;
            }
            return;
          }
          case REL_OBJECT_PROPERTY:
          case REL_OBJECT_PROPERTY_RESTRICT_SOME:
          case REL_OBJECT_PROPERTY_RESTRICT_ALL:
          case REL_OBJECT_PROPERTY_RESTRICT_CARD:

          {
            ed.type = SIGN_START_TYPE;
            if (inheritable) {
              ed.edgeTile = INHSIGN_TILE;
            }
            if (isvariable) {
              ed.col = VAR_SIGN_EDGE_COLOR;
            }
            else {
              ed.col = SIGN_EDGE_COLOR;
            }
            return;
          }

          case REL_DATA_PROPERTY:

          case REL_DATA_PROPERTY_RESTRICT_SOME:
          case REL_DATA_PROPERTY_RESTRICT_ALL:
          case REL_DATA_PROPERTY_RESTRICT_CARD: {
            ed.type = SIGN_START_TYPE;
            if (inheritable) {
              ed.edgeTile = INHSIGN_TILE;
            }
            if (isvariable) {
              ed.col = VAR_SIGN_EDGE_COLOR;
            }
            else {
              ed.col = DATA_SIGN_EDGE_COLOR;
            }
            return;
          }
          case REL_ARE_EQUIVALENT:
          case REL_ARE_SAME:
          case REL_ARE_DIFFERENT:
          case REL_ARE_DISJOINT: {
            ed.type = CONSTRAINT_OPERATOR_START_TYPE;
            ed.col = CONSTRAINT_EDGE_COLOR;
            return;
          }

        }

      }
      else if (ed.edgeOrder == OBEdge.END_EDGE) {
        switch (relationCategory) {
          case REL_INVERSE_OF: {
            ed.type = ATTR_END_TYPE;
            ed.col = ISA_SUBCLASS_EDGE_COLOR;
            return;
          }
          case REL_OBJECT_PROPERTY_VALUE:
          case REL_OBJECT_PROPERTY_RESTRICT_VALUE: {
            if (multy) {
              ed.type = MULTYATTR_END_TYPE;
            }
            else {
              ed.type = ATTR_END_TYPE;

            }
            if (isvariable) {
              ed.col = VAR_ATTRIB_EDGE_COLOR;
            }
            else {
              ed.col = ATTRIB_EDGE_COLOR;

            }
            return;
          }

          case REL_DATA_PROPERTY_VALUE:
          case REL_DATA_PROPERTY_RESTRICT_VALUE:

          {
            if (multy) {
              ed.type = MULTYATTR_END_TYPE;
            }
            else {
              ed.type = ATTR_END_TYPE;

            }
            if (isvariable) {
              ed.col = VAR_ATTRIB_EDGE_COLOR;
            }
            else {
              ed.col = DATA_ATTRIB_EDGE_COLOR;

            }
            return;
          }
          case REL_OBJECT_PROPERTY:

          case REL_OBJECT_PROPERTY_RESTRICT_SOME:
          case REL_OBJECT_PROPERTY_RESTRICT_ALL:
          case REL_OBJECT_PROPERTY_RESTRICT_CARD: {
            if (multy) {
              ed.type = MULTYSIGN_END_TYPE;
            }
            else {
              ed.type = SIGN_END_TYPE;
            }
            if (isvariable) {
              ed.col = VAR_SIGN_EDGE_COLOR;
            }
            else {
              ed.col = SIGN_EDGE_COLOR;
            }
            return;
          }

          case REL_DATA_PROPERTY:

          case REL_DATA_PROPERTY_RESTRICT_SOME:
          case REL_DATA_PROPERTY_RESTRICT_ALL:
          case REL_DATA_PROPERTY_RESTRICT_CARD: {
            if (multy) {
              ed.type = MULTYSIGN_END_TYPE;
            }
            else {
              ed.type = SIGN_END_TYPE;
            }
            if (isvariable) {
              ed.col = VAR_SIGN_EDGE_COLOR;
            }
            else {
              ed.col = DATA_SIGN_EDGE_COLOR;
            }
            return;
          }
          case REL_ARE_EQUIVALENT:
          case REL_ARE_SAME:
          case REL_ARE_DIFFERENT:
          case REL_ARE_DISJOINT: {
            ed.type = CONSTRAINT_MULTY_OPERATOR_END_TYPE;
            ed.col = CONSTRAINT_EDGE_COLOR;
            return;
          }
        }

      }
    }

  public void resetObjectColor(OBObjectNode on) {
    /*
     * if(on.isvariable) {
     *
     * on.borderColor=VARIABLE_OBJ_BORDER_COLOR;
     * on.backColor=VARIABLE_OBJ_BACK_COLOR;
     * on.textColor=VARIABLE_OBJ_TEXT_COLOR; on.typ=OBJECT_SHAPE;
     *  } else { on.borderColor=OBJECT_BORDER_COLOR;
     * on.backColor=OBJECT_BACK_COLOR; on.textColor=OBJECT_TEXT_COLOR; }
     */
  }

  public void initObject(OBObjectNode on) {
  }

}