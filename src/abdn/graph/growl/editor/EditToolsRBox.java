/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package abdn.graph.growl.editor;

import javax.swing.JPanel;

import abdn.graph.growl.RenderGrowl;

/**
 * @author skrivov
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EditToolsRBox
    extends JPanel {
  private EditorGUI edGUI;

  private javax.swing.JButton relationButton = null;
  private javax.swing.JButton subpropertyButton = null;
  private javax.swing.JButton inverseButton = null;
  private javax.swing.JButton browseButton = null;
  private javax.swing.JButton deleteButton = null;
  private javax.swing.JButton jButton = null;
  /**
   * This is the default constructor
   */
  public EditToolsRBox(EditorGUI gui) {
    super();
    edGUI = gui;
    initialize();
  }

  /**
   * This method initializes this
   *
   * @return void
   */
  private void initialize() {
    this.setLayout(null);
    this.add(getRelationButton(), null);
    this.add(getSubpropertyButton(), null);
    this.add(getInverseButton(), null);
    this.add(getBrowseButton(), null);
    this.add(getDeleteButton(), null);
    this.add(getJButton(), null);
    this.setSize(34, 531);
  }

  public void deselectALL() {
    getRelationButton().setSelected(false);
    getSubpropertyButton().setSelected(false);
    getInverseButton().setSelected(false);
    getBrowseButton().setSelected(false);
    getDeleteButton().setSelected(false);
  }

  /**
   * This method initializes relationButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getRelationButton() {
    if (relationButton == null) {
      relationButton = new javax.swing.JButton();
      relationButton.setBounds(3, 70, 28, 28);
      relationButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolObjectProperty.png")));
      relationButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_RELATION, EditorGUI.TOOL_PROPERTY,
                       RenderGrowl.REL_OBJECT_PROPERTY);
        }
      });
    }
    return relationButton;
  }

  /**
   * This method initializes subpropertyButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getSubpropertyButton() {
    if (subpropertyButton == null) {
      subpropertyButton = new javax.swing.JButton();
      subpropertyButton.setBounds(3, 100, 28, 28);
      subpropertyButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolSubclass.png")));
      subpropertyButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_EDGE, EditorGUI.TOOL_EDGE_ISA, 0);
        }
      });
    }
    return subpropertyButton;
  }

  /**
   * This method initializes inverseButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getInverseButton() {
    if (inverseButton == null) {
      inverseButton = new javax.swing.JButton();
      inverseButton.setBounds(3, 130, 28, 28);
      inverseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(
          "/abdn/graph/growl/images/toolInverseOf.png")));
      inverseButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_RELATION, EditorGUI.TOOL_INVERSE_OF,
                       0);
        }
      });
    }
    return inverseButton;
  }

  /**
   * This method initializes browseButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getBrowseButton() {
    if (browseButton == null) {
      browseButton = new javax.swing.JButton();
      browseButton.setBounds(3, 7, 28, 28);
      browseButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolView.gif")));
      browseButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_RELATION, EditorGUI.TOOL_BROWSE, 0);
          edGUI.esdOntoBrowser.clearSelect();
          browseButton.setSelected(false);
        }
      });
    }
    return browseButton;
  }

  /**
   * This method initializes deleteButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDeleteButton() {
    if (deleteButton == null) {
      deleteButton = new javax.swing.JButton();
      deleteButton.setBounds(3, 39, 28, 28);
      deleteButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDelete.gif")));
      deleteButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_RELATION, EditorGUI.TOOL_DELETE, 0);
        }
      });
    }
    return deleteButton;
  }

  public void buttonAction(
      java.awt.event.ActionEvent e,
      int editingItem,
      int editTool,
      int createItemType) {
    deselectALL();
    ( (javax.swing.JButton) e.getSource()).setSelected(true);

    edGUI.editingItem = editingItem;
    if (editTool == -1) {
      abdn.graph.ontobrowser.ESDOntoBrowser.deleteOnLClick =
          true;
      edGUI.editToolPressed = 0;
      edGUI.createNodeType = 0;
    }
    else {
      abdn.graph.ontobrowser.ESDOntoBrowser.deleteOnLClick =
          false;
      edGUI.editToolPressed = editTool;
      edGUI.createNodeType = createItemType;
    }

  }

  /**
   * This method initializes jButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getJButton() {
    if (jButton == null) {
      jButton = new javax.swing.JButton();
      jButton.setBounds(3, 160, 28, 28);
      jButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(
          "/abdn/graph/growl/images/toolClassesEquivalent.png")));
      jButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(e, EditorGUI.EDITING_RELATION, EditorGUI.TOOL_EQVIVALENT,
                       RenderGrowl.REL_ARE_EQUIVALENT);
        }
      });
    }
    return jButton;
  }
} //  @jve:visual-info  decl-index=0 visual-constraint="10,10"
