/*
 * Created on Jun 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package abdn.graph.growl.editor;

import javax.swing.JPanel;

import abdn.graph.growl.RenderGrowl;

/**
 * @author skrivov
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EditToolsPanel
    extends JPanel {

  private javax.swing.JLabel jLabel = null;
  private javax.swing.JButton cClassButton = null;
  private javax.swing.JButton cInstButton = null;
  private javax.swing.JButton cDatatypeButton = null;
  private javax.swing.JButton cDValueButton = null;
  private javax.swing.JLabel jLabel1 = null;
  private javax.swing.JButton cIsaEdgeButton = null;
  private javax.swing.JButton cEdgeEqvButton = null;
  private EditorGUI edGUI;
  private javax.swing.JLabel jLabel2 = null;
  private javax.swing.JButton intersectButton = null;
  private javax.swing.JButton unionButton = null;
  private javax.swing.JButton complButton = null;
  private javax.swing.JLabel jLabel3 = null;
  private javax.swing.JButton objPropertyButton = null;
  private javax.swing.JButton dataPropertyButton = null;
  private javax.swing.JButton objPropValueButton = null;
  private javax.swing.JButton dataPropValueButton = null;
  private javax.swing.JLabel jLabel4 = null;
  private javax.swing.JButton objPrSomeButton = null;
  private javax.swing.JButton objAllButton = null;
  private javax.swing.JButton objValueButton = null;
  private javax.swing.JButton objCardButton = null;
  private javax.swing.JButton dataSomeButton = null;
  private javax.swing.JButton dataAllButton = null;
  private javax.swing.JButton dataValueButton = null;
  private javax.swing.JButton dataCardButton = null;
  private javax.swing.JButton eqvButton = null;
  private javax.swing.JLabel jLabel5 = null;
  private javax.swing.JButton sameButton = null;
  private javax.swing.JButton disjointButton = null;
  private javax.swing.JButton differentButton = null;
  private javax.swing.JButton browseButton = null;
  private javax.swing.JButton deleteButton = null;
  private javax.swing.JButton oneOfButton = null;
  /**
   * This is the default constructor
   */
  public EditToolsPanel() {
    super();
    initialize();
  }

  public EditToolsPanel(EditorGUI edgui) {
    super();
    initialize();
    edGUI = edgui;
  }

  public void deselectALL() {
    getCClassButton().setSelected(false);
    getCInstButton().setSelected(false);
    getCDatatypeButton().setSelected(false);
    getCDValueButton().setSelected(false);
    getCIsaEdgeButton().setSelected(false);
    getCEdgeEqvButton().setSelected(false);
    getIntersectButton().setSelected(false);
    getUnionButton().setSelected(false);
    getComplButton().setSelected(false);
    getObjPropertyButton().setSelected(false);
    getDataPropertyButton().setSelected(false);
    getObjPropValueButton().setSelected(false);
    getDataPropValueButton().setSelected(false);
    getObjPrSomeButton().setSelected(false);
    getObjAllButton().setSelected(false);
    getObjValueButton().setSelected(false);
    getObjCardButton().setSelected(false);
    getDataSomeButton().setSelected(false);
    getDataAllButton().setSelected(false);
    getDataValueButton().setSelected(false);
    getDataCardButton().setSelected(false);
    getEqvButton().setSelected(false);
    getSameButton().setSelected(false);
    getDisjointButton().setSelected(false);
    getDifferentButton().setSelected(false);
    getDeleteButton().setSelected(false);
    getBrowseButton().setSelected(false);
    getOneOfButton().setSelected(false);
  }

  /**
   * This method initializes this
   *
   * @return void
   */
  private void initialize() {
    this.setLayout(null);
    this.add(getCClassButton(), null);
    this.add(getJLabel(), null);
    this.add(getCInstButton(), null);
    this.add(getCDatatypeButton(), null);
    this.add(getCDValueButton(), null);
    this.add(getJLabel1(), null);
    this.add(getCIsaEdgeButton(), null);
    this.add(getCEdgeEqvButton(), null);
    this.add(getJLabel2(), null);
    this.add(getIntersectButton(), null);
    this.add(getUnionButton(), null);
    this.add(getJLabel3(), null);
    this.add(getComplButton(), null);
    this.add(getObjPropertyButton(), null);
    this.add(getDataPropertyButton(), null);
    this.add(getObjPropValueButton(), null);
    this.add(getDataPropValueButton(), null);
    this.add(getJLabel4(), null);
    this.add(getObjPrSomeButton(), null);
    this.add(getObjAllButton(), null);
    this.add(getObjValueButton(), null);
    this.add(getObjCardButton(), null);
    this.add(getDataSomeButton(), null);
    this.add(getDataAllButton(), null);
    this.add(getDataValueButton(), null);
    this.add(getDataCardButton(), null);
    this.add(getEqvButton(), null);
    this.add(getJLabel5(), null);
    this.add(getSameButton(), null);
    this.add(getDisjointButton(), null);
    this.add(getDifferentButton(), null);
    this.add(getBrowseButton(), null);
    this.add(getDeleteButton(), null);
    this.add(getOneOfButton(), null);
    this.setSize(67, 531);
  }

  /**
   * This method initializes jLabel
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel() {
    if (jLabel == null) {
      jLabel = new javax.swing.JLabel();
      jLabel.setBounds(5, 36, 55, 15);
      jLabel.setText("Create Object");
      jLabel.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel;
  }

  /**
   * This method initializes cClassButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCClassButton() {
    if (cClassButton == null) {
      cClassButton = new javax.swing.JButton();
      cClassButton.setBounds(6, 52, 28, 28);
      cClassButton.setText("");
      cClassButton.setToolTipText("Create Class");
      cClassButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolClass.gif")));
      cClassButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_CLASS,
              RenderGrowl.OBJ_CLASS);
        }
      });
    }
    return cClassButton;
  }

  /**
   * This method initializes cInstButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCInstButton() {
    if (cInstButton == null) {
      cInstButton = new javax.swing.JButton();
      cInstButton.setBounds(6, 81, 28, 28);
      cInstButton.setToolTipText("Create Individual");
      cInstButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolInstance.png")));
      cInstButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_INDIVIDUAL,
              RenderGrowl.OBJ_INDIVIDUAL);
        }
      });
    }
    return cInstButton;
  }

  /**
   * This method initializes cDatatypeButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCDatatypeButton() {
    if (cDatatypeButton == null) {
      cDatatypeButton = new javax.swing.JButton();
      cDatatypeButton.setBounds(33, 52, 28, 28);
      cDatatypeButton.setText("");
      cDatatypeButton.setToolTipText("Create Data Type");
      cDatatypeButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDatatype.png")));
      cDatatypeButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_DATATYPE,
              RenderGrowl.OBJ_DATATYPE);
        }
      });
    }
    return cDatatypeButton;
  }

  /**
   * This method initializes cDValueButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCDValueButton() {
    if (cDValueButton == null) {
      cDValueButton = new javax.swing.JButton();
      cDValueButton.setBounds(33, 81, 28, 28);
      cDValueButton.setToolTipText("Create Data Value");
      cDValueButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDValue.png")));
      cDValueButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_DATA_VALUE,
              RenderGrowl.OBJ_DATAVALUE);

        }
      });
    }
    return cDValueButton;
  }

  /**
   * This method initializes jLabel1
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel1() {
    if (jLabel1 == null) {
      jLabel1 = new javax.swing.JLabel();
      jLabel1.setBounds(5, 109, 59, 15);
      jLabel1.setText(" IS-A Relations");
      jLabel1.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel1;
  }

  /**
   * This method initializes cSubClassButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCIsaEdgeButton() {
    if (cIsaEdgeButton == null) {
      cIsaEdgeButton = new javax.swing.JButton();
      cIsaEdgeButton.setBounds(6, 126, 28, 28);

      cIsaEdgeButton.setToolTipText("Create Subclass Relation");
      cIsaEdgeButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolSubclass1.png")));
      cIsaEdgeButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_EDGE,
              EditorGUI.TOOL_EDGE_ISA,
              0);

        }
      });
    }
    return cIsaEdgeButton;
  }

  /**
   * This method initializes cInstanceOfButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getCEdgeEqvButton() {
    if (cEdgeEqvButton == null) {
      cEdgeEqvButton = new javax.swing.JButton();
      cEdgeEqvButton.setBounds(33, 126, 28, 28);
      cEdgeEqvButton.setToolTipText("Create Instance of Relation");
      cEdgeEqvButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolEqArrow.png")));
      cEdgeEqvButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_EDGE,
              EditorGUI.TOOL_EDGE_EQVIVALENT,
              0);

        }
      });
    }
    return cEdgeEqvButton;
  }

  /**
   * This method initializes jLabel2
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel2() {
    if (jLabel2 == null) {
      jLabel2 = new javax.swing.JLabel();
      jLabel2.setBounds(7, 357, 55, 15);
      jLabel2.setText("Set Operators");
      jLabel2.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel2;
  }

  /**
   * This method initializes intersectButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getIntersectButton() {
    if (intersectButton == null) {
      intersectButton = new javax.swing.JButton();
      intersectButton.setBounds(33, 373, 28, 28);
      intersectButton.setText("");
      intersectButton.setToolTipText(
          "Create Set theoretic  intersection of Classes");
      intersectButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolIntersection.gif")));
      intersectButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_AND,
              RenderGrowl.OBJ_INTERSECTION);
        }
      });
    }
    return intersectButton;
  }

  /**
   * This method initializes unionButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getUnionButton() {
    if (unionButton == null) {
      unionButton = new javax.swing.JButton();
      unionButton.setBounds(6, 373, 28, 28);
      unionButton.setText("");
      unionButton.setToolTipText(
          "Create Set theoretic  Union of Classes");
      unionButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolUnion.gif")));
      unionButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_OR,
              RenderGrowl.OBJ_UNION);
        }
      });
    }
    return unionButton;
  }

  /**
   * This method initializes complButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getComplButton() {
    if (complButton == null) {
      complButton = new javax.swing.JButton();
      complButton.setBounds(6, 400, 28, 28);
      complButton.setToolTipText(
          "Create Set theoretic  Compliment to a Class");
      complButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolComplement.gif")));
      complButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_NOT,
              RenderGrowl.OBJ_COMPLEMENT);
        }
      });
    }
    return complButton;
  }

  /**
   * This method initializes jLabel3
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel3() {
    if (jLabel3 == null) {
      jLabel3 = new javax.swing.JLabel();
      jLabel3.setBounds(8, 156, 49, 15);
      jLabel3.setText("Properties");
      jLabel3.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel3;
  }

  /**
   * This method initializes objPropertyButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjPropertyButton() {
    if (objPropertyButton == null) {
      objPropertyButton = new javax.swing.JButton();
      objPropertyButton.setBounds(6, 172, 28, 28);
      objPropertyButton.setToolTipText("Create Object Property");
      objPropertyButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolObjectProperty.png")));
      objPropertyButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_PROPERTY,
              RenderGrowl.REL_OBJECT_PROPERTY);
        }
      });
    }
    return objPropertyButton;
  }

  /**
   * This method initializes dataPropertyButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataPropertyButton() {
    if (dataPropertyButton == null) {
      dataPropertyButton = new javax.swing.JButton();
      dataPropertyButton.setBounds(33, 172, 28, 28);
      dataPropertyButton.setToolTipText("Create Data Property");
      dataPropertyButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDataProperty.png")));
      dataPropertyButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_PROPERTY,
              RenderGrowl.REL_DATA_PROPERTY);
        }
      });
    }
    return dataPropertyButton;
  }

  /**
   * This method initializes objPropValueButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjPropValueButton() {
    if (objPropValueButton == null) {
      objPropValueButton = new javax.swing.JButton();
      objPropValueButton.setBounds(6, 201, 28, 28);
      objPropValueButton.setToolTipText("Create Object Property value");
      objPropValueButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolPropertyValue.png")));
      objPropValueButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_PROPERTY_VALUE,
              RenderGrowl.REL_OBJECT_PROPERTY_VALUE);
        }
      });
    }
    return objPropValueButton;
  }

  /**
   * This method initializes dataPropValueButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataPropValueButton() {
    if (dataPropValueButton == null) {
      dataPropValueButton = new javax.swing.JButton();
      dataPropValueButton.setBounds(33, 201, 28, 28);

      dataPropValueButton.setToolTipText("Create Data property Value");
      dataPropValueButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDataPropertyValue.png")));
      dataPropValueButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_PROPERTY_VALUE,
              RenderGrowl.REL_DATA_PROPERTY_VALUE);
        }
      });
    }
    return dataPropValueButton;
  }

  public void buttonAction(
      java.awt.event.ActionEvent e,
      int editingItem,
      int editTool,
      int createItemType) {
    deselectALL();
    ( (javax.swing.JButton) e.getSource()).setSelected(true);

    edGUI.editingItem = editingItem;
    if (editTool == -1) {
      abdn.graph.ontobrowser.ESDOntoBrowser.deleteOnLClick =
          true;
      edGUI.editToolPressed = 0;
      edGUI.createNodeType = 0;
    }
    else {
      abdn.graph.ontobrowser.ESDOntoBrowser.deleteOnLClick =
          false;
      edGUI.editToolPressed = editTool;
      edGUI.createNodeType = createItemType;
    }

  }

  /**
   * This method initializes jLabel4
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel4() {
    if (jLabel4 == null) {
      jLabel4 = new javax.swing.JLabel();
      jLabel4.setBounds(5, 229, 56, 15);
      jLabel4.setText("Restrictions");
      jLabel4.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel4;
  }

  /**
   * This method initializes objPrSomeButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjPrSomeButton() {
    if (objPrSomeButton == null) {
      objPrSomeButton = new javax.swing.JButton();
      objPrSomeButton.setBounds(6, 273, 28, 28);
      objPrSomeButton.setToolTipText(
          "Create Object  Property Restriction: Some");
      objPrSomeButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionSome.png")));
      objPrSomeButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_RESTRICTION_SOME,
              RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME);
        }
      });
    }
    return objPrSomeButton;
  }

  /**
   * This method initializes objAllButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjAllButton() {
    if (objAllButton == null) {
      objAllButton = new javax.swing.JButton();
      objAllButton.setBounds(6, 245, 28, 28);
      objAllButton.setText("");
      objAllButton.setToolTipText(
          "Create Object  Property Restriction: All");
      objAllButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionAll.png")));
      objAllButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_RESTRICTION_ALL,
              RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL);
        }
      });
    }
    return objAllButton;
  }

  /**
   * This method initializes objValueButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjValueButton() {
    if (objValueButton == null) {
      objValueButton = new javax.swing.JButton();
      objValueButton.setBounds(6, 329, 28, 28);
      objValueButton.setToolTipText(
          "Create Object  Property Restriction: Value");
      objValueButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionVal.png")));
      objValueButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_RESTRICTION_VALUE,
              RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE);
        }
      });
    }
    return objValueButton;
  }

  /**
   * This method initializes objCardButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getObjCardButton() {
    if (objCardButton == null) {
      objCardButton = new javax.swing.JButton();
      objCardButton.setBounds(6, 301, 28, 28);
      objCardButton.setToolTipText(
          "Create Object  Property Restriction: Cardinality");
      objCardButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionCard.png")));
      objCardButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_RESTRICTION_NUMBER,
              RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD);
        }
      });
    }
    return objCardButton;
  }

  /**
   * This method initializes dataSomeButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataSomeButton() {
    if (dataSomeButton == null) {
      dataSomeButton = new javax.swing.JButton();
      dataSomeButton.setBounds(33, 273, 28, 28);
      dataSomeButton.setToolTipText(
          "Create Data  Property Restriction: Some");
      dataSomeButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionSomeData.png")));
      dataSomeButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_RESTRICTION_SOME,
              RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME);
        }
      });
    }
    return dataSomeButton;
  }

  /**
   * This method initializes dataAllButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataAllButton() {
    if (dataAllButton == null) {
      dataAllButton = new javax.swing.JButton();
      dataAllButton.setBounds(33, 245, 28, 28);
      dataAllButton.setToolTipText(
          "Create Data  Property Restriction:All");
      dataAllButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionAllData.png")));
      dataAllButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_RESTRICTION_ALL,
              RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL);
        }
      });
    }
    return dataAllButton;
  }

  /**
   * This method initializes dataValueButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataValueButton() {
    if (dataValueButton == null) {
      dataValueButton = new javax.swing.JButton();
      dataValueButton.setBounds(33, 329, 28, 28);
      dataValueButton.setToolTipText(
          "Create Data  Property Restriction: Value");
      dataValueButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionValData.png")));
      dataValueButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_RESTRICTION_VALUE,
              RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE);
        }
      });
    }
    return dataValueButton;
  }

  /**
   * This method initializes dataCardButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDataCardButton() {
    if (dataCardButton == null) {
      dataCardButton = new javax.swing.JButton();
      dataCardButton.setBounds(33, 301, 28, 28);
      dataCardButton.setToolTipText(
          "Create Data  Property Restriction: Cardinality");
      dataCardButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolRestrictionCardData.png")));
      dataCardButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DATA_RESTRICTION_NUMBER,
              RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD);
        }
      });
    }
    return dataCardButton;
  }

  /**
   * This method initializes eqvButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getEqvButton() {
    if (eqvButton == null) {
      eqvButton = new javax.swing.JButton();
      eqvButton.setBounds(6, 447, 28, 28);
      eqvButton.setToolTipText("Create Relation: Classes are Equivalent");
      eqvButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolClassesEquivalent.png")));
      eqvButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_EQVIVALENT,
              RenderGrowl.REL_ARE_EQUIVALENT);
        }
      });
    }
    return eqvButton;
  }

  /**
   * This method initializes jLabel5
   *
   * @return javax.swing.JLabel
   */
  private javax.swing.JLabel getJLabel5() {
    if (jLabel5 == null) {
      jLabel5 = new javax.swing.JLabel();
      jLabel5.setBounds(5, 429, 58, 17);
      jLabel5.setText("Operators");
      jLabel5.setFont(
          new java.awt.Font("Arial Narrow", java.awt.Font.PLAIN, 10));
    }
    return jLabel5;
  }

  /**
   * This method initializes sameButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getSameButton() {
    if (sameButton == null) {
      sameButton = new javax.swing.JButton();
      sameButton.setBounds(33, 447, 28, 28);
      sameButton.setToolTipText("Create Relation: Individuals are Same");
      sameButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolIndividualsSame.png")));
      sameButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_INDIVIDUALS_EQUAL,
              RenderGrowl.REL_ARE_SAME);
        }
      });
    }
    return sameButton;
  }

  /**
   * This method initializes disjointButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDisjointButton() {
    if (disjointButton == null) {
      disjointButton = new javax.swing.JButton();
      disjointButton.setBounds(6, 474, 28, 28);
      disjointButton.setToolTipText(
          "Create Relation: Classes are Disjoint");
      disjointButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolClassesDisjoint.png")));
      disjointButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DISJOINT,
              RenderGrowl.REL_ARE_DISJOINT);
        }
      });
    }
    return disjointButton;
  }

  /**
   * This method initializes differentButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDifferentButton() {
    if (differentButton == null) {
      differentButton = new javax.swing.JButton();
      differentButton.setBounds(33, 474, 28, 28);
      differentButton.setToolTipText(
          "Create Relation: Individuals are Different");
      differentButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolIndividualsDifferent.png")));
      differentButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_INDIVIDUALS_DIFFERENT,
              RenderGrowl.REL_ARE_DIFFERENT);
        }
      });
    }
    return differentButton;
  }

  /**
   * This method initializes browseButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getBrowseButton() {
    if (browseButton == null) {
      browseButton = new javax.swing.JButton();
      browseButton.setBounds(6, 6, 28, 28);
      browseButton.setText("");
      browseButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolView.gif")));
      browseButton.setToolTipText("Deselect All, Browse");
      browseButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_BROWSE,
              0);
          edGUI.esdOntoBrowser.clearSelect();
          browseButton.setSelected(false);

        }
      });
    }
    return browseButton;
  }

  /**
   * This method initializes deleteButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getDeleteButton() {
    if (deleteButton == null) {
      deleteButton = new javax.swing.JButton();
      deleteButton.setBounds(33, 6, 28, 28);
      deleteButton.setText("");
      deleteButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolDelete.gif")));
      deleteButton.setToolTipText("Delete Item");
      deleteButton
          .addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_RELATION,
              EditorGUI.TOOL_DELETE,
              0);
        }
      });
    }
    return deleteButton;
  }

  /**
   * This method initializes oneOfButton
   *
   * @return javax.swing.JButton
   */
  private javax.swing.JButton getOneOfButton() {
    if (oneOfButton == null) {
      oneOfButton = new javax.swing.JButton();
      oneOfButton.setBounds(33, 400, 28, 28);
      oneOfButton.setIcon(
          new javax.swing.ImageIcon(
          getClass().getResource(
          "/abdn/graph/growl/images/toolOneOf.gif")));
      oneOfButton.setToolTipText("Create relation:One Of");
      oneOfButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          buttonAction(
              e,
              EditorGUI.EDITING_OBJECT,
              EditorGUI.TOOL_ONE_OF,
              RenderGrowl.OBJ_ONE_OF);
        }
      });
    }
    return oneOfButton;
  }
} //  @jve:visual-info  decl-index=0 visual-constraint="10,9"
