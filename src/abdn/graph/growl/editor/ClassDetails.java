/*
 * ClassDetails.java
 * Created on Oct 6, 2004
 *
 */
package abdn.graph.growl.editor;

import java.util.Iterator;
import java.util.Vector;

import abdn.graph.growl.GraphUtil;
import abdn.graph.ontobrowser.OBEdge;
import abdn.graph.ontobrowser.OBNode;
import abdn.graph.ontobrowser.OBObjectNode;

/**
 * ClassDetails.java
 * @author Rich
 *
 */
public class ClassDetails
{
  public class ClassDetail
  {
    OBNode detailItem;
    OBNode from;

    ClassDetail(OBNode item, OBNode f)
    {
      detailItem = item;
      from = f;
    }

    String displayString(OBNode classNode)
    {
      StringBuffer s = new StringBuffer();
      s.append(detailItem.getLabel());
      if( from != classNode )
      {
        s.append( " (from ");
        s.append(from.getLabel());
        s.append(")");
      }
      return s.toString();
    }
  }

  OBObjectNode classNode;
  Vector details;

  ClassDetails(OBObjectNode node)
  {
    details = getAllClassDetails(classNode = node);
  }

  Vector getDetails()
  {
    return details;
  }

  OBNode getDetailNode(int i)
  {
    ClassDetail d = (ClassDetail)details.get(i);
    return d.detailItem;
  }

  String displayString(int i)
  {
    ClassDetail d = (ClassDetail)details.get(i);
    return d.displayString(classNode);
  }

  private void getClassDetailsHelper(OBObjectNode node, Vector results)
  {
    for (Iterator i = node.edges.iterator(); i.hasNext();)
    {
      OBEdge e = (OBEdge)i.next();
      if( e.from == node )
      {
        if( !(e.edgeOrder == OBEdge.SUBCLASS_OF_EDGE && GraphUtil.isClass((OBNode)e.to)) && !results.contains(e.to) )
        {
          results.add(new ClassDetail((OBNode)e.to, node));
        }
      }
    }
  }

  private Vector getAllClassDetails(OBObjectNode node)
  {
    Vector results = new Vector();
    if( node != null )
    {
      Vector superClasses = EditorGUI.getAllSuperClasses(node);
      getClassDetailsHelper(node, results);
      for( Iterator i = superClasses.iterator(); i.hasNext();)
      {
        node = (OBObjectNode)i.next();
        getClassDetailsHelper(node, results);
      }
    }
    return results;
  }

}
