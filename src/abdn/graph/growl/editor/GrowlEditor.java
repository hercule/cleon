/*
 * GFLEditor.java
 *
 * Created on December 5, 2003, 12:05 PM
 */

package abdn.graph.growl.editor;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import abdn.graph.growl.KRPolicyOWL;
import abdn.graph.growl.NotificationPolicyOWL;
import abdn.graph.growl.RenderGrowl;
import abdn.graph.ontobrowser.ESDOntoBrowser;
import abdn.graph.ontobrowser.RTPolicy;

/**
 *
 * @author  skrivov
 */
public class GrowlEditor
    extends JFrame
    implements RTPolicy {

  static String appName = "Growl v 0.02";

  //new File(".", "test.txt").getCanonicalPath()
  /*
   *File dir1 = new File (".");
       File dir2 = new File ("..");
       try {
         System.out.println ("Current dir : " + dir1.getCanonicalPath());
         System.out.println ("Parent  dir : " + dir2.getCanonicalPath());
         }
   */

  /*
   *URL url=myProgram.class.getResource("images/New16.gif");
    Dummy = new JButton(new ImageIcon(URL));
   *
   *URL url = null;
    Class ThisClass = this.getClass();
    url=ThisClass.getResource("images/New16.gif");
   */

  /** Creates a new instance of GFLEditor */
  public GrowlEditor() {
    super();
    setOntoTitle(null);
  }

  public void initialize() {

  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (ClassNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    catch (InstantiationException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    catch (IllegalAccessException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    catch (UnsupportedLookAndFeelException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    RenderGrowl renderPolicy =
        new RenderGrowl();
    KRPolicyOWL krPolicy = new KRPolicyOWL();
    GrowlEditor obFrame = new GrowlEditor();
    EditorGUI obPanel = new EditorGUI();
    ESDOntoBrowser esdOntoBrowser =
        new ESDOntoBrowser(
        obFrame,
        obPanel,
        krPolicy,
        renderPolicy,
        new NotificationPolicyOWL(krPolicy));
    renderPolicy.LoadImages();
    //esdOntoBrowsernew.modeSelectPanel.setVisible(true);
    //esdOntoBrowsernew.xmlFileName = null;

    try {
      esdOntoBrowser.documentBase = new File(".").toURL();
      //System.getProperty("user.dir")
    }
    catch (Exception e) {
    }

    obFrame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    obFrame.getContentPane().setLayout(new java.awt.BorderLayout());
    obFrame.getContentPane().add(obPanel, java.awt.BorderLayout.CENTER);

    //obFrame.setJMenuBar(obPanel.getMainMenuBar());
    // obFrame.getContentPane().add(obPanel.getJToolBar(), java.awt.BorderLayout.NORTH);
    //obFrame.setSize(880, 600);
    obFrame.setVisible(true);
  }

  public void setOntoTitle(String name) {
    if (name == null) {
      name = "untitled";
    }
    setTitle(appName + " [" + name + "]");
  }

}
