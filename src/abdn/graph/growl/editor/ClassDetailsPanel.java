/*
 * ClassDetailsPanel.java
 * Created on Oct 5, 2004
 *
 */
package abdn.graph.growl.editor;

import java.awt.Component;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import abdn.graph.ontobrowser.ESDOntoBrowser;
import abdn.graph.ontobrowser.OBNode;
import abdn.graph.ontobrowser.OBObjectNode;

/**
 * ClassDetailsPanel.java
 * @author Rich
 *
 */
public class ClassDetailsPanel extends JScrollPane
{
  ESDOntoBrowser ob;
  JList detailsList;
  ClassDetails details;

  ClassDetailsPanel(ESDOntoBrowser o)
  {
    this.ob = o;
    setViewportView(detailsList = new JList(new DefaultListModel()));
    detailsList.setCellRenderer(new DetailsRenderer());

    detailsList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        Integer i = (Integer)detailsList.getSelectedValue();
        if( i != null )
        {
          OBNode node = details.getDetailNode(i.intValue());
          if( node != null && !node.isHiden && node != ob.getTGPanel().getSelect() )
          {
            ob.setSelect(node);
            ob.getTGPanel().fireResetEvent();
            ob.hvScroll.slowScrollToCenter(node);
          }
        }
      }
    } );
  }

  void setClassNode(OBObjectNode node)
  {
    DefaultListModel model = (DefaultListModel)detailsList.getModel();
    details = new ClassDetails(node);
    model.removeAllElements();
    int size = details.getDetails().size();
    for(int i = 0; i < size; i++)
    {
      model.addElement(new Integer(i));
    }
  }

  class DetailsRenderer extends JLabel implements ListCellRenderer
  {
    DetailsRenderer()
    {
      setOpaque(true);
      setHorizontalAlignment(LEFT);
      setVerticalAlignment(CENTER);
    }

    /* (non-Javadoc)
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
      //Get the selected index. (The index param isn't
      //always valid, so just use the value.)
      String str;
      if( value != null )
      {
        str = details.displayString(((Integer)value).intValue());
        if (isSelected)
        {
          setBackground(list.getSelectionBackground());
          setForeground(list.getSelectionForeground());
        }
        else
        {
          setBackground(list.getBackground());
          setForeground(list.getForeground());
        }
      }
      else
      {
        str = null;
      }
      setText(str);
      return this;
    }
  }

}
