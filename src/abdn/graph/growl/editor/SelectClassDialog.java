/*
 * SelectClassDialog.java
 * Created on Sep 15, 2004
 *
 */
package abdn.graph.growl.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.tree.TreePath;


/**
 * SelectClassDialog.java
 * @author Rich
 *
 */
public class SelectClassDialog extends JDialog
{
  JLabel infoLabel = new JLabel();   // label at top of window describing what classes are being selected for
  ClassTree classTree;
  JButton okButton = new JButton("Ok");
  JButton cancelButton = new JButton("Cancel");
  Set classes = null;

  SelectClassDialog(Set classes, JFrame f, int x, int y, String info)
  {
    super(f, "Select Classes", true);
    classTree = new ClassTree(classes, true);
    infoLabel.setText(info);
    layoutControls();
    setupListeners();
    setSize(200, 250);
    setLocation(x, y);
  }

  private void layoutControls()
  {
    JScrollPane scroll = new JScrollPane(classTree);
//    scroll.setPreferredSize(new Dimension(200,200));
    Box controlBox = Box.createHorizontalBox();
    controlBox.add(Box.createHorizontalStrut(8));
    controlBox.add(okButton);
    controlBox.add(Box.createHorizontalStrut(8));
    controlBox.add(cancelButton);
    controlBox.add(Box.createHorizontalGlue());
    Box infoBox = Box.createHorizontalBox();
    infoBox.add(Box.createHorizontalStrut(8));
    infoBox.add(infoLabel);
    infoBox.add(Box.createHorizontalGlue());
    Box b = Box.createVerticalBox();
    b.add(Box.createVerticalStrut(3));
    b.add(infoBox);
    b.add(Box.createVerticalStrut(3));
    b.add(scroll);
    b.add(Box.createVerticalStrut(3));
    b.add(controlBox);
    b.add(Box.createVerticalStrut(3));
    getContentPane().add(b);
  }

  private void setupListeners()
  {
    cancelButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setVisible(false);
        }
      }
    );

    okButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          TreePath[] sel = classTree.getSelectionPaths();
          int size;
          if( (size = sel.length) > 0 )
          {
            classes = new HashSet();
            for(int i = 0; i < size; i++ )
            {
              Object node = classTree.getNodeOfPath(sel[i]);
              if( node != null )
              {
                classes.add(node);
              }
            }
          }
          setVisible(false);
        }
      }
    );
  }

  public Set getSelectedClasses()
  {
    return classes;
  }
}
