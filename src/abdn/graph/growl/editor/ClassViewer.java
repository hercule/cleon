/*
 * ClassViewer.java
 * Created on Oct 6, 2004
 *
 */
package abdn.graph.growl.editor;

import java.util.Set;

import javax.swing.JSplitPane;

import abdn.graph.ontobrowser.ESDOntoBrowser;
import abdn.graph.ontobrowser.OBObjectNode;
//import abdn.taxon.property.ui.EntityPropertyPanel;

/**
 * ClassViewer.java
 * @author Rich
 *
 */
public class ClassViewer extends JSplitPane
{
  ESDOntoBrowser ob;
  SelectClassPanel classPanel;
  ClassDetailsPanel detailsPanel;
  //EntityPropertyPanel propPanel;

  /*public ClassViewer(EditorGUI ed, EntityPropertyPanel p)
  {
    super(JSplitPane.VERTICAL_SPLIT);
    this.ob = ed.esdOntoBrowser;
    this.propPanel = p;
    classPanel = new SelectClassPanel( ed, p );
    //classPanel.setPropertyPanel(p);
    //detailsPanel = new ClassDetailsPanel(ob);
    //classPanel = new SelectClassPanel(ed, detailsPanel);
    //classPanel = new SelectClassPanel( ed );
    setTopComponent(classPanel);
    //setBottomComponent(detailsPanel);
    setResizeWeight(0.65);
  }*/

  public ClassViewer(EditorGUI ed)
 {
   super(JSplitPane.VERTICAL_SPLIT);
   this.ob = ed.esdOntoBrowser;
   detailsPanel = new ClassDetailsPanel(ob);
   classPanel = new SelectClassPanel(ed, detailsPanel);
   //classPanel = new SelectClassPanel( ed );
   setTopComponent(classPanel);
   setBottomComponent(detailsPanel);
   setResizeWeight(0.65);
 }

  /*public void setPropertyPanel( EntityPropertyPanel p ) {
    //this.propPanel = p;
    classPanel.setPropertyPanel(p);
  }*/

  public void setClasses(Set classes)
  {
    classPanel.setClasses(classes);
  }

  public void positionTo(OBObjectNode node) {
    classPanel.positionTo(node);
  }
}
