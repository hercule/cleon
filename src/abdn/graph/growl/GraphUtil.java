/*
 * GraphUtil.java
 * Created on Oct 19, abdn.graph
 *
 * Various static utility functions
 *
 */
package abdn.graph.growl;

import java.util.Iterator;
import java.util.Vector;

import abdn.graph.ontobrowser.OBEdge;
import abdn.graph.ontobrowser.OBNode;
import abdn.graph.ontobrowser.OBObjectNode;

import com.touchgraph.graphlayout.Node;

/**
 * GraphUtil.java
 * @author Rich
 *
 */
public class GraphUtil
{
  static OBObjectNode thingNode = null;

  public static boolean isClass(OBNode obj) {
    if ((obj instanceof OBObjectNode)
        && ((OBObjectNode) obj).objectCategory == RenderGrowl.OBJ_CLASS)
      return true;
    else
      return false;
  }

  static public Vector getSuperClasses(OBObjectNode node)
  {
    Vector parents = new Vector();
    for (Iterator i = node.edges.iterator(); i.hasNext();)
    {
      OBEdge e = (OBEdge)i.next();
      if( e.from == node && e.edgeOrder == OBEdge.SUBCLASS_OF_EDGE && isClass((OBNode)e.to) )
      {
        parents.add(e.to);
      }
    }
    return parents;
  }

  public static void setThingNode(OBObjectNode n)
  {
    thingNode = n;
    hideThingNode(true);
  }

  public static OBObjectNode getThingNode()
  {
    return thingNode;
  }


  public static void hideThingNode(boolean hidden)
  {
    if( thingNode != null )
    {
      thingNode.isHiden = hidden;
      thingNode.markedForRemoval = hidden;
    }
  }
}
