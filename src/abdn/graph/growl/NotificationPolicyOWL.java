/*
 * NotificationPolicyOWL.java
 * Created on Jan 13, 2004
 * @author Rich Williams
 *
 */
package abdn.graph.growl;

import java.util.*;
import java.net.*;
import com.touchgraph.graphlayout.*;
import com.touchgraph.graphlayout.graphelements.*;
import abdn.graph.ontobrowser.*;
import org.semanticweb.owl.model.*;
import org.semanticweb.owl.model.change.*;

/**
 * NotificationPolicyOWL.java
 *
 */
public class NotificationPolicyOWL implements NotificationPolicy
{
    ESDOntoBrowser ob;
    GraphEltSet eltSet;
    KRPolicyOWL kr;
    OWLOntology onto;
    OWLDataFactory factory;
    Hashtable objectHash;       // key=ObjectNode ID, element=OWLObject
    Hashtable relationHash;     // key=RelationNode ID, element=collection of OWLObject
    Hashtable reverseHash;      // key=OWLProperty, element=collection of RelationNode

    public NotificationPolicyOWL(KRPolicyOWL k)
    {
      kr = k;
      resetOntology(false);
    }

    public void resetOntology(boolean empty)
    {
      if( empty )
      {
        kr.getMetadata().setDefaults();
      }
      objectHash = new Hashtable();
      relationHash = new Hashtable();
      onto = kr.getOntology();
      if( onto != null )
      {
        try
        {
            factory = onto.getOWLDataFactory();
        }
        catch (OWLException e)
        {
            e.printStackTrace();
        }
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setOBPointer(abdn.graph.ontobrowser.ESDOntoBrowser)
     */
    public void setOBPointer(ESDOntoBrowser ob)
    {
        this.ob = ob;
        eltSet = ob.getTGPanel().getCompleteEltSet();
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#createNewRelation(java.lang.String, int)
     */
    public void createNewRelation(String relID, int RelationType)
    {
      try
      {
        OWLObject owlObj = null;
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            owlObj = addObjectProperty(node.getID());
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
         // case RenderGrowl.REL_SUBCLASS:
           // break;
         // case RenderGrowl.REL_INSTANCE:
           // break;
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
        addToRelationHash(node, owlObj);
      }
      catch (OWLException e)
      {
        e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#createNewRelation(java.lang.String, int, java.lang.String, java.lang.String)
     */
    public void createNewRelation(String relID, int RelationType, String fromObjID, String toObjID)
    {
      try
      {
        OWLObject owlObj = null;
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        OBObjectNode fromNode = (OBObjectNode)eltSet.findNode(fromObjID);
        OBObjectNode toNode = (OBObjectNode)eltSet.findNode(toObjID);
        OWLObjectProperty prop;
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            String propertyID = stripFromID(node.getID(), fromNode.getID());
            Node n = eltSet.findNode(propertyID);
            prop = (OWLObjectProperty)objectHash.get(n);
            addObjectPropertyInstance((OWLIndividual)objectHash.get(fromNode), prop, (OWLIndividual)objectHash.get(toNode));
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            prop = addObjectProperty(node.getID());
            owlObj = prop;
            addToDomain(prop, (OWLClass)objectHash.get(fromNode));
            addToObjectPropertyRange(prop, (OWLClass)objectHash.get(toNode));
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
         /* case RenderGrowl.REL_SUBCLASS:
            owlObj = addSubClass((OWLClass)objectHash.get(fromNode), (OWLClass)objectHash.get(toNode));
            break;
          case RenderGrowl.REL_INSTANCE:
            addClassToIndividual((OWLIndividual)objectHash.get(fromNode), (OWLClass)objectHash.get(toNode));
            break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

            //break;
        }
        if( owlObj != null )
          objectHash.put(node, owlObj);
      }
      catch (OWLException e)
      {
        e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#addObjectToRange(java.lang.String, java.lang.String)
     */
    public void addObjectToRange(String relID, String toObjID)
    {
      try
      {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        OBObjectNode toNode = (OBObjectNode)eltSet.findNode(toObjID);
        Collection edgesFrom = node.getOutputs();
        OWLObject owlObj = null;
        OWLClass cls;
        OWLObjectProperty prop;
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            OWLIndividual value = (OWLIndividual)objectHash.get(toNode);
            for(Iterator i = edgesFrom.iterator(); i.hasNext();)
            {
              Node n = (Node)i.next();
              String propertyID = stripFromID(node.getID(), n.getID());
              Node propNode = eltSet.findNode(propertyID);
              prop = (OWLObjectProperty)objectHash.get(propNode);
              OWLIndividual indiv = (OWLIndividual)objectHash.get(n);
              addObjectPropertyInstance(indiv, prop, value);
            }
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            prop = (OWLObjectProperty)getUniqueFromRelationHash(node);
            cls = (OWLClass)objectHash.get(toNode);
            addToObjectPropertyRange(prop, cls);
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
        /*  case RenderGrowl.REL_SUBCLASS:
            OWLClass superCls = (OWLClass)objectHash.get(toNode);
            for(Iterator i = edgesFrom.iterator(); i.hasNext();)
            {
              OWLClass subCls = (OWLClass)objectHash.get(i.next());
              owlObj = addSubClass(subCls, superCls);
              addToRelationHash(node, owlObj);
            }
            break;
          case RenderGrowl.REL_INSTANCE:
            cls = (OWLClass)objectHash.get(toNode);
            for(Iterator i = edgesFrom.iterator(); i.hasNext();)
            {
              OWLIndividual indiv = (OWLIndividual)objectHash.get(i.next());
              addClassToIndividual(indiv, cls);
            }
            break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
      }
      catch (OWLException e)
      {
          e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#addObjectToDomain(java.lang.String, java.lang.String)
     */
    public void addObjectToDomain(String relID, String fromObjID)
    {
      try
      {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        OBObjectNode fromNode = (OBObjectNode)eltSet.findNode(fromObjID);
        Collection edgesTo = node.getInputs();
        OWLIndividual indiv;
        OWLObjectProperty prop;
        OWLClass cls;
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            indiv = (OWLIndividual)objectHash.get(fromNode);
            String propertyID = stripFromID(node.getID(), fromNode.getID());
            Node propNode = eltSet.findNode(propertyID);
            prop = (OWLObjectProperty)objectHash.get(propNode);
            for(Iterator i = edgesTo.iterator(); i.hasNext();)
            {
              OWLIndividual value = (OWLIndividual)objectHash.get(i.next());
              addObjectPropertyInstance(indiv, prop, value);
            }
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            prop = (OWLObjectProperty)getUniqueFromRelationHash(node);
            cls = (OWLClass)objectHash.get(fromNode);
            addToDomain(prop, cls);
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
         /* case RenderGrowl.REL_SUBCLASS:
            OWLClass subCls = (OWLClass)objectHash.get(fromNode);
            for(Iterator i = edgesTo.iterator(); i.hasNext();)
            {
              OWLClass superCls = (OWLClass)objectHash.get(i.next());
              OWLObject owlObj = addSubClass(subCls, superCls);
              addToRelationHash(node, owlObj);
            }
            break;
          case RenderGrowl.REL_INSTANCE:
            indiv = (OWLIndividual)objectHash.get(fromNode);
            for(Iterator i = edgesTo.iterator(); i.hasNext();)
            {
              cls = (OWLClass)objectHash.get(i.next());
              addClassToIndividual(indiv, cls);
            }
            break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
      }
      catch (OWLException e)
      {
          e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#deleteObjectFromRange(java.lang.String, java.lang.String)
     */
    public void deleteObjectFromRange(String relID, String toObjID)
    {
      try
      {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        OBObjectNode toNode = (OBObjectNode)eltSet.findNode(toObjID);
        Collection edgesTo = node.getInputs();
        Collection edgesFrom = node.getOutputs();
        OWLClass cls;
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            OWLObjectProperty prop = (OWLObjectProperty)relationHash.get(node);
            cls = (OWLClass)objectHash.get(toNode);
            removeFromObjectPropertyRange(prop, cls);
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
         /* case RenderGrowl.REL_SUBCLASS:
            OWLSubClassAxiom axiom = (OWLSubClassAxiom)relationHash.get(node);
            removeClassAxiom(axiom);
            relationHash.remove(node);
            break;
          case RenderGrowl.REL_INSTANCE:
            OWLIndividual indiv = (OWLIndividual)relationHash.get(node);
            cls = (OWLClass)objectHash.get(toNode);
            removeClassFromIndividual(indiv, cls);
             break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
      }
      catch (OWLException e)
      {
          e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#deleteObjectFromDomain(java.lang.String, java.lang.String)
     */
    public void deleteObjectFromDomain(String relID, String fromObjID)
    {
      try
      {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        OBObjectNode fromNode = (OBObjectNode)eltSet.findNode(fromObjID);
        Collection edgesTo = node.getInputs();
        Collection edgesFrom = node.getOutputs();
        OWLClass cls;
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            OWLObjectProperty prop = (OWLObjectProperty)relationHash.get(node);
            cls = (OWLClass)objectHash.get(fromNode);
            removeFromDomain(prop, cls);
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
          /*case RenderGrowl.REL_SUBCLASS:
            OWLSubClassAxiom axiom = (OWLSubClassAxiom)relationHash.get(node);
            removeClassAxiom(axiom);
            relationHash.remove(node);
            break;
          case RenderGrowl.REL_INSTANCE:
            OWLIndividual indiv = (OWLIndividual)relationHash.get(node);
            cls = (OWLClass)objectHash.get(fromNode);
            removeClassFromIndividual(indiv, cls);
            break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
      }
      catch (OWLException e)
      {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#deleteRelation(java.lang.String)
     */
    public void deleteRelation(String relID)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        switch( node.getRelationCategory() )
        {
          case RenderGrowl.REL_OBJECT_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY:
            break;
          case RenderGrowl.REL_DATA_PROPERTY:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_OBJECT_PROPERTY_RESTRICT_CARD:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_SOME:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_ALL:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_VALUE:
            break;
          case RenderGrowl.REL_DATA_PROPERTY_RESTRICT_CARD:
            break;
         /* case RenderGrowl.REL_SUBCLASS:
            break;
          case RenderGrowl.REL_INSTANCE:
            break;*/
          case RenderGrowl.REL_ARE_EQUIVALENT:
            break;
          case RenderGrowl.REL_ARE_SAME:
            break;
          case RenderGrowl.REL_ARE_DIFFERENT:
            break;
          case RenderGrowl.REL_ARE_DISJOINT:
            break;

        }
        // TODO Auto-generated method stub
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#deleteObject(java.lang.String)
     */
    public void deleteObject(String objID)
    {
        OBObjectNode node = (OBObjectNode)eltSet.findNode(objID);
        OWLObject owlObj = (OWLObject)objectHash.get(node);

        objectHash.remove(node);

        // TODO Auto-generated method stub
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#createNewObject(java.lang.String)
     */
    public void createNewObject(String objID)
    {
        // Deprecated
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#createNewRelation(java.lang.String)
     */
    public void createNewRelation(String relID)
    {
        // Deprecated
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#createNewRelation(java.lang.String, java.lang.String, java.lang.String)
     */
    public void createNewRelation(
        String relID,
        String fromObjID,
        String toObjID)
    {
        // TODO Deprecated
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setObjectName(java.lang.String, java.lang.String)
     */
    public void setObjectName(String objID, String name)
    {
      OBObjectNode node = (OBObjectNode)eltSet.findNode(objID);
      OWLObject obj = (OWLObject)objectHash.get(node);
      if( obj instanceof OWLClass )
      {
        OWLClass cls = (OWLClass)obj;
      }
      else if( obj instanceof OWLIndividual )
      {
        OWLIndividual indiv = (OWLIndividual)obj;
      }
        // TODO Auto-generated method stub
    }

    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setObjectDescription(java.lang.String, java.lang.String)
     */
    public void setObjectDescription(String objID, String desc)
    {
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setObjectAsVariable(java.lang.String, boolean)
     */
    public void setObjectAsVariable(String objID, boolean flag)
    {
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setObjectAsClass(java.lang.String, boolean)
     */
    public void setObjectAsClass(String objID, boolean flag)
    {
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setClassMinCardinality(java.lang.String, int)
     */
    public void setClassMinCardinality(String objID, int minCard)
    {
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setClassMaxCardinality(java.lang.String, int)
     */
    public void setClassMaxCardinality(String objID, int maxCard)
    {
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationType(java.lang.String, int)
     */
    public void setRelationType(String relID, int type)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationName(java.lang.String, java.lang.String)
     */
    public void setRelationName(String relID, String Name)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationDescription(java.lang.String, java.lang.String)
     */
    public void setRelationDescription(String relID, String desc)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationAsVariable(java.lang.String, boolean)
     */
    public void setRelationAsVariable(String relID, boolean flag)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationMinCardinality(java.lang.String, int)
     */
    public void setRelationMinCardinality(String relID, int minCard)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }
    /* (non-Javadoc)
     * @see abdn.graph.ontobrowser.NotificationPolicy#setRelationMaxCardinality(java.lang.String, int)
     */
    public void setRelationMaxCardinality(String relID, int maxCard)
    {
        OBRelationNode node = (OBRelationNode)eltSet.findNode(relID);
        // TODO Auto-generated method stub
    }

//////////////////////////////////////////////////////////////////
// methods to add OWL entities (class, property, individual)
//

    private OWLClass addClass(String className) throws OWLException
    {
        try {
          OWLClass cls;
          URI uri = new URI(className);
          if( (cls = onto.getClass(uri)) == null )
          {
              cls = factory.getOWLClass(uri);
              AddEntity ae = new AddEntity(onto, cls, null);
              ae.accept((ChangeVisitor)onto);
          }
          return cls;
        } catch (URISyntaxException ex) {
          /* This should not happen! */
          return null;
        }
    }

    private OWLObjectProperty addObjectProperty(String uriStr) throws OWLException
    {
        try {
          OWLObjectProperty prop;
          URI uri = new URI(uriStr);
          if( (prop = onto.getObjectProperty(uri)) == null )
          {
              prop = factory.getOWLObjectProperty(uri);
              AddEntity ae = new AddEntity(onto, prop, null);
              ae.accept((ChangeVisitor)onto);
          }
          return prop;
        }
        catch (URISyntaxException ex) {
          /* This should not happen! */
          return null;
        }
    }

    private OWLDataProperty addDataProperty(String uriStr) throws OWLException
    {
        try {
          OWLDataProperty prop;
          URI uri = new URI(uriStr);
          if( (prop = onto.getDataProperty(uri)) == null )
          {
              prop = factory.getOWLDataProperty(uri);
              AddEntity ae = new AddEntity(onto, prop, null);
              ae.accept((ChangeVisitor)onto);
          }
          return prop;
        }
        catch (URISyntaxException ex) {
          /* This should not happen! */
          return null;
        }
    }

    private OWLIndividual addIndividual(String name) throws OWLException
    {
        try {
            OWLIndividual indiv;
            URI uri = new URI(name);
            indiv = factory.getOWLIndividual(uri);
            OntologyChange ae = new AddEntity(onto, indiv, null);
            ae.accept((ChangeVisitor)onto);
            return indiv;
        } catch (URISyntaxException ex) {
          /* This should not happen! */
          return null;
        }
    }

//////////////////////////////////////////////////////////////
// class axiom methods
//

    private OWLSubClassAxiom addSubClass(OWLClass subCls, OWLClass superCls) throws OWLException
    {
        OWLSubClassAxiom axiom = factory.getOWLSubClassAxiom(subCls, superCls);
        OntologyChange oc = new AddClassAxiom(onto, axiom, null);
        oc.accept((ChangeVisitor)onto);
        return axiom;
    }

    private void removeClassAxiom(OWLClassAxiom axiom) throws OWLException
    {
        OntologyChange oc = new RemoveClassAxiom(onto, axiom, null);
        oc.accept((ChangeVisitor)onto);
    }

///////////////////////////////////////////////////////////
// property range and domain methods
//
    private void addToObjectPropertyRange(OWLObjectProperty prop, OWLClass cls) throws OWLException
    {
        Set ranges = prop.getRanges(onto);
        if( !ranges.contains(cls) )
        {
            ranges.add(cls);
            setObjectPropertyRange(ranges, prop);
        }
    }

    private void removeFromObjectPropertyRange(OWLObjectProperty prop, OWLClass cls) throws OWLException
    {
        Set ranges = prop.getRanges(onto);
        if( ranges.contains(cls) )
        {
            ranges.remove(cls);
            setObjectPropertyRange(ranges, prop);
        }
    }

    private void setObjectPropertyRange(Set ranges, OWLObjectProperty prop) throws OWLException
    {
        OWLDescription descr = null;
        if( ranges.size() > 0 )
        {
            descr = factory.getOWLAnd(ranges);
        }
        OntologyChange oc = new AddObjectPropertyRange(onto, prop, descr, null);
        oc.accept((ChangeVisitor)onto);
    }

    private void addToDomain(OWLProperty prop, OWLClass cls) throws OWLException
    {
        Set domains = prop.getDomains(onto);
        if( !domains.contains(cls) )
        {
            domains.add(cls);
            setDomain(domains, prop);
         }
    }

    private void removeFromDomain(OWLProperty prop, OWLClass cls) throws OWLException
    {
        Set domains = prop.getDomains(onto);
        if( domains.contains(cls) )
        {
            domains.remove(cls);
            setDomain(domains, prop);
        }
    }

    private void setDomain(Set domains, OWLProperty prop) throws OWLException
    {
        OWLDescription descr = null;
        if( domains.size() > 0 )
        {
            descr = factory.getOWLOr(domains);
        }
        OntologyChange oc = new AddDomain(onto, prop, descr, null);
        oc.accept((ChangeVisitor)onto);
    }

///////////////////////////////////////////////////////////////////////
// individual methods
//
    private void addClassToIndividual(OWLIndividual indiv, OWLClass cls) throws OWLException
    {
        Set types = indiv.getTypes(onto);
        if( !types.contains(cls) )
        {
            types.add(cls);
            setTypes(types, indiv);
        }
    }

    private void removeClassFromIndividual(OWLIndividual indiv, OWLClass cls) throws OWLException
    {
        Set types = indiv.getTypes(onto);
        if( types.contains(cls) )
        {
            types.remove(cls);
            setTypes(types, indiv);
        }
    }

    private void setTypes(Set types, OWLIndividual indiv) throws OWLException
    {
        OWLDescription descr = null;
        if( types.size() > 0 )
        {
            descr = factory.getOWLAnd(types);
        }
        OntologyChange oc = new AddIndividualClass(onto, indiv, descr, null);
        oc.accept((ChangeVisitor)onto);
    }

    private void addObjectPropertyInstance(OWLIndividual indiv, OWLObjectProperty prop, OWLIndividual propValue) throws OWLException
    {
        OntologyChange oc = new AddObjectPropertyInstance(onto, indiv, prop, propValue, null);
        oc.accept((ChangeVisitor)onto);
    }

    private void addDataPropertyInstance(OWLIndividual indiv, OWLDataProperty prop, OWLDataValue propValue) throws OWLException
    {
        OntologyChange oc = new AddDataPropertyInstance(onto, indiv, prop, propValue, null);
        oc.accept((ChangeVisitor)onto);
    }

////////////////////////////////////////////////////////////////
// methods to manipulate the relation hash
//
    private void addToRelationHash(Node key, OWLObject obj)
    {
        Collection c = (Collection)relationHash.get(key);
        if( c == null )
        {
            c = new Vector();
            relationHash.put(key, c);
        }
        if( !c.contains(obj) )
        {
            c.add(obj);
        }
    }

    private void removeFromRelationHash(Node key, OWLObject obj)
    {
        Collection c = (Collection)relationHash.get(key);
        if( c != null )
        {
            c.remove(obj);
        }
    }

    private Object getUniqueFromRelationHash(Node key)
    {
        Collection c = (Collection)relationHash.get(key);
        if( c.size() == 1 )
        {
            return c.toArray()[0];
        }
        return null;
    }
///////////////////////////////////////////////////////////////

    String stripFromID(String id, String start)
    {
        return id.substring(start.length());
    }

	/* (non-Javadoc)
	 * @see abdn.graph.ontobrowser.NotificationPolicy#creatNewObject(java.lang.String, int)
	 */
	public void creatNewObject(String objID, int objCategory)
  {
    OBObjectNode node = (OBObjectNode)eltSet.findNode(objID);
    try
    {
      OWLObject owlObj = null;
      switch (objCategory) {
        case RenderGrowl.OBJ_CLASS:
          owlObj = addClass(node.getURL());
          break;
        case RenderGrowl.OBJ_INDIVIDUAL:
          owlObj = addIndividual(node.getURL());
          break;
        case RenderGrowl.OBJ_DATATYPE:
          break;
        case RenderGrowl.OBJ_DATAVALUE:
          break;
        case RenderGrowl.OBJ_INTERSECTION:
          break;
        case RenderGrowl.OBJ_UNION:
          break;
        case RenderGrowl.OBJ_COMPLEMENT:
          break;
      }
      if( owlObj != null )
      {
        objectHash.put(node, owlObj);
      }
    }
    catch (OWLException e)
    {
        e.printStackTrace();
    }
	}

	/* (non-Javadoc)
	 * @see abdn.graph.ontobrowser.NotificationPolicy#addLink(java.lang.String, java.lang.String)
	 */
	public void addLink(String fromObjID, String toObjID)
  {
    OBNode fromNode = (OBNode)eltSet.findNode(fromObjID);
    OBNode toNode = (OBNode)eltSet.findNode(toObjID);
    if( fromNode instanceof OBRelationNode )  // origin is a relation node
      {
        if( toNode instanceof OBRelationNode )  // destination is a relation node
        {
        }
        else  // destination is a object node
        {
        }
      }
      else  // origin is a object node
      {
        if( toNode instanceof OBRelationNode )  // destination is a relation node
        {
        }
        else  // destination is a object node
        {
        }
      }
  }
}
