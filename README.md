# Introduction #
Ontologies are vital for the achievement of the Semantic Web. Given their central role it is highly desirable that ontologies be evaluated and made consistent. Several evaluation approaches investigate whether an ontology is “fit for purpose”. Further approaches focus on the consistency of the axioms populating the ontology. By contrast, we propose a new evaluation approach, CleOn, which evaluates the adequacy of taxonomic relationships between sub-concept concept pairs. This method effectively processes the linguistic information inherent in class names, and deﬁnes a lexical path for each concept in the taxonomy in terms of its immediate parent, and recursively for all terms back to the root node. The lexical adequacy of a link between a parent and a child concept is then deﬁned in terms of their lexical paths.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
